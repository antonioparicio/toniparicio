.PHONY: all

# Makefile dependencies
include etc/make/code-quality.mk
include etc/make/composer.mk
include etc/make/docker.mk
include etc/make/documentation.mk
include etc/make/git.mk
include etc/make/testing.mk
include etc/make/base.mk


# Include here the project specific make options


##
## 💚 Status
##

.PHONY: ping-mysql
ping-mysql: ## Ping MySQL
	@docker exec toniparicio-mysql mysqladmin --user=root --password= --host "127.0.0.1" ping --silent

.PHONY: ping-elasticsearch
ping-elasticsearch:	## Ping ElasticSearch
	@curl -I -XHEAD localhost:9200

.PHONY: ping-rabbitmq
ping-rabbitmq: ## Ping RabbitMQ
	@docker exec toniparicio-rabbitmq rabbitmqctl ping --silent
