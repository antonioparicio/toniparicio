<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230918215216 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_form_workshop ADD is_handled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE who_person_comment ADD game_id INT DEFAULT NULL, ADD game_round_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_person_comment ADD CONSTRAINT FK_D0431CC1E48FD905 FOREIGN KEY (game_id) REFERENCES who_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_person_comment ADD CONSTRAINT FK_D0431CC1D44A386 FOREIGN KEY (game_round_id) REFERENCES who_game_round (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_D0431CC1E48FD905 ON who_person_comment (game_id)');
        $this->addSql('CREATE INDEX IDX_D0431CC1D44A386 ON who_person_comment (game_round_id)');
        $this->addSql('ALTER TABLE who_person_like ADD game_id INT DEFAULT NULL, ADD game_round_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_person_like ADD CONSTRAINT FK_DF64F82CE48FD905 FOREIGN KEY (game_id) REFERENCES who_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_person_like ADD CONSTRAINT FK_DF64F82CD44A386 FOREIGN KEY (game_round_id) REFERENCES who_game_round (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_DF64F82CE48FD905 ON who_person_like (game_id)');
        $this->addSql('CREATE INDEX IDX_DF64F82CD44A386 ON who_person_like (game_round_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_form_workshop DROP is_handled');
        $this->addSql('ALTER TABLE who_person_comment DROP FOREIGN KEY FK_D0431CC1E48FD905');
        $this->addSql('ALTER TABLE who_person_comment DROP FOREIGN KEY FK_D0431CC1D44A386');
        $this->addSql('DROP INDEX IDX_D0431CC1E48FD905 ON who_person_comment');
        $this->addSql('DROP INDEX IDX_D0431CC1D44A386 ON who_person_comment');
        $this->addSql('ALTER TABLE who_person_comment DROP game_id, DROP game_round_id');
        $this->addSql('ALTER TABLE who_person_like DROP FOREIGN KEY FK_DF64F82CE48FD905');
        $this->addSql('ALTER TABLE who_person_like DROP FOREIGN KEY FK_DF64F82CD44A386');
        $this->addSql('DROP INDEX IDX_DF64F82CE48FD905 ON who_person_like');
        $this->addSql('DROP INDEX IDX_DF64F82CD44A386 ON who_person_like');
        $this->addSql('ALTER TABLE who_person_like DROP game_id, DROP game_round_id');
    }
}
