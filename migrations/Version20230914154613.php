<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230914154613 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add delete cascade in person_image';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_person_image DROP FOREIGN KEY FK_A5FE8D32217BBB47');
        $this->addSql('ALTER TABLE who_person_image ADD CONSTRAINT FK_A5FE8D32217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_person_image DROP FOREIGN KEY FK_A5FE8D32217BBB47');
        $this->addSql('ALTER TABLE who_person_image ADD CONSTRAINT FK_A5FE8D32217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
    }
}
