<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230915105545 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add delete cascade in who_game and who_image tables';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_game_round DROP FOREIGN KEY FK_AF3E4827217BBB47');
        $this->addSql('ALTER TABLE who_game_round DROP FOREIGN KEY FK_AF3E482799E6F5DF');
        $this->addSql('ALTER TABLE who_game_round DROP FOREIGN KEY FK_AF3E4827E48FD905');
        $this->addSql('ALTER TABLE who_game_round CHANGE person_id person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_game_round ADD CONSTRAINT FK_AF3E4827217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_game_round ADD CONSTRAINT FK_AF3E482799E6F5DF FOREIGN KEY (player_id) REFERENCES who_player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_game_round ADD CONSTRAINT FK_AF3E4827E48FD905 FOREIGN KEY (game_id) REFERENCES who_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B0217BBB47');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B099E6F5DF');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B0A6005CA0');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B0E48FD905');
        $this->addSql('ALTER TABLE who_game_round_clue CHANGE person_id person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B0217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B099E6F5DF FOREIGN KEY (player_id) REFERENCES who_player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B0A6005CA0 FOREIGN KEY (round_id) REFERENCES who_game_round (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B0E48FD905 FOREIGN KEY (game_id) REFERENCES who_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_image DROP FOREIGN KEY FK_F2FC74D0217BBB47');
        $this->addSql('ALTER TABLE who_image ADD CONSTRAINT FK_F2FC74D0217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_image_queue DROP FOREIGN KEY FK_9159C77E217BBB47');
        $this->addSql('ALTER TABLE who_image_queue DROP FOREIGN KEY FK_9159C77E3DA5256D');
        $this->addSql('ALTER TABLE who_image_queue CHANGE person_id person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_image_queue ADD CONSTRAINT FK_9159C77E217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_image_queue ADD CONSTRAINT FK_9159C77E3DA5256D FOREIGN KEY (image_id) REFERENCES who_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_image_version DROP FOREIGN KEY FK_85CA6AC53DA5256D');
        $this->addSql('ALTER TABLE who_image_version ADD CONSTRAINT FK_85CA6AC53DA5256D FOREIGN KEY (image_id) REFERENCES who_image (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_game_round DROP FOREIGN KEY FK_AF3E4827E48FD905');
        $this->addSql('ALTER TABLE who_game_round DROP FOREIGN KEY FK_AF3E482799E6F5DF');
        $this->addSql('ALTER TABLE who_game_round DROP FOREIGN KEY FK_AF3E4827217BBB47');
        $this->addSql('ALTER TABLE who_game_round CHANGE person_id person_id INT NOT NULL');
        $this->addSql('ALTER TABLE who_game_round ADD CONSTRAINT FK_AF3E4827E48FD905 FOREIGN KEY (game_id) REFERENCES who_game (id)');
        $this->addSql('ALTER TABLE who_game_round ADD CONSTRAINT FK_AF3E482799E6F5DF FOREIGN KEY (player_id) REFERENCES who_player (id)');
        $this->addSql('ALTER TABLE who_game_round ADD CONSTRAINT FK_AF3E4827217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B0E48FD905');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B0A6005CA0');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B099E6F5DF');
        $this->addSql('ALTER TABLE who_game_round_clue DROP FOREIGN KEY FK_518436B0217BBB47');
        $this->addSql('ALTER TABLE who_game_round_clue CHANGE person_id person_id INT NOT NULL');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B0E48FD905 FOREIGN KEY (game_id) REFERENCES who_game (id)');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B0A6005CA0 FOREIGN KEY (round_id) REFERENCES who_game_round (id)');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B099E6F5DF FOREIGN KEY (player_id) REFERENCES who_player (id)');
        $this->addSql('ALTER TABLE who_game_round_clue ADD CONSTRAINT FK_518436B0217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
        $this->addSql('ALTER TABLE who_image DROP FOREIGN KEY FK_F2FC74D0217BBB47');
        $this->addSql('ALTER TABLE who_image ADD CONSTRAINT FK_F2FC74D0217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
        $this->addSql('ALTER TABLE who_image_queue DROP FOREIGN KEY FK_9159C77E3DA5256D');
        $this->addSql('ALTER TABLE who_image_queue DROP FOREIGN KEY FK_9159C77E217BBB47');
        $this->addSql('ALTER TABLE who_image_queue CHANGE person_id person_id INT NOT NULL');
        $this->addSql('ALTER TABLE who_image_queue ADD CONSTRAINT FK_9159C77E3DA5256D FOREIGN KEY (image_id) REFERENCES who_image (id)');
        $this->addSql('ALTER TABLE who_image_queue ADD CONSTRAINT FK_9159C77E217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
        $this->addSql('ALTER TABLE who_image_version DROP FOREIGN KEY FK_85CA6AC53DA5256D');
        $this->addSql('ALTER TABLE who_image_version ADD CONSTRAINT FK_85CA6AC53DA5256D FOREIGN KEY (image_id) REFERENCES who_image (id)');
    }
}
