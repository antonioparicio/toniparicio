<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230909143622 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('SET foreign_key_checks = 0');
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C9666CC70C7C');
        $this->addSql('DROP INDEX idx_continent_code ON country');
        $this->addSql('CREATE INDEX IDX_5373C9666CC70C7C ON country (continent)');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C9666CC70C7C FOREIGN KEY (continent) REFERENCES continent (code)');
        $this->addSql('SET foreign_key_checks = 1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('SET foreign_key_checks = 0');
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C9666CC70C7C');
        $this->addSql('DROP INDEX idx_5373c9666cc70c7c ON country');
        $this->addSql('CREATE INDEX idx_continent_code ON country (continent)');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C9666CC70C7C FOREIGN KEY (continent) REFERENCES continent (code)');
        $this->addSql('SET foreign_key_checks = 1');
    }
}
