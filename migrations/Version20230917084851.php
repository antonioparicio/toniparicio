<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230917084851 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add relation between chatgpt_prompt and chatgpt_queue';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chatgpt_prompt ADD queue_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chatgpt_prompt ADD CONSTRAINT FK_65C9CC63477B5BAE FOREIGN KEY (queue_id) REFERENCES chatgpt_queue (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_65C9CC63477B5BAE ON chatgpt_prompt (queue_id)');
        $this->addSql('ALTER TABLE chatgpt_queue ADD prompt_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chatgpt_queue ADD CONSTRAINT FK_34993D67B5C4AA38 FOREIGN KEY (prompt_id) REFERENCES chatgpt_prompt (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_34993D67B5C4AA38 ON chatgpt_queue (prompt_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chatgpt_prompt DROP FOREIGN KEY FK_65C9CC63477B5BAE');
        $this->addSql('DROP INDEX UNIQ_65C9CC63477B5BAE ON chatgpt_prompt');
        $this->addSql('ALTER TABLE chatgpt_prompt DROP queue_id');
        $this->addSql('ALTER TABLE chatgpt_queue DROP FOREIGN KEY FK_34993D67B5C4AA38');
        $this->addSql('DROP INDEX UNIQ_34993D67B5C4AA38 ON chatgpt_queue');
        $this->addSql('ALTER TABLE chatgpt_queue DROP prompt_id');
    }
}
