<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230908150429 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chatgpt_prompt (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, person_locale_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, input_tokens INT NOT NULL, output_tokens INT NOT NULL, cost DOUBLE PRECISION NOT NULL, is_success TINYINT(1) NOT NULL, response VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_65C9CC635D703C7B (person_locale_id), INDEX search_idx (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chatgpt_prompt ADD CONSTRAINT FK_65C9CC63217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
        $this->addSql('ALTER TABLE chatgpt_prompt ADD CONSTRAINT FK_65C9CC635D703C7B FOREIGN KEY (person_locale_id) REFERENCES who_person_locale (id)');
        $this->addSql('ALTER TABLE continent CHANGE code code VARCHAR(2) NOT NULL, CHANGE name name VARCHAR(255) NOT NULL, CHANGE model model VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE country CHANGE code code VARCHAR(2) NOT NULL, CHANGE name name VARCHAR(64) NOT NULL, CHANGE full_name full_name VARCHAR(128) NOT NULL, CHANGE iso3 iso3 VARCHAR(3) NOT NULL, CHANGE number number INT NOT NULL, CHANGE continent continent VARCHAR(2) NOT NULL, CHANGE display_order display_order INT NOT NULL');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C9666CC70C7C FOREIGN KEY (continent) REFERENCES continent (code)');
        $this->addSql('ALTER TABLE who_person_error ADD is_handled TINYINT(1) NOT NULL, CHANGE user user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_person_error ADD CONSTRAINT FK_3D1E351CA76ED395 FOREIGN KEY (user_id) REFERENCES who_player (id)');
        $this->addSql('CREATE INDEX IDX_3D1E351CA76ED395 ON who_person_error (user_id)');
        $this->addSql('DROP INDEX provider ON who_player');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE chatgpt_prompt');
        $this->addSql('ALTER TABLE continent CHANGE code code CHAR(2) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci` COMMENT \'Continent code\', CHANGE name name VARCHAR(255) CHARACTER SET utf8mb3 DEFAULT NULL COLLATE `utf8mb3_general_ci`, CHANGE model model VARCHAR(100) CHARACTER SET utf8mb3 DEFAULT NULL COLLATE `utf8mb3_general_ci`');
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C9666CC70C7C');
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C9666CC70C7C');
        $this->addSql('ALTER TABLE country CHANGE continent continent CHAR(2) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, CHANGE code code CHAR(2) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci` COMMENT \'Two-letter country code (ISO 3166-1 alpha-2)\', CHANGE name name VARCHAR(64) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci` COMMENT \'English country name\', CHANGE full_name full_name VARCHAR(128) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci` COMMENT \'Full English country name\', CHANGE iso3 iso3 CHAR(3) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci` COMMENT \'Three-letter country code (ISO 3166-1 alpha-3)\', CHANGE number number SMALLINT UNSIGNED NOT NULL COMMENT \'Three-digit country number (ISO 3166-1 numeric)\', CHANGE display_order display_order INT DEFAULT 900 NOT NULL');
        $this->addSql('DROP INDEX idx_5373c9666cc70c7c ON country');
        $this->addSql('ALTER TABLE who_person_error DROP FOREIGN KEY FK_3D1E351CA76ED395');
        $this->addSql('DROP INDEX IDX_3D1E351CA76ED395 ON who_person_error');
        $this->addSql('ALTER TABLE who_person_error DROP is_handled, CHANGE user_id user INT DEFAULT NULL');
        $this->addSql('CREATE INDEX provider ON who_player (provider)');
    }
}
