<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230909143230 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('SET foreign_key_checks = 0');
        $this->addSql('DROP INDEX search_idx ON chatgpt_prompt');
        $this->addSql('ALTER TABLE chatgpt_prompt DROP updated_at, DROP deleted_at');
        $this->addSql('CREATE INDEX IDX_65C9CC63217BBB47 ON chatgpt_prompt (person_id)');
        $this->addSql('CREATE INDEX search_idx ON chatgpt_prompt (person_id, person_locale_id)');
        $this->addSql('DROP INDEX search_idx ON chatgpt_queue');
        $this->addSql('ALTER TABLE chatgpt_queue DROP deleted_at');
        $this->addSql('CREATE INDEX IDX_34993D67217BBB47 ON chatgpt_queue (person_id)');
        $this->addSql('CREATE INDEX search_idx ON chatgpt_queue (person_id, person_locale_id)');
        $this->addSql('SET foreign_key_checks = 1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('SET foreign_key_checks = 0');
        $this->addSql('DROP INDEX IDX_65C9CC63217BBB47 ON chatgpt_prompt');
        $this->addSql('DROP INDEX search_idx ON chatgpt_prompt');
        $this->addSql('ALTER TABLE chatgpt_prompt ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX search_idx ON chatgpt_prompt (person_id)');
        $this->addSql('DROP INDEX IDX_34993D67217BBB47 ON chatgpt_queue');
        $this->addSql('DROP INDEX search_idx ON chatgpt_queue');
        $this->addSql('ALTER TABLE chatgpt_queue ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX search_idx ON chatgpt_queue (person_id)');
        $this->addSql('SET foreign_key_checks = 1');
    }
}
