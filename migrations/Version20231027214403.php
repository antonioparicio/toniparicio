<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231027214403 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE who_person_clue_share (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, person_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, clue INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_8D28809599E6F5DF (player_id), INDEX IDX_8D288095217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE who_person_clue_share ADD CONSTRAINT FK_8D28809599E6F5DF FOREIGN KEY (player_id) REFERENCES who_player (id)');
        $this->addSql('ALTER TABLE who_person_clue_share ADD CONSTRAINT FK_8D288095217BBB47 FOREIGN KEY (person_id) REFERENCES who_person_locale (id)');
        $this->addSql('ALTER TABLE who_person_fact CHANGE person_id person_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE who_person_clue_share');
        $this->addSql('ALTER TABLE who_person_fact CHANGE person_id person_id INT NOT NULL');
    }
}
