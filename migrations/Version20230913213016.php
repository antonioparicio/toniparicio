<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230913213016 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add delete cascade in chatgpt tables';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chatgpt_prompt DROP FOREIGN KEY FK_65C9CC63217BBB47');
        $this->addSql('ALTER TABLE chatgpt_prompt ADD CONSTRAINT FK_65C9CC63217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chatgpt_queue DROP FOREIGN KEY FK_34993D67217BBB47');
        $this->addSql('ALTER TABLE chatgpt_queue ADD CONSTRAINT FK_34993D67217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chatgpt_prompt DROP FOREIGN KEY FK_65C9CC63217BBB47');
        $this->addSql('ALTER TABLE chatgpt_prompt ADD CONSTRAINT FK_65C9CC63217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
        $this->addSql('ALTER TABLE chatgpt_queue DROP FOREIGN KEY FK_34993D67217BBB47');
        $this->addSql('ALTER TABLE chatgpt_queue ADD CONSTRAINT FK_34993D67217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
    }
}
