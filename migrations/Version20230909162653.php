<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230909162653 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Prepopulate who_person_work_type';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('SET foreign_key_checks = 0');
        $this->addSql('ALTER TABLE who_person_work_type ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_person_work_type ADD CONSTRAINT FK_FFD00C1512469DE2 FOREIGN KEY (category_id) REFERENCES who_category (id)');
        $this->addSql('CREATE INDEX IDX_FFD00C1512469DE2 ON who_person_work_type (category_id)');
        $this->addSql('TRUNCATE TABLE `who_person_work_type`');
        $this->addSql(<<<EOF
INSERT INTO `who_person_work_type` (`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`, `wikipedia`, `image`, `icon`, `category_id`) VALUES
(1, 'book', 'Obra literaria', '2020-07-13 19:56:21', '2020-08-26 23:31:09', NULL, 'Q571', NULL, 'ic_work_book', 3),
(2, 'music', 'Obra musical', '2020-07-13 19:57:25', '2020-08-26 23:30:58', NULL, 'Q482994', NULL, 'ic_work_music', 7),
(3, 'sculpture', 'Obra escultura', '2020-07-13 19:57:53', '2020-07-13 19:57:53', NULL, NULL, NULL, 'ic_work_sculpture', 6),
(4, 'picture', 'Obra pictorica', '2020-07-13 19:58:12', '2020-08-26 23:30:32', NULL, 'Q3305213', 1, 'ic_work_picture', 2),
(5, 'cinema', 'Obra de cine o televisión', '2020-08-06 16:14:35', '2020-08-26 23:30:24', NULL, 'Q11424', NULL, 'ic_work_cinema', 11),
(6, 'play', 'form of literature intended for theatrical performance', '2020-08-26 08:08:32', '2020-08-26 23:30:13', NULL, 'Q25379', NULL, 'ic_work_book', NULL),
(7, 'writting', 'any creative work expressed in writing like: inscriptions, manuscripts, documents or maps', '2020-08-26 08:38:23', '2020-08-26 23:30:02', NULL, 'Q47461344', NULL, 'ic_work_book', NULL),
(8, 'novella', 'written, fictional, prose narrative normally longer than a short story but shorter than a novel', '2020-08-26 08:40:11', '2020-08-26 23:29:55', NULL, 'Q149537', NULL, 'ic_work_book', NULL),
(9, 'painting series', 'thematic group of an artist\'s paintings', '2020-08-26 11:51:04', '2020-08-26 23:29:31', NULL, 'Q15727816', 1, 'ic_work_picture', NULL),
(10, 'serenade', NULL, '2020-08-26 23:25:59', '2020-08-26 23:25:59', NULL, 'Q215911', 0, 'ic_work_music', NULL),
(11, 'dramatico_musical', NULL, '2020-08-26 23:26:36', '2020-08-26 23:26:36', NULL, 'Q58483083', 0, 'ic_work_music', NULL),
(12, 'musical composition', NULL, '2020-08-26 23:29:08', '2020-08-26 23:29:08', NULL, 'Q207628', 0, 'ic_work_music', NULL),
(13, 'composition for piano', 'piece of music for piano', '2020-08-26 23:34:32', '2020-08-26 23:34:32', NULL, 'Q1746015', 0, 'ic_work_music', NULL),
(14, 'opera', 'artform combining sung text and musical score in a theatrical setting', '2020-08-26 23:36:11', '2020-08-26 23:36:11', NULL, 'Q1344', 0, 'ic_work_music', NULL),
(15, 'symphony', 'extended musical composition', '2020-08-26 23:36:39', '2020-08-26 23:36:39', NULL, 'Q9734', 0, 'ic_work_music', NULL),
(16, 'novel series', 'set of novels that should be read in order', '2020-08-26 23:40:17', '2020-08-26 23:40:17', NULL, 'Q1667921', 0, 'ic_work_book', NULL),
(17, 'book series', 'sequence of books having certain characteristics in common that are formally identified together as a group\r\nseries of books', '2020-08-26 23:40:46', '2020-08-26 23:40:46', NULL, 'Q277759', 0, 'ic_work_book', NULL),
(18, 'short story', 'brief work of literature, usually written in narrative prose', '2020-08-26 23:41:10', '2020-08-26 23:41:10', NULL, 'Q49084', 0, 'ic_work_book', NULL),
(19, 'short story collection', NULL, '2020-08-26 23:41:26', '2020-08-26 23:41:26', NULL, 'Q1279564', 0, 'ic_work_book', NULL),
(20, 'novelette', 'narrative prose fiction shorter than a novella and longer than a short story', '2020-08-26 23:41:46', '2020-08-26 23:41:46', NULL, 'Q472808', 0, 'ic_work_book', NULL),
(21, 'poen', 'poetical work', '2020-08-26 23:51:35', '2020-08-26 23:51:35', NULL, 'Q5185279', 0, 'ic_work_book', NULL),
(22, 'literary work', 'creative work by a writer created with aesthetic, academic or recreative purposes', '2020-08-26 23:52:08', '2020-08-26 23:52:08', NULL, 'Q7725634', 0, 'ic_work_book', NULL),
(23, 'series of prints', 'print series', '2020-08-27 19:17:05', '2020-08-27 19:17:05', NULL, 'Q19960510', 1, 'ic_work_picture', NULL),
(24, 'invent', NULL, '2020-08-29 16:04:03', '2020-08-29 16:04:03', NULL, '0', 0, 'ic_work_machine', NULL),
(25, 'business', NULL, '2020-10-10 10:53:43', '2020-10-10 10:53:43', NULL, 'Q4830453', 0, 'ic_work_business', 19),
(26, 'architecture', NULL, '2020-10-10 10:54:16', '2020-10-10 10:54:43', NULL, 'Q385378', 1, 'ic_work_architecture', 14),
(27, 'theory', 'Theory', '2020-12-06 10:22:07', '2020-12-06 10:22:07', NULL, '0', 0, 'ic_work_machine', NULL),
(28, 'battle', 'Battle or war where person was relevant', '2020-12-23 10:35:01', '2020-12-23 10:35:01', NULL, 'Q178561', 0, 'ic_battle', 5);
EOF
);
        $this->addSql('SET foreign_key_checks = 1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_person_work_type DROP FOREIGN KEY FK_FFD00C1512469DE2');
        $this->addSql('DROP INDEX IDX_FFD00C1512469DE2 ON who_person_work_type');
        $this->addSql('ALTER TABLE who_person_work_type DROP category_id');
    }
}
