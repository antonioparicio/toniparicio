<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230925215323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add PersonLocale to Comments with index';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_game_round ADD person_locale_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_game_round ADD CONSTRAINT FK_AF3E48275D703C7B FOREIGN KEY (person_locale_id) REFERENCES who_person_locale (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_AF3E48275D703C7B ON who_game_round (person_locale_id)');
        $this->addSql('ALTER TABLE who_person_comment DROP FOREIGN KEY FK_D0431CC1217BBB47');
        $this->addSql('ALTER TABLE who_person_comment ADD person_locale_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE who_person_comment ADD CONSTRAINT FK_D0431CC15D703C7B FOREIGN KEY (person_locale_id) REFERENCES who_person_locale (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_person_comment ADD CONSTRAINT FK_D0431CC1217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX search_idx ON who_person_comment (person_locale_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_game_round DROP FOREIGN KEY FK_AF3E48275D703C7B');
        $this->addSql('DROP INDEX IDX_AF3E48275D703C7B ON who_game_round');
        $this->addSql('ALTER TABLE who_game_round DROP person_locale_id');
        $this->addSql('ALTER TABLE who_person_comment DROP FOREIGN KEY FK_D0431CC15D703C7B');
        $this->addSql('ALTER TABLE who_person_comment DROP FOREIGN KEY FK_D0431CC1217BBB47');
        $this->addSql('DROP INDEX search_idx ON who_person_comment');
        $this->addSql('ALTER TABLE who_person_comment DROP person_locale_id');
        $this->addSql('ALTER TABLE who_person_comment ADD CONSTRAINT FK_D0431CC1217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
    }
}
