<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230927102736 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_game DROP FOREIGN KEY FK_3A604E6799E6F5DF');
        $this->addSql('ALTER TABLE who_game ADD CONSTRAINT FK_3A604E6799E6F5DF FOREIGN KEY (player_id) REFERENCES who_player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_player CHANGE comments can_comment TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE who_game DROP FOREIGN KEY FK_3A604E6799E6F5DF');
        $this->addSql('ALTER TABLE who_game ADD CONSTRAINT FK_3A604E6799E6F5DF FOREIGN KEY (player_id) REFERENCES who_player (id)');
        $this->addSql('ALTER TABLE who_player CHANGE can_comment comments TINYINT(1) NOT NULL');
    }
}
