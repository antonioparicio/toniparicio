<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231001133414 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create Table who_workshop_difficulty';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE who_workshop_difficulty (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, author_id INT DEFAULT NULL, difficulty SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, status INT NOT NULL, INDEX IDX_99D61BFA217BBB47 (person_id), INDEX IDX_99D61BFAF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE who_workshop_difficulty ADD CONSTRAINT FK_99D61BFA217BBB47 FOREIGN KEY (person_id) REFERENCES who_person_locale (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE who_workshop_difficulty ADD CONSTRAINT FK_99D61BFAF675F31B FOREIGN KEY (author_id) REFERENCES who_player (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE who_workshop_difficulty');
    }
}
