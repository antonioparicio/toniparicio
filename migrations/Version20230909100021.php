<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230909100021 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chatgpt_queue (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, person_locale_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, is_handled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_34993D675D703C7B (person_locale_id), INDEX search_idx (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chatgpt_queue ADD CONSTRAINT FK_34993D67217BBB47 FOREIGN KEY (person_id) REFERENCES who_person (id)');
        $this->addSql('ALTER TABLE chatgpt_queue ADD CONSTRAINT FK_34993D675D703C7B FOREIGN KEY (person_locale_id) REFERENCES who_person_locale (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE chatgpt_queue');
    }
}
