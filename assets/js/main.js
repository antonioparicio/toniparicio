$(document).ready(function() {
  // fix aside navbar on scroll
  $('#navbar-fixed').pushpin({ top: $('#navbar-fixed').offset().top });

  // scrollspy for trigger events on scroll
  $('.scrollspy').scrollSpy();
});
