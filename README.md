## Installation and configuration

1. Clone repository:  
    ```shell
    git clone git@bitbucket.org:antonioparicio/toniparicio.git
    ```
2. Generate SSL certificates
```shell
cd etc/infrastructure/nginx/certs
openssl && openssl req -x509 -nodes -days 365 -subj "/C=CA/ST=QC/O=Toni Paricio, Inc./CN=toniparicio.com" -addext "subjectAltName=DNS:toniparicio.dev.com" -newkey rsa:2048 -keyout certificate.key -out certificate.crt;
```
3. Add toniparicio.dev.com to know hosts
```shell
nano /etc/hosts
```

add `127.0.0.1       toniparicio.dev.com`


2. Run docker-compose:  
   ```shell
   docker-compose up
   ```
3. 


# Trobleshooting

If after run serve we see a:
```log
Fatal error: require(): Failed opening required '/app/var/cache/prod/doctrine/orm/Proxies/__CG__AppEntityImage.php' (include_path='.:/usr/local/lib/php') in /app/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php on line 204
PHP message: PHP Fatal error: require(): Failed opening required '/app/var/cache/prod/doctrine/orm/Proxies/__CG__AppEntityBook.php' (include_path='.:/usr/local/lib/php') in /app/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php on line 204
```

Run:
```shell
chown -R 1000:1000 var/cache/
php bin/console cache:warmup
php bin/console cache:warmup --env=dev
chown -R 1000:1000 var/cache/
```