.PHONY: all

ifndef DISABLE_INTERACTIVE
	FLAG =
else
	FLAG = -T
endif

# Folders
CWD := $(shell pwd)

# Docker constants
DOCKER_NAMESPACE := $(shell basename $(CURDIR) | tr A-Z a-z)
DOCKER_COMPOSE   := docker-compose -p $(DOCKER_NAMESPACE) -f docker-compose.yml --project-dir ${PWD}

# Project constants
SERVICE := php
RUN  := $(DOCKER_COMPOSE) run $(FLAG) --user="${UID}:${GID}" --rm ${SERVICE} sh -c

# Others
SECURITY_CHECKER=2.0.6

##
## 🧐 Code Quality
##


.PHONY: security-check
security-check: ## Check composer dependencies for security risks
	@wget -q https://github.com/fabpot/local-php-security-checker/releases/download/v${SECURITY_CHECKER}/local-php-security-checker_${SECURITY_CHECKER}_linux_amd64 -O ./bin/local-php-security-checker
	@chmod +x ./bin/local-php-security-checker
	@$(RUN) "./bin/local-php-security-checker"

.PHONY: code-duplicate
code-duplicate: ## Check code by search duplication
	@$(RUN) "./bin/phpcpd src/ --min-lines 10"

.PHONY: code-check
code-check: ## Check code with quality tools
	@$(RUN) "php -d memory-limit=-1 ./vendor/bin/php-cs-fixer fix --dry-run --diff --allow-risky=yes src"
	@$(RUN) "php -d memory-limit=-1 ./vendor/bin/php-cs-fixer fix --dry-run --diff --allow-risky=yes tests"
	@$(RUN) "php -d memory-limit=-1 ./vendor/bin/phpcs --standard=ruleset.xml --parallel=$(shell nproc) -s src tests"

.PHONY: code-fix
code-fix: ## Check and fix code with quality tools
	@$(RUN) "php -d memory-limit=-1 ./vendor/bin/php-cs-fixer fix --diff --allow-risky=yes src"
	@$(RUN) "php -d memory-limit=-1 ./vendor/bin/php-cs-fixer fix --diff --allow-risky=yes tests"
	@$(RUN) "php -d memory-limit=-1 ./vendor/bin/phpcs --standard=ruleset.xml --parallel=$(shell nproc) -s src tests"

.PHONY: rector-check
rector-dry-run: ## Check code with Rector
	$(RUN) "XDEBUG_MODE=off php -d memory-limit=-1 ./vendor/bin/rector process --config rector.php --ansi --dry-run"

.PHONY: rector
rector: ## Check code with Rector
	@$(RUN) "XDEBUG_MODE=off php -d memory-limit=-1 ./vendor/bin/rector process --config rector.php --ansi"

.PHONY: phpstan
phpstan: ## Check code with phpstan
	@$(RUN) "php ./vendor/bin/phpstan analyse -c phpstan.neon --memory-limit=-1"