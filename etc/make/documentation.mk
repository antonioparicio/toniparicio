.PHONY: all



##
## 📝 Documentation
##

.PHONY: openapi-lint
openapi-lint: ## Validate OpenAPI documentation
	docker run --rm -v $(PWD)/doc/openapi:/tmp -w /tmp stoplight lint openapi.yaml