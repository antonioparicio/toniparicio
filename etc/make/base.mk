.PHONY: all

.DEFAULT_GOAL := help

ifndef DISABLE_INTERACTIVE
	FLAG =
else
	FLAG = -T
endif

# Permissions constants
UID := $(shell id -u)
GID := $(shell id -g)

export UID
export GID

##
## 🔧 General
##
# https://www.padok.fr/en/blog/beautiful-makefile-awk
.PHONY: help
help: ## Display Makefile help
	@awk 'BEGIN { \
			FS = ":.*##"; \
			printf "\nUsage:\n  make \033[36m<target>\033[0m\n\n" \
		} \
		/^## .*/ { \
			printf "\n  \033[0;33m%-18s%s\033[0m\n\n", substr($$1,4), $$2 \
		} \
		/^[a-zA-Z_0-9-]+:.*?##/ { \
			printf "  \033[36m%-18s\033[0m %s\n", $$1, $$2 \
		} \
		/^##@/ { \
			printf "\n\033[1m%s\033[0m\n", substr($$0, 5) \
		} ' $(MAKEFILE_LIST)

.PHONY: init
init: ## Initialize project by install dependencies and git hooks
	clean git-hooks docker-prepare up composer-install db-init

.PHONY: build
build: deps up ## Build project by install dependencies and up containers

.PHONY: deps
deps: composer-install ## Install project dependencies
