.PHONY: all

CWD := $(shell pwd)


##
## 🎵 Composer
##
.PHONY: composer
# default composer command to use for other composer commands with CMD
composer composer-validate composer-install composer-update composer-require composer-require-dev: composer-env-file
	@docker run --rm $(INTERACTIVE) --volume $(CWD):/app --user ${UID}:${GID} \
		composer:2.3.7 $(CMD) --no-ansi

.PHONY: composer-validate
composer-validate: CMD=validate ## Validate composer json file

.PHONY: composer-install
composer-install: CMD=install --ignore-platform-reqs ## Install composer dependencies

.PHONY: composer-update
composer-update: CMD=udpate --ignore-platform-reqs ## Update composer dependencies

.PHONY: composer-env-file
composer-env-file: ## Generate .env.local file
	@if [ ! -f .env.local ]; then cp .env .env.local; fi

.PHONY: composer-require
composer-require: CMD=require --ignore-platform-reqs	## Require a composer dependency
composer-require: INTERACTIVE=-ti --interactive

.PHONY: composer-require-dev
composer-require-dev: CMD=require --ignore-platform-reqs --dev	## Require a composer dependency in development
composer-require-dev: INTERACTIVE=-ti --interactive

.PHONY: reload
reload: composer-env-file
	@docker-compose exec php-fpm kill -USR2 1
	@docker-compose exec nginx nginx -s reload

.PHONY: rebuild
rebuild: composer-env-file
	docker-compose build --pull --force-rm --no-cache
	make deps
	make start