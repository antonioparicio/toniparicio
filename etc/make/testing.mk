.PHONY: all

# Permissions constants
UID := $(shell id -u)
GID := $(shell id -g)

export UID
export GID

# Folders
CWD := $(shell pwd)
UNIT_TEST_PATH :=
MUTANT_TEST_PATH:=
BDD_TEST_PATH :=

# Project constants
SERVICE := php
RUN  := $(DOCKER_COMPOSE) run $(FLAG) --user="${UID}:${GID}" --rm ${SERVICE} sh -c

##
## 🧪 Testing
##

.PHONY: phpunit
phpunit: ## Run PHPUnit tests
ifneq ($(strip $(UNIT_TEST_PATH)),)
	@$(RUN) "php -d memory_limit=-1 ./vendor/bin/phpunit --no-coverage $(UNIT_TEST_PATH)"
else
	@$(RUN) "php -d memory_limit=-1 ./vendor/bin/phpunit --no-coverage --stop-on-failure"
endif

.PHONY: coverage
coverage: ## Run PHPUnit tests with coverage report
	@$(RUN) "php -d memory_limit=-1 ./vendor/bin/phpunit --stop-on-failure"

.PHONY: mutant
mutant: ## Run mutant tests by Infection
	@$(RUN) "php -d memory_limit=-1 ./vendor/bin/phpunit --stop-on-failure"
ifneq ($(strip $(MUTANT_TEST_PATH)),)
	@$(RUN) "XDEBUG_MODE=coverage php -d memory_limit=-1 ./vendor/bin/infection --filter=$(MUTANT_TEST_PATH) --threads=$(shell nproc) --coverage=report --show-mutations"
else
	@$(RUN) "XDEBUG_MODE=coverage php -d memory_limit=-1 ./vendor/bin/infection --threads=$(shell nproc) --coverage=report --show-mutations"
endif

.PHONY: bdd
bdd: ## Run BDD tests by Behat
	@$(RUN) "rm -rf /tmp/symfony-cache"
	@$(RUN) "php bin/console doctrine:database:drop	  --env=test --quite --no-interaction --if-exists --force"
	@$(RUN) "php bin/console doctrine:database:create  --env=test --quite --no-interaction"
	@$(RUN) "php bin/console doctrine:database:migrate --env=test --quite --no-interaction"
ifneq ($(strip $(BDD_TEST_PATH)),)
	@$(RUN) "php -d memory_limit=-1 ./vendor/bin/behat --no-snippets --strict -vvv $(BDD_TEST_PATH)"
else
	@$(RUN) "php -d memory_limit=-1 ./vendor/bin/behat --no-snippets --strict -vvv"
endif