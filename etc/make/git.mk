.PHONY: all

##
## 🔀 Git
##

.PHONY: git-hooks
git-hooks: ## Configure git hooks defined in /etc/git
	@git config core.hooksPath ./etc/git

.PHONY: git-check
git-check: ## Run all git-hooks without perform the git operations like commit
	@./etc/git/pre-push