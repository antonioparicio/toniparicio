.PHONY: all

# Permissions constants
UID := $(shell id -u)
GID := $(shell id -g)

export UID
export GID

# Docker constants
DOCKER_NAMESPACE := $(shell basename $(CURDIR) | tr A-Z a-z)
DOCKER_COMPOSE   := docker-compose -p $(DOCKER_NAMESPACE) -f docker-compose.yml --project-dir ${PWD}
SERVICE          := php

##
## 🐳 Docker
##
# ${PWD}/.env && $(DOCKER_COMPOSE) --profile basic up -d --remove-orphans
.PHONY: up
up: ## Up docker containers
	$(DOCKER_COMPOSE) --profile basic up -d --remove-orphans

.PHONY: restart
restart: ## Restart docker containers
	@$(DOCKER_COMPOSE) restart

.PHONY: stop
stop: ## Stop docker containers
	@$(DOCKER_COMPOSE) stop

.PHONY: down
down: ## Down docker containers
	@$(DOCKER_COMPOSE) down

.PHONY: clean
clean: ## Clean container by remove containers and volumes
	@$(DOCKER_COMPOSE) rm -s -v

.PHONY: bash
bash: ## Run shell console in the main container
	@$(DOCKER_COMPOSE) exec $(SERVICE) sh

# Usage: `make doco CMD="ps --services"`
# Usage: `make doco CMD="build --parallel --pull --force-rm --no-cache"`
.PHONY: doco
doco: ## Run docker-compose command. Usage 'make doco CMD="ps --services"'
	UID=${UID} GID=${GID} docker-compose $(CMD)