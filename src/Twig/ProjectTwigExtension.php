<?php
// src/Twig/ProjectTwigExtension.php
namespace App\Twig;

use App\Entity\SocialNetwork;
use App\Entity\BookQuote;
use Doctrine\Persistence\ManagerRegistry;

class ProjectTwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * RegistryInterface for get doctrine query
     * @var ManagerRegistry
     */
    protected $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * add some doctrine results to Twig globa variables
     *
     * @return array
     */
    public function getGlobals()
    {
        $em = $this->doctrine->getManager();

        $socials = $em->getRepository(SocialNetwork::class)->findAllOrderByPosition();
        $quote   = $em->getRepository(BookQuote::class)->getOneRandom();

        return ['socials' => $socials, 'quote' => $quote];
    }
}
