<?php

namespace App\Twig;

class JsonEncodeTwigExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('pretty_print', array($this, 'prettyPrint')),
        );
    }

    public function prettyPrint($value)
    {
        if (is_string($value)) {
            $arr = @json_decode($value, true);
            $value = $arr ?? $value;
        }
        if (is_array($value)) {
            $value = json_encode($value, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        }
        return (string) $value;
    }
}