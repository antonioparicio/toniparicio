<?php

namespace App\Repository;

use App\Entity\TicTacTocModes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TicTacTocModes|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicTacTocModes|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicTacTocModes[]    findAll()
 * @method TicTacTocModes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicTacTocModesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicTacTocModes::class);
    }

//    /**
//     * @return TicTacTocModes[] Returns an array of TicTacTocModes objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicTacTocModes
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
