<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\GameRound;
use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLocale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use RectorPrefix202309\Illuminate\Contracts\Support\Arrayable;

/**
 *
 */
class PersonLocaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonLocale::class);
    }

    /**
     * fetches Persons that are updates since date
     *
     * @return array
     */
    public function getUpdates(string $locale, \DateTime $date): array
    {
        $result = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.updated_at > :from AND a.locale = :locale')
            ->setParameter('from', $date->format('Y-m-d H:i:s'))
            ->setParameter('locale', $locale)
            ->orderBy('a.updated_at')
            // define relations manually to avoid objects and extra data
            ->getQuery()
            ->getResult();



        return $result;
    }

    /**
     * @return PersonLocale|null
     */
    public function getByName(string $name, string $locale = 'es'): ?PersonLocale
    {
        return $this->findOneBy(['name' => $name, 'locale' => $locale]);
    }

    public function getPersonsWithoutFacts($locale = 'es'): array
    {
        $sql = <<<EOF
            SELECT a.* FROM who_person_locale a
            LEFT JOIN who_person_fact f
            ON a.id = f.person_id
            WHERE f.id IS NULL
            AND a.locale = :locale
        EOF;

        $connection = $this->getEntityManager()->getConnection();

        return $connection->fetchAllAssociative($sql, ['locale' => $locale]);
    }

    public function getPersonsWithoutQuotes($locale = 'es'): array
    {
        $sql = <<<EOF
            SELECT a.* FROM who_person_locale a
            LEFT JOIN who_person_quote q
            ON a.id = q.person_id
            WHERE q.id IS NULL
            AND a.locale = :locale
        EOF;

        $connection = $this->getEntityManager()->getConnection();

        return $connection->fetchAllAssociative($sql, ['locale' => $locale]);
    }

    public function getPersonsWithoutWorks($locale = 'es'): array
    {
        $sql = <<<EOF
            SELECT a.* FROM who_person_locale a
            LEFT JOIN who_person_work w
            ON a.id = w.person_id
            WHERE w.id IS NULL
            AND a.locale = :locale
        EOF;

        $connection = $this->getEntityManager()->getConnection();

        return $connection->fetchAllAssociative($sql, ['locale' => $locale]);
    }

    public function getPersonsWithoutBiography($locale = 'es'): array
    {
        $sql = <<<EOF
            SELECT a.* FROM who_person_locale a
            WHERE (LENGTH(a.description) < 128 OR a.description IS NULL) 
            AND a.locale = :locale
        EOF;

        $connection = $this->getEntityManager()->getConnection();

        return $connection->fetchAllAssociative($sql, ['locale' => $locale]);
    }

    public function getDuplicate(Person $person): ?PersonLocale
    {
        $results = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.person != :person')
            ->andWhere('a.name = :name')
            ->setParameter('person', $person)
            ->setParameter('name', $person->getRealName())
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $results ? $results[0] : null;
    }

    public function getPersonDifficulties(string $locale, int $minimum = 3): array
    {
        $sql = <<<EOF
            SELECT COUNT(*) as games, SUM(is_success) as success, locale, SUM(is_success)/COUNT(*) as avg, person_locale_id
            FROM `who_game_round`
            GROUP BY person_locale_id
            HAVING games > :min and locale = :locale and person_locale_id IS NOT NULL
            ORDER BY avg DESC;
        EOF;

        $connection = $this->getEntityManager()->getConnection();

        return $connection->fetchAllAssociative($sql, ['locale' => $locale, 'min' => $minimum]);
    }
}
