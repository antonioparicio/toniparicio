<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonCommentLike;
use App\Entity\WhoIAm\PersonLike;
use App\Entity\WhoIAm\PersonScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 *
 */
class PersonCommentLikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonCommentLike::class);
    }

    /**
     * fetches Persons that are updates since date
     *
     * @param int $person
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAverage(Person $person): float
    {
        $result = $this->createQueryBuilder('s')
            ->select('avg(s.score) as score')
            ->where('s.person = :person')
            ->setParameter('person', $person->getId())
            ->getQuery()
            ->getSingleResult();

        return round((float)($result['score'] ?? 0), 1);
    }

}
