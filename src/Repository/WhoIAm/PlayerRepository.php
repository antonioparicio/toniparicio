<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 *
 */
class PlayerRepository extends ServiceEntityRepository
{
    /**
     * how many results return for page in ranking
     */
    private const ITEMS_PER_PAGE = 50;
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

    /**
     * @return array
     */
    public function getRankingPage(Player $player, int $page = 0): array
    {
        $min = $page * self::ITEMS_PER_PAGE;
        $result = $this->createQueryBuilder('p')
            ->select('p.id, p.display, p.trophies, p.season, p.avatar')
            ->where('p.season != 0 AND p.locale = :locale AND (p.display IS NOT NULL OR p.id = :player)')
            ->setParameter('locale', $player->getLocale())
            ->setParameter('player', $player->getId())
            ->orderBy('p.season', 'DESC')
            ->setFirstResult($min)
            ->setMaxResults(self::ITEMS_PER_PAGE)
            ->getQuery()
            ->getArrayResult();

        $i = $min;
        $result = array_map(static function ($item) use (&$i) {
            $item['position'] = ++$i;
            return $item;
        }, $result);

        return $result;
    }
    
    /**
     * @param int|null $position : pass position if was calculated to avoid a query
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPlayerSeasonTable(Player $player, ?int $position = null): array
    {
        $position ??= $this->getPlayerSeasonPosition($player);
        $min = max($position - 10, 0);
        $max = $position + 10;

        $result = $this->createQueryBuilder('p')
            ->select('p.id, p.display, p.trophies, p.season, p.avatar')
            ->where('p.season != 0 AND p.locale = :locale AND (p.display IS NOT NULL OR p.id = :player)')
            ->setParameter('locale', $player->getLocale())
            ->setParameter('player', $player->getId())
            ->orderBy('p.season', 'DESC')
            ->setFirstResult($min)
            ->setMaxResults(21)
            ->getQuery()
            ->getArrayResult();

        $i = $min;
        $result = array_map(static function ($item) use (&$i) {
            $item['position'] = ++$i;
            return $item;
        }, $result);

        return $result;
    }

    /**
     * @return array
     */
    public function getPlayerSeasonPodium(string $locale): array
    {
        $result = $this->createQueryBuilder('p')
            ->select('p.id, p.display, p.trophies, p.season, p.avatar')
            ->where('p.season != 0 AND p.locale = :locale AND p.display IS NOT NULL')
            ->setParameter('locale', $locale)
            ->orderBy('p.season', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->getQuery()
            ->getArrayResult();

        $i = 1;
        $result = array_map(static function ($item) use (&$i) {
            $item['position'] = ++$i;
            return $item;
        }, $result);

        return $result;
    }

    /**
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPlayerSeasonPosition(Player $player): int
    {
        $result = $this->createQueryBuilder('g')
            ->select('COUNT(g.id) AS position')
            ->where('g.season > :season AND g.locale = :locale AND (g.display IS NOT NULL OR g.id = :player)')
            ->setParameter('season', $player->getSeason())
            ->setParameter('locale', $player->getLocale())
            ->setParameter('player', $player->getId())
            ->getQuery()
            ->getSingleResult();

        return (int)($result['position'] ?? 0) + 1;
    }

    /**
     * return position in season table by trophies
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getScoreSeasonPosition(int $trophies, string $locale): int
    {
        $result = $this->createQueryBuilder('g')
            ->select('COUNT(g.id) AS position')
            ->where('g.season > :trophies AND g.locale = :locale AND g.display IS NOT NULL')
            ->setParameter('trophies', $trophies)
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getSingleResult();

        return (int)($result['position'] ?? 0) + 1;
    }

    /**
     * @return array|int|string
     */
    public function getSeasonPlayers(string $locale): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.season > :trophies AND p.locale = :locale AND p.display IS NOT NULL')
            ->setParameter('trophies', 0)
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getResult();
    }
}
