<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class GameRepository
 * @package App\Repository\WhoIAm
 */
class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    /**
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTodayPosition(Game $game): int
    {
        $timeline = (new \DateTime())->setTime(0, 0, 0);
        $result = $this->createQueryBuilder('g')
            ->select('COUNT(g.id) AS position')
            ->where('g.trophies > :trophies AND g.created_at >= :created AND g.locale = :locale')
            ->setParameter('trophies', $game->getTrophies())
            ->setParameter('created', $timeline->getTimestamp())
            ->setParameter('locale', $game->getLocale())
            ->getQuery()
            ->getSingleResult();

        $position = (int)($result['position'] ?? 0);
        return $position + 1;
    }
}
