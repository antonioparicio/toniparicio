<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\GameRound;
use App\Entity\WhoIAm\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 *
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameRound::class);
    }

    /**
     * @return array
     */
    public function getWikipediaCategories(array $categories): array
    {
        return $this->findBy(['wikipedia' => $categories]);
    }
}
