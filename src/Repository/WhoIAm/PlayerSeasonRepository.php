<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\PlayerSeason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class PlayerSeasonRepository
 * @package App\Repository\WhoIAm
 */
class PlayerSeasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayerSeason::class);
    }
}
