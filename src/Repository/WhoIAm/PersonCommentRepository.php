<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonComment;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\PersonScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 *
 */
class PersonCommentRepository extends ServiceEntityRepository
{
    /**
     * @var int
     */
    private const PER_PAGE = 50;

    /**
     * PersonCommentRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonComment::class);
    }

    /**
     * fetches Persons that are updates since date
     *
     * @param PersonLocale $person
     * @return int
     */
    public function getCommentsCount(PersonLocale $person): int
    {
        try {
            $result = $this->createQueryBuilder('c')
                ->select('count(*) as comments')
                ->where('c.personLocale = :person')
                ->setParameter('person', $person->getId())
                ->getQuery()
                ->getSingleResult();
        } catch (\Exception) {
            return 0;
        }

        return (int)($result['comments'] ?? 0);
    }

    public function getComments(PersonLocale $person, int $page = 0): array
    {
        $result = $this->createQueryBuilder('c')
            ->select('c.id, c.comment, c.created_at, c.likes, c.dislikes, (c.likes - c.dislikes) AS diff, p.id AS user, p.display, p.avatar')
            ->innerJoin('c.player', 'p', Join::WITH, 'p.id = c.player')
            ->where('c.personLocale = :person')
            ->setParameter('person', $person->getId())
            ->orderBy('diff', 'DESC')
            ->setFirstResult($page * self::PER_PAGE)
            ->setMaxResults(self::PER_PAGE)
            ->getQuery()
            ->getArrayResult();

        return $result;
    }
}
