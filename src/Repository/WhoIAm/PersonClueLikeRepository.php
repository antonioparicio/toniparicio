<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\PersonClueLike;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class PersonClueLikeRepository
 * @package App\Repository\WhoIAm
 */
class PersonClueLikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonClueLike::class);
    }

    /**
     * return PersonClue resume info
     *
     * @param int|null $clue
     * @return array
     */
    public function getClueResume(PersonLocale $person, string $type, bool $workshop, ?int $clue): array
    {
        try {
            $result = $this->createQueryBuilder('c')
                ->select('SUM(c.likes) as likes, SUM(c.dislikes) as dislikes ')
                ->where('c.person = :person AND c.type = :type AND c.workshop = :workshop AND c.clue = :clue')
                ->setParameter('person', $person->getId())
                ->setParameter('type', $type)
                ->setParameter('clue', $clue)
                ->setParameter('workshop', $workshop)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException | NonUniqueResultException) {
            $result = ['likes' => 0, 'dislikes' => 0];
        }

        return $this->evaluateResult($result);
    }

    /**
     * @return array
     */
    public function evaluateResult(array $result): array
    {
        $likes = (int)$result['likes'];
        $dislikes = (int)$result['dislikes'];
        $result['approved'] = PersonClueLike::CLUE_WAITING;
        // for accept each dislike increase the required likes in a MINIMAL_LIKES_RELATION factor
        // i.e. if 10 likes to approved with 5 dislikes will be necessary 5+5*0.5+10=17.5~18 likes instead of 15
        $dislikesCorrected = $dislikes - ($dislikes + $dislikes * PersonClueLike::MINIMAL_LIKES_RELATION);
        if ($likes - $dislikesCorrected > PersonClueLike::LIKES_TO_ACCEPT) {
            $result['approved'] = PersonClueLike::CLUE_ACCEPTED;
        } elseif ($dislikes - $likes > PersonClueLike::DISLIKES_TO_REJECT) {
            $result['approved'] = PersonClueLike::CLUE_REJECTED;
        }
        $result['likes'] = $likes;
        $result['dislikes'] = $dislikes;
        return $result;
    }
}
