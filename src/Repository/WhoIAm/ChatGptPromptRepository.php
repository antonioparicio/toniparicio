<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\ChatGptPrompt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 *
 */
class ChatGptPromptRepository extends ServiceEntityRepository
{
    /**
     * how many results return for page in ranking
     */
    private const ITEMS_PER_PAGE = 50;
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChatGptPrompt::class);
    }
}
