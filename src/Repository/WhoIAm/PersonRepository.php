<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    /**
     * fetches Persons that are updates since date.
     */
    public function getUpdates(\DateTime $date): array
    {
        $result = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.updated_at >= :from')
            ->setParameter('from', $date->format('Y-m-d H:i:s'))
            ->orderBy('a.id')
            ->getQuery()
            ->getResult();

        return $result;
    }

    /** @return Person[] */
    public function getRandomPersons(int $maxResults = 5): array
    {
        $total = $this->count(['deleted_at' => null]);
        $first = rand(0, $total - $maxResults);
        $result = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.deleted_at IS NULL')
            ->setFirstResult($first)
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function getDuplicate(Person $person): ?Person
    {
        $duplicates = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.id != :person')
            ->andWhere('a.realName = :name')
            ->setParameter('person', $person->getId())
            ->setParameter('name', $person->getRealName())
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
        return $duplicates ? $duplicates[0] : null;
    }
}
