<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\ChatGptQueue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ChatGptQueueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChatGptQueue::class);
    }
}
