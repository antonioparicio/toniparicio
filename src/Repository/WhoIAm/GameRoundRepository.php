<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\GameRound;
use App\Entity\WhoIAm\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GameRoundRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameRound::class);
    }

    public function getPersonAvg(Person $person): int
    {
        try {
            $result = $this->createQueryBuilder('r')
                ->select('AVG(r.isSuccess) AS avg')
                ->where('r.person = :person')
                ->setParameter('person', $person->getId())
                ->getQuery()
                ->getSingleResult();
        } catch (\Exception) {
            return 0;
        }

        return (int)(($result['avg'] ?? 0) * 100);
    }

    public function getPersonScore(Person $person): int
    {
        try {
            $result = $this->createQueryBuilder('r')
                ->select('AVG(r.score) AS score')
                ->where('r.person = :person AND r.isSuccess = 1')
                ->setParameter('person', $person->getId())
                ->getQuery()
                ->getSingleResult();
        } catch (\Exception) {
            return 0;
        }

        return (int)($result['score'] ?? 0);
    }
}
