<?php

namespace App\Repository\WhoIAm;

use App\Entity\WhoIAm\PersonClueShare;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class PersonClueShareRepository.
 */
class PersonClueShareRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonClueShare::class);
    }
}
