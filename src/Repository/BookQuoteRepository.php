<?php

namespace App\Repository;

use App\Entity\BookQuote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method BookQuote|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookQuote|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookQuote[]    findAll()
 * @method BookQuote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookQuoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookQuote::class);
    }

    /**
    * @return BookQuote Returns a random quote
    */
    public function getOneRandom($amount = 7)
    {
        // count total active items on BookQuote
        $total = $this->createQueryBuilder('b')
            ->select('count(b.id)')
            ->andWhere('b.deleted_at IS NULL')
            ->getQuery()
            ->getSingleScalarResult();

        // get random offset
        // substract one to avoid overflow
        $offset = random_int(0, $total - 1);

        return $this->createQueryBuilder('b')
            ->andWhere('b.deleted_at IS NULL')
            ->setMaxResults(1)
            ->setFirstResult($offset)
            ->getQuery()
            ->getOneOrNullResult();
    }

//    /**
//     * @return BookQuote[] Returns an array of BookQuote objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BookQuote
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
