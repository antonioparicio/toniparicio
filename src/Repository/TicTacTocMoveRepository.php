<?php

namespace App\Repository;

use App\Entity\TicTacTocMove;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TicTacTocMove|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicTacTocMove|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicTacTocMove[]    findAll()
 * @method TicTacTocMove[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicTacTocMoveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicTacTocMove::class);
    }

//    /**
//     * @return TicTacTocMove[] Returns an array of TicTacTocMove objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicTacTocMove
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
