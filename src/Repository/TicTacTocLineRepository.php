<?php

namespace App\Repository;

use App\Entity\TicTacTocLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TicTacTocLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicTacTocLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicTacTocLine[]    findAll()
 * @method TicTacTocLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicTacTocLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicTacTocLine::class);
    }

//    /**
//     * @return TicTacTocLine[] Returns an array of TicTacTocLine objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicTacTocLine
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
