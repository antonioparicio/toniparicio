<?php

namespace App\Repository;

use App\Entity\ProjectAchievement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProjectAchievement|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectAchievement|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectAchievement[]    findAll()
 * @method ProjectAchievement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectAchievementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectAchievement::class);
    }

//    /**
//     * @return ProjectAchievement[] Returns an array of ProjectAchievement objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjectAchievement
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
