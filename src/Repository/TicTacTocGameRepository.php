<?php

namespace App\Repository;

use App\Entity\TicTacTocGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TicTacTocGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicTacTocGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicTacTocGame[]    findAll()
 * @method TicTacTocGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicTacTocGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicTacTocGame::class);
    }

//    /**
//     * @return TicTacTocGame[] Returns an array of TicTacTocGame objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicTacTocGame
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
