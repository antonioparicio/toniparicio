<?php

namespace App\Repository;

use App\Entity\TicTacTocRound;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TicTacTocRound|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicTacTocRound|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicTacTocRound[]    findAll()
 * @method TicTacTocRound[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicTacTocRoundRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicTacTocRound::class);
    }

//    /**
//     * @return TicTacTocRound[] Returns an array of TicTacTocRound objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicTacTocRound
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
