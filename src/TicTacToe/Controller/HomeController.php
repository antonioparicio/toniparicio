<?php

namespace App\TicTacToe\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    /**
     * @Route("/tictactoe", name="tictactoe")
     */
    public function index()
    {
        return $this->render('/tic_tac_toe/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
