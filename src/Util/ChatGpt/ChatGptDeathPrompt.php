<?php

namespace App\Util\ChatGpt;

final class ChatGptDeathPrompt extends ChatGptPrompt
{
    public function type(): string
    {
        return 'death';
    }

    public function prompt(): array
    {
        $locale = $this->personLocale();
        $sample = ['results' => '<death>'];
        if ('es' === $locale->getLocale()) {
            $query = "Dado el personaje \"{$locale->getName()}\", ".
                "también conocido como {$locale->getPerson()->getRealName()} ".
                'devuele información sobre su muerte, incluyendo fecha y lugar. '.
                'Añade información como la causa y/o el lugar de reposo solo si son importantes. '.
                'Omite cualquier dato desconocido y devuelve null si el personaje no ha fallecido. '.
                'No incluyas el nombre del personaje en el texto. '.
                'Redacta la muerte en primera persona,  simulando la personalidad del personaje, '.
                'con una longitúd máxima de 128, '.
                'en un objeto JSON con la clave "results" con esta estructura '.json_encode($sample);

            return [
                [
                    'role' => 'system',
                    'content' => 'Eres un experto en historia y personajes conocidos, '.
                        'responde de la forma más concisa posible con un límite de 128 caracteres',
                ],
                ['role' => 'user', 'content' => $query],
            ];
        }
        if ('en' === $locale->getLocale()) {
            $query = "Given the character \"{$locale->getName()}\", ".
                "also known as {$locale->getPerson()->getRealName()} ".
                'return information about his death, including date and place. '.
                'Add information about cause and/or resting place only if they are important. '.
                'Don\' include the character name in the response. '.
                'Ignore any unknown data and return null if the character is not deceased. '.
                'Write text in first person, simulating the character personality '.
                'with a maximum of 128 characters, '.
                'in a JSON object with the key "results" with this structure '.json_encode($sample);

            return [
                [
                    'role' => 'system',
                    'content' => 'You are a history and characters expert, '.
                        'answer in a concise way with a limit of 128 characteres.',
                ],
                ['role' => 'user', 'content' => $query],
            ];
        }
        throw new \RuntimeException(sprintf('Locale %s have not a valid %s prompt', $locale->getLocale(), $this->type()));
    }

    /**
     * persists facts into person locale.
     */
    public function post(): void
    {
        $person = $this->personLocale();
        $death = $this->response()->getOutput();

        if ($this->isValidDeath($death)) {
            $person->setDeath($death);
            $this->manager()->persist($person);
        }
        $this->manager()->flush();
    }

    private function isValidDeath($death): bool
    {
        if (!is_string($death)) {
            return false;
        }
        if (strlen($death) < 10) {
            return false;
        }
        if (strlen($death) > 256) {
            return false;
        }

        return true;
    }

    public function hasMinimum(): bool
    {
        return false;
    }
}
