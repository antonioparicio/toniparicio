<?php

namespace App\Util\ChatGpt;

use App\Entity\WhoIAm\ChatGptQueue;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Throwable;

final class ChatGptClient
{
    private const CHAT_GPT_API = 'https://api.openai.com/v1/';
    private Client $guzzleClient;

    public function __construct(
        private readonly EntityManagerInterface $manager,
        private LoggerInterface $logger,
        private readonly string $chatGptApiKey
    ) {
        $this->guzzleClient = new Client([
            'base_uri' => self::CHAT_GPT_API, // GPT-3.5 Turbo API base URL
        ]);
    }

    public function query(ChatGptPrompt $prompt, ?ChatGptQueue $queue): ChatGptPrompt
    {
        if ($prompt->hasMinimum()) {
            $error = sprintf(
                '[ChatGptClient] Person [%s] %s already have the minimum items for %s.',
                $prompt->personLocale()->getLocale(),
                $prompt->personLocale()->getName(),
                $prompt->type()
            );
            $this->logger->warning($error);
            $prompt->setResponse(ChatGptResponse::error(-1, $error));
            return $prompt;
        }
        try {
            $response = $this->guzzleClient->post('chat/completions', [
                'headers' => $this->getHeaders(),
                'json' => $prompt->request(),
            ]);

            $prompt = $prompt->setResponse(ChatGptResponse::build($response));
            $prompt->post();
        } catch (Throwable $e) {
            $this->logger->error(sprintf('[ChatGptClient] API ERROR %d: %s', $e->getCode(), $e->getMessage()));
            $prompt = $prompt->setResponse(ChatGptResponse::error($e->getCode(), $e->getMessage()));
        }
        $this->save($prompt, $queue);
        return $prompt;
    }

    private function getHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . $this->chatGptApiKey,
            'Content-Type' => 'application/json',
        ];
    }

    private function save(ChatGptPrompt $prompt, ?ChatGptQueue $queue = null): void
    {
        $model = new \App\Entity\WhoIAm\ChatGptPrompt();
        $model->setPerson($prompt->person())
            ->setPersonLocale($prompt->personLocale())
            ->setQueue($queue)
            ->setType($prompt->type())
            ->setInputTokens($prompt->inputTokens())
            ->setOutputTokens($prompt->outputTokens())
            ->setCost($model->cost())
            ->setIsSuccess($prompt->isSuccess())
            ->setResponse($prompt->response());

        $this->manager->persist($model);
        $this->manager->flush();
    }
}
