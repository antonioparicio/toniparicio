<?php

namespace App\Util\ChatGpt;

use App\Entity\WhoIAm\PersonFact;
use App\Entity\WhoIAm\Player;

final class ChatGptFactPrompt extends ChatGptPrompt
{
    public function type(): string
    {
        return 'fact';
    }

    public function prompt(): array
    {
        $locale = $this->personLocale();

        $query = "Given the character \"{$locale->getName()}\", ";
        if ($locale->getName() !== $locale->getPerson()->getRealName()) {
            $query .= "also known as \"{$locale->getPerson()->getRealName()}\", ";
        }
        if ($locale->getPerson()->getYear()) {
            $query .= "born in the year {$locale->getPerson()->getYear()}, ";
        }
        $query .= 'return a maximum of 5 achievements, curiosities or facts about the character. '.
            "Write the sentences exclusively in {$this->language()} and first person, ".
            "simulating the character's personality, ".
            'with a maximum of 304 characters and '.
            "don't use character name and ".
            'you NEVER use " in facts, only \’'
        ;

        return [
            [
                'role' => 'system',
                'content' => 'You are a history and characters expert, '.
                    'you MUST answer exclusively in '.$this->language(),
            ],
            [
                'role' => 'user',
                'content' => $query,
            ],
        ];
    }

    /**
     * persists facts into person locale.
     */
    public function post(): void
    {
        $player = $this->manager()->getRepository(Player::class)->find(1);
        $person = $this->personLocale();
        foreach ($this->response()->getOutput() as $fact) {
            $fact = $fact['fact'] ?? $fact;
            if ($this->isValidFact($fact)) {
                $fact = str_replace(['\'', '‘', '’'], '"', $fact);
                $personFact = new PersonFact();
                $personFact->setAuthor($player);
                $personFact->setPerson($person);
                $personFact->setFact($fact);
                $this->manager()->persist($personFact);
            }
        }
        $this->manager()->flush();
    }

    private function isValidFact($fact): bool
    {
        if (!is_string($fact)) {
            return false;
        }
        if (strlen($fact) < 10) {
            return false;
        }
        if (strlen($fact) > 304) {
            return false;
        }

        return true;
    }

    public function hasMinimum(): bool
    {
        return $this->personLocale()->totalFacts() >= self::MINIMUM;
    }

    public function functions(): array
    {
        return [
            [
                'name' => 'parse_response',
                'description' => 'Extract the facts from the input',
                'parameters' => [
                    'type' => 'object',
                    'properties' => [
                        'results' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'object',
                                'properties' => [
                                    'fact' => [
                                        'type' => 'string',
                                        'description' => "A character fact in {$this->language()}, you must replace ANY double-quote with single-quote",
                                    ],
                                ],
                                'required' => ['fact'],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
