<?php

namespace App\Util\ChatGpt;

use App\Entity\WhoIAm\Category;
use App\Entity\WhoIAm\ImageQueue;
use App\Entity\WhoIAm\PersonCategory;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\PersonWork;
use App\Entity\WhoIAm\PersonWorkType;
use App\Entity\WhoIAm\Player;

final class ChatGptWorkPrompt extends ChatGptPrompt
{
    public function type(): string
    {
        return 'work';
    }

    public function prompt(): array
    {
        $locale = $this->personLocale();
        // get prompt part depending on category
        $workPromptText = $this->getWorkPromptText($locale);
        if (null === $workPromptText) {
            throw new \RuntimeException(sprintf('%s is not candidate for works because categories', $locale->getName()));
        }
        if ('es' === $locale->getLocale()) {
            $query = "Dado el personaje \"{$locale->getName()}\", ".
                "también conocido como \"{$locale->getPerson()->getRealName()}\". ".
                $workPromptText;

            return [
                ['role' => 'system', 'content' => 'Eres un experto en historia, arte y letras.'],
                ['role' => 'user', 'content' => $query],
            ];
        }
        if ('en' === $locale->getLocale()) {
            $query = "Given the character \"{$locale->getName()}\", ".
                "also known as \"{$locale->getPerson()->getRealName()}\". ".
                $workPromptText;

            return [
                ['role' => 'system', 'content' => 'You are a history expert.'],
                ['role' => 'user', 'content' => $query],
            ];
        }
        throw new \RuntimeException(sprintf('Locale %s have not a valid %s prompt', $locale->getLocale(), $this->type()));
    }

    /** @param PersonCategory[] $categories */
    public function getWorkPromptText(PersonLocale $person): ?string
    {
        $work = null;
        $categories = $person->getPerson()->getCategories()->toArray();
        $mainCategory = $this->mainCategory($categories);

        $format = [
            'results' => [
                [
                    'title' => '<<<OBRA>>>',
                    'wikipedia' => '<<<LINK>>>',
                    'image' => '<<<IMAGE>>',
                    'type' => $mainCategory,
                ],
            ],
        ];
        if ('en' === $person->getLocale()) {
            $work = match ($mainCategory) {
                'painting', 'picture' => 'Return a maximum of 3 of your most important paintings. Include an image in the <IMAGE> link to a royalty-free image of the painting. ', //phpcs:ignore
                'literature' => 'Return a maximum of 3 of their most important works as a writer. ',
                'sculpture' => 'Return a maximum of 3 of their most important works as a sculptor. Include an image in the <IMAGE> link to a royalty-free image of the sculpture. ', // phpcs:ignore
                'military' => 'Return a maximum of 5 of their most important wars or military battles. ',
                'cinema' => 'Return a maximum of 5 of their most outstanding films or series. ',
                'music' => 'Return a maximum of 5 of their most important songs or albums. ',
                'architecture' => 'Return a maximum of 5 of their most important works as an architect. Include an image in the <IMAGE> link to a royalty-free image of the building. ', // phpcs:ignore
                'entrepreneur' => 'Return a maximum of 5 of their most important companies or invents.',
                default => null,
            };
            if ($work) {
                $work .= $this->promptJsonFormat('en', $format).
                    'If you cannot complete any key, set it as null';
            }
        }
        if ('es' === $person->getLocale()) {
            $work = match ($mainCategory) {
                'painting', 'picture' => 'Devuelve un máximo de 3 de sus obrás más importante como pintor. Incluye un enlace a una imagen de la obra, libre de derechos, en la clave "image". ', //phpcs:ignore
                'literature' => 'Devuelve un máximo de 2 de sus obrás más importante como escritor. ',
                'sculpture' => 'Devuelve un máximo de 5 de sus obrás más importante como escultor. Include un link a una imagen libre de derechos en <IMAGE> de la obra. ', //phpcs:ignore
                'military' => 'Devuelve un máximo de 5 de sus guerras o batallas militares más destacadas. ',
                'cinema' => 'Devuelve un máximo de 5 de sus peliculas o series más destacadas.  ',
                'music' => 'Devuelve un máximo de 5 de sus canciones o discos más importantes. ',
                'architecture' => 'Devuelve un máximo de 5 de sus obrás más importante como arquitecto. Include un link a una imagen libre de derechos en <IMAGE> de la obra. ', //phpcs:ignore
                'entrepreneur' => 'Devuelve un máximo de 5 de sus empresas o inventos más importantes. ',
                default => null,
            };
            if ($work) {
                $work .= $this->promptJsonFormat('es', $format).
                    'Si no puedes rellenar alguna clave, pásala como null';
            }
        }
        return $work;
    }

    private function mainCategory(array $categories): ?string
    {
        $priorities = ['military', 'literature', 'cinema', 'music', 'painting', 'picture', 'architecture', 'sculpture', 'entrepreneur'];
        $categories = array_map(static function (Category $category) {
            return $category->getName();
        }, $categories);

        $intersection = array_intersect($priorities, $categories);

        return array_shift($intersection) ?? array_shift($categories);
    }

    /**
     * persists facts into person locale.
     */
    public function post(): void
    {
        $player = $this->manager()->getRepository(Player::class)->find(1);
        $person = $this->personLocale();
        foreach ($this->response()->getOutput() as $work) {
            $categoryName = $this->mapCategories($work['type'] ?? 'unknown');
            $category = $this->manager()
                ->getRepository(Category::class)->findOneBy(['name' => $categoryName]);
            if (null === $category) {
                continue;
            }
            $type = $this->manager()->getRepository(PersonWorkType::class)
                ->findOneBy(['category' => $category->getId()]);
            if ($category && $type && $this->isValidWork($work)) {
                $personWork = new PersonWork();
                $personWork->setAuthor($player);
                $personWork->setPerson($person);
                $personWork->setType($type);
                $personWork->setTitle($work['title']);
                $personWork->setUrl($work['wikipedia'] ?? null);
                $person->addWork($personWork);

                if (isset($work['image']) && filter_var($work['image'], FILTER_VALIDATE_URL)) {
                    $queue = new ImageQueue();
                    $queue->setPerson($person->getPerson());
                    $queue->setType(ImageQueue::TYPE_WORK);
                    $queue->setPath($work['image']);
                    $queue->setMetadata(['title' => $work['title'], 'type' => $type->getName()]);
                    $this->manager()->persist($queue);
                }
                //$personWork->setImage($work['image'] ?? null);
                $this->manager()->persist($personWork);
            }
        }
        $this->manager()->flush();
    }

    private function mapCategories(string $category): string
    {
        $map = [
            'series' => 'cinema',
            'tv' => 'cinema',
            'company' => 'entrepreneur',
            'invent' => 'entrepreneur',
        ];

        return $map[$category] ?? $category;
    }

    private function isValidWork($work): bool
    {
        if (!is_array($work)) {
            return false;
        }
        if (!isset($work['title'])) {
            return false;
        }
        if (!is_string($work['title']) || strlen($work['title']) < 3 || strlen($work['title']) > 64) {
            return false;
        }

        return true;
    }

    public function hasMinimum(): bool
    {
        return $this->personLocale()->totalWorks() >= self::MINIMUM;
    }
}
