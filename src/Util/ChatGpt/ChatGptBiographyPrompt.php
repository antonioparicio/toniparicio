<?php

namespace App\Util\ChatGpt;

final class ChatGptBiographyPrompt extends ChatGptPrompt
{
    public function type(): string
    {
        return ChatGptService::TYPE_BIOGRAPHY;
    }

    public function prompt(): array
    {
        $locale = $this->personLocale();

        $query = "Given the character \"{$locale->getName()}\", ";
        if ($locale->getName() !== $locale->getPerson()->getRealName()) {
            $query .= "also known as \"{$locale->getPerson()->getRealName()}\", ";
        }
        if ($locale->getPerson()->getYear()) {
            $query .= "born in the year {$locale->getPerson()->getYear()}, ";
        }
        $query .= 'provide a detailed biography of the character.'.
            'I\'m interested in learning about their background, achievements, and significant life events.'.
            "Write the sentences exclusively in {$this->language()} and first person, ".
            "simulating the character's personality, ".
            'you NEVER use " in text, only \’. '.
            'Format the response with paragraphs and line breaks for readability.';

        return [
            [
                'role' => 'system',
                'content' => 'You are a history and characters biographer expert, '.
                    'you MUST answer exclusively in '.$this->language(),
            ],
            [
                'role' => 'user',
                'content' => $query,
            ],
        ];
    }

    /**
     * persists biography in description into person locale.
     */
    public function post(): void
    {
        $person = $this->personLocale();
        $output = $this->response()->getOutput();
        $biography = $output[ChatGptService::TYPE_BIOGRAPHY] ?? null;
        if ($this->isValidBiography($biography)) {
            $person->setDescription($biography);
            $this->manager()->persist($person);
            $this->manager()->flush();
        }
    }

    private function isValidBiography($fact): bool
    {
        if (!is_string($fact)) {
            return false;
        }
        if (strlen($fact) < 10) {
            return false;
        }
        if (strlen($fact) > 4096) {
            return false;
        }

        return true;
    }

    public function hasMinimum(): bool
    {
        return false;
    }

    public function functions(): array
    {
        return [
            [
                'name' => 'parse_response',
                'description' => 'Extract the biography from the input',
                'parameters' => [
                    'type' => 'object',
                    'properties' => [
                        'results' => [
                            'type' => 'object',
                            'properties' => [
                                'biography' => [
                                    'type' => 'string',
                                    'description' => "A character biography in {$this->language()}, you must replace ANY double-quote with single-quote. Format the response with paragraphs and line breaks for readability.",   //phpcs:ignore
                                ],
                            ],
                            'required' => [ChatGptService::TYPE_BIOGRAPHY],
                        ],
                    ],
                ],
            ],
        ];
    }
}
