<?php

namespace App\Util\ChatGpt;

use App\Util\Json\ExtractJsonFromString;
use App\Util\Json\JsonFixer;
use Exception;

final class ChatGptResponse
{
    private mixed $output;
    private int $error;
    private array $body;

    private function __construct($output, ?int $errorCode = null)
    {
        $this->output = $output;
        if ($errorCode) {
            $this->error = $errorCode;
        }
    }

    public function setBody(array $body): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Create ChatGptResponse from a HTTP Response.
     *
     * @throws Exception
     *
     * @see https://platform.openai.com/docs/api-reference/chat/object
     */
    public static function build($response): self
    {
        $body = $response->getBody();

        $result = json_decode($body, true, 512, JSON_UNESCAPED_UNICODE);

        if (isset($result['choices'][0]['message']['function_call']['arguments'])) {
            return self::processFunction($result);
        }

        return self::process($result);
    }

    /**
     * Create ChatGptResponse from a JSON.
     *
     * @throws Exception
     */
    public static function json($json): self
    {
        $result = json_decode($json, true);

        if (isset($result['choices'][0]['message']['function_call']['arguments'])) {
            return self::processFunction($result);
        }

        return self::process($result);
    }

    public static function fromContent(string $content): self
    {
        $result = [
            'choices' => [
                0 => [
                    'message' => [
                        'content' => $content,
                    ],
                ],
            ],
        ];

        return (new self($content))->setBody($result);
    }

    /** @throws Exception */
    private static function process(array $result): self
    {
        $content = $result['choices'][0]['message']['content'] ?? null;

        if ($content) {
            $contentArr = json_decode($content, true, 512, JSON_UNESCAPED_UNICODE)
                // sometimes ChatGPT return a broken JSON so we try to fix it
                ?? JsonFixer::fix($content);

            $contentArr = isset($contentArr['results']) ? $contentArr : self::fallback($content);

            if (!isset($contentArr['results'])) {
                throw new Exception(sprintf('Failed to generate text, no results: %s', json_encode($result)));
            }

            return (new self($contentArr['results']))->setBody($result);
        } else {
            throw new Exception(sprintf('Failed to generate text, no content: %s', json_encode($result)));
        }
    }

    /**
     * @throws Exception
     */
    private static function processFunction(array $result): self
    {
        $content = $result['choices'][0]['message']['function_call']['arguments'] ?? null;

        if ($content) {
            // try to fix issues with content and double-quotes
            $content = str_replace('\\\\\\"', '\'', $content);
            $contentArr = json_decode($content, true, 512, JSON_UNESCAPED_UNICODE) ?? JsonFixer::fix($content) ?? [];
            $results = $contentArr['results'] ?? null;
            if (null === $results) {
                throw new Exception(sprintf('Failed to generate text, no results: %s', json_encode($result)));
            }
            return (new self($results))->setBody($result);
        } else {
            throw new Exception(sprintf('Failed to generate text, no function arguments: %s', json_encode($result)));
        }
    }

    /**
     * Sometimes ChatGPT doesn't return a clean JSON but include some extra text, try again cleaning extra text.
     */
    private static function fallback(string $content): array
    {
        $json = ExtractJsonFromString::json($content);

        return json_decode($json, true, 512, JSON_UNESCAPED_UNICODE) ?? [];
    }

    public static function error($code, string $message): self
    {
        $arr = json_decode($message, true);
        $message = $arr['error']['message'] ?? $message;

        return new self($message, $code);
    }

    public function isError(): bool
    {
        return isset($this->error);
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function getOutput(): array|string
    {
        return $this->output;
    }

    public function getBody(): array
    {
        return $this->body ?? [];
    }

    public function __toString(): string
    {
        $output = is_string($this->output) ? $this->output : json_encode($this->output);
        if (isset($this->error)) {
            return "[ERROR $this->error] $output";
        }

        return $output;
    }
}
