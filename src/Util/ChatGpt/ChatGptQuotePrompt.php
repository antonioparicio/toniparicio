<?php

namespace App\Util\ChatGpt;

use App\Entity\WhoIAm\PersonQuote;
use App\Entity\WhoIAm\Player;

final class ChatGptQuotePrompt extends ChatGptPrompt
{
    public function type(): string
    {
        return 'quote';
    }

    public function prompt(): array
    {
        $locale = $this->personLocale();
        if ('es' === $locale->getLocale()) {
            $query = "Dado el personaje \"{$locale->getName()}\", ".
                "también conocido como {$locale->getPerson()->getRealName()} ".
                'devuelve un máximo de 5 de sus frases más conocidas, '.
                'con un máximo de 256 caracteres, '.
                'en un objeto JSON con la clave "results"';

            return [
                ['role' => 'system', 'content' => 'Eres un experto en historia y frases celebres.'],
                ['role' => 'user', 'content' => $query],
            ];
        }
        if ('en' === $locale->getLocale()) {
            $query = "Given the character \"{$locale->getName()}\", ".
                "also known as {$locale->getPerson()->getRealName()} ".
                'returns a maximum of 5 of his best-known phrases. '.
                'with a maximum of 256 characters, '.
                'in a JSON object with the key "results"';

            return [
                ['role' => 'system', 'content' => 'You are a history expert.'],
                ['role' => 'user', 'content' => $query],
            ];
        }
        throw new \RuntimeException(sprintf('Locale %s have not a valid %s prompt', $locale->getLocale(), $this->type()));
    }

    /**
     * persists facts into person locale.
     */
    public function post(): void
    {
        $player = $this->manager()->getRepository(Player::class)->find(1);
        $person = $this->personLocale();
        foreach ($this->response()->getOutput() as $quote) {
            $quote = $this->processQuote($quote);
            if ($this->isValidQuote($quote)) {
                $personQuote = new PersonQuote();
                $personQuote->setAuthor($player);
                $personQuote->setPerson($person);
                $personQuote->setQuote($quote);
                $this->manager()->persist($personQuote);
            }
        }
        $this->manager()->persist($person);
        $this->manager()->flush();
    }

    private function processQuote(mixed $input): ?string
    {
        if (is_string($input)) {
            return $input;
        }
        if (is_array($input)) {
            return $input['quote'] ?? $input['frase'] ?? $input['cita'] ?? null;
        }

        return null;
    }

    private function isValidQuote($quote): bool
    {
        if (!is_string($quote)) {
            return false;
        }
        if (strlen($quote) < 10) {
            return false;
        }
        if (strlen($quote) > 512) {
            return false;
        }

        return true;
    }

    public function hasMinimum(): bool
    {
        return $this->personLocale()->totalQuotes() >= self::MINIMUM;
    }
}
