<?php

namespace App\Util\ChatGpt;

use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLocale;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

abstract class ChatGptPrompt
{
    /** @var int max 4097 */
    private const DEFAULT_MAX_TOKENS = 1000;
    public const MINIMUM = 3;

    private ChatGptResponse $response;
    private PersonLocale $personLocale;
    private int $inputTokens = 0;
    private int $outputTokens = 0;

    private mixed $output;
    private bool $isSuccess;

    public function __construct(private EntityManagerInterface $entityManager, private LoggerInterface $logger)
    {
    }

    public function setPerson(PersonLocale $personLocale): self
    {
        $this->personLocale = $personLocale;
        // reset prompt
        $this->inputTokens = 0;
        $this->outputTokens = 0;
        $this->output = null;
        unset($this->response, $this->isSuccess);

        return $this;
    }

    abstract public function type(): string;

    abstract public function prompt(): array;

    abstract public function post(): void;

    public function functions(): array
    {
        return [];
    }

    abstract public function hasMinimum(): bool;

    public function person(): Person
    {
        return $this->personLocale->getPerson();
    }

    public function personLocale(): PersonLocale
    {
        return $this->personLocale;
    }

    public function language(): string
    {
        return match ($this->personLocale()->getLocale()) {
            'es' => 'Spanish',
            default => 'English'
        };
    }

    public function manager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @see https://platform.openai.com/docs/api-reference/chat/object
     */
    public function setResponse(ChatGptResponse $response): self
    {
        $this->response = $response;
        $body = $response->getBody();
        $this->isSuccess = ($body['choices'][0]['finish_reason'] ?? 'error') === 'stop';
        $this->output = $response->getOutput();
        $this->inputTokens = $body['usage']['prompt_tokens'] ?? 0;
        $this->outputTokens = $body['usage']['completion_tokens'] ?? 0;

        $this->logger->warning(
            sprintf(
                'ChatGptResponse: %s',
                is_array($this->output) ? json_encode($this->output) : $this->output
            )
        );

        return $this;
    }

    public function request(): array
    {
        $config = [
            'model' => 'gpt-3.5-turbo-0613', // gpt-3.5-turbo
            'messages' => $this->prompt(),
            'temperature' => 1,
            'top_p' => 1,
            'max_tokens' => $this->maxTokens(),
        ];
        $functions = $this->functions();
        if ([] !== $functions) {
            $config['functions'] = $functions;
            $config['function_call'] = [
                'name' => $functions[0]['name'],
            ];
        }
        return $config;
    }

    public function response(): ChatGptResponse
    {
        return $this->response;
    }

    public function output(): mixed
    {
        return $this->output;
    }

    public function maxTokens(): int
    {
        return self::DEFAULT_MAX_TOKENS;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function inputTokens(): int
    {
        return $this->inputTokens;
    }

    public function outputTokens(): int
    {
        return $this->outputTokens;
    }

    public function tokens(): int
    {
        return $this->inputTokens + $this->outputTokens;
    }

    protected function promptJsonFormat(string $locale = 'es', array $sample = []): string
    {
        if ('en' === $locale) {
            return 'Inside content return only a JSON object with key "results" '.
                'with this format '.json_encode($sample).', without line breaks. '.
                'Use only single quotes escaped with backslash like this example: '.
                '{"key": "text with \'single quotes\'"}. '.
                'If you cannot return a response return a "error" key instead "results" with the error message.'
            ;
        }

        return 'Dentro de la clave content devuelve únicamente un objeto JSON con la clave "results" '.
            'con la siguiente estructura '.json_encode($sample).', sin incluir saltos de línea. '.
            'Utiliza solo comillas simples correctamente escapadas con contrabarra como en este ejemplo: '.
            '{"clave": "texto con \'comillas\'"}. '.
            'Si no puedes dar una respuesta devuelve una clave "error" en lugar de "results" con el mensaje de error. '
        ;
    }
}
