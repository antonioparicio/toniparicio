<?php

namespace App\Util\ChatGpt;

use App\Entity\WhoIAm\ChatGptQueue;
use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\ChatGptPromptRepository;
use App\Repository\WhoIAm\ChatGptQueueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

final class ChatGptService
{
    private const DEFAULT_TYPES = [
        self::TYPE_FACTS,
        self::TYPE_QUOTES,
        self::TYPE_WORKS,
        self::TYPE_DEATH,
        self::TYPE_BIOGRAPHY,
    ];
    public const TYPE_FACTS = 'fact';
    public const TYPE_QUOTES = 'quote';
    public const TYPE_WORKS = 'work';
    public const TYPE_DEATH = 'death';
    public const TYPE_BIOGRAPHY = 'biography';

    public function __construct(
        private readonly ChatGptClient     $client,
        private readonly ChatGptFactPrompt $factPrompt,
        private readonly ChatGptQuotePrompt $quotePrompt,
        private readonly ChatGptWorkPrompt  $workPrompt,
        private readonly ChatGptDeathPrompt $deathPrompt,
        private readonly ChatGptBiographyPrompt $biographyPrompt,
        private readonly ChatGptPromptRepository $repository,
        private readonly ChatGptQueueRepository $queueRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface   $logger
    ) {
    }

    /**
     * @param PersonLocale[] $persons
     * @param string|null $type type of clue to populate or null for all
     */
    public function get(array $persons, ?string $type = null, ?ChatGptQueue $queue = null, bool $force = false): bool
    {
        foreach ($persons as $index => $person) {
            if (!$this->single($person, $type, $queue, $force)) {
                $this->logger->error(sprintf('Aborting after %d/%d persons processes', $index, count($persons)));
                return false;
            }
        }
        return true;
    }

    /**
     * @param PersonLocale[] $persons
     * @param string|null $target type of clue to populate or null for all
     */
    public function queue(array $persons, string $target = null, bool $force = false): void
    {
        $types = $target ? [$target] : self::DEFAULT_TYPES;
        foreach ($persons as $person) {
            if ($force) {
                $this->cleanPreviousQueue($person, $target);
            }
            // queue each type as a different request in queue to limit requests
            foreach ($types as $type) {
                if ($this->canQueue($person, $type, $target)) {
                    $queue = new ChatGptQueue();
                    $queue->setPerson($person->getPerson())
                        ->setPersonLocale($person)
                        ->setType($type)
                        ->setIsHandled(false);
                    $this->entityManager->persist($queue);
                }
            }
        }
        $this->entityManager->flush();
    }

    private function cleanPreviousQueue(PersonLocale $personLocale, string $target = null): void
    {
        $query = ['personLocale' => $personLocale->getId()];
        if ($target) {
            $query['type'] = $target;
        }
        foreach ($this->queueRepository->findBy($query) as $queue) {
            $this->entityManager->remove($queue);
        }
        foreach ($this->repository->findBy($query) as $prompt) {
            $this->entityManager->remove($prompt);
        }
        $this->entityManager->flush();
    }

    private function single(
        PersonLocale $person,
        ?string $type = null,
        ?ChatGptQueue $queue = null,
        bool $force = false
    ): bool {
        $success = true;
        if ($this->isProcessable($person, self::TYPE_FACTS, $type, $force)) {
            $success = $this->facts($person, $queue);
        }
        if ($success && $this->isProcessable($person, self::TYPE_QUOTES, $type, $force)) {
            $success = $this->quotes($person, $queue);
        }
        if ($success && $this->isProcessable($person, self::TYPE_WORKS, $type, $force)) {
            $success = $this->works($person, $queue);
        }
        if ($success && $this->isProcessable($person, self::TYPE_DEATH, $type, $force)) {
            $success = $this->death($person, $queue);
        }
        if ($success && $this->isProcessable($person, self::TYPE_BIOGRAPHY, $type, $force)) {
            $success = $this->biography($person, $queue);
        }

        $person->setClues();
        if (!$person->isPlayable() && $person->getClues() >= PersonLocale::PLAYABLE_CLUES) {
            $person->setIsPlayable(true);
            $person->setDifficulty(PersonLocale::DEFAULT_DIFFICULTY);

            $this->save($person);
        }

        return $success;
    }

    private function save(PersonLocale $personLocale): void
    {
        if (!$this->entityManager->getConnection()->isConnected()) {
            // try reconnect if no connected
            $this->entityManager->getConnection()->connect();
        }
        $this->entityManager->persist($personLocale);
        $this->entityManager->flush();
    }

    private function facts(PersonLocale $person, ?ChatGptQueue $queue = null): bool
    {
        $prompt = $this->factPrompt->setPerson($person);
        $result = $this->client->query($prompt, $queue);

        if ($result->isSuccess()) {
            $this->save($person);
        }
        return $result->isSuccess();
    }

    private function quotes(PersonLocale $person, ?ChatGptQueue $queue = null): bool
    {
        $prompt = $this->quotePrompt->setPerson($person);
        $result = $this->client->query($prompt, $queue);

        if ($result->isSuccess()) {
            $this->save($person);
        }
        return $result->isSuccess();
    }

    private function works(PersonLocale $person, ?ChatGptQueue $queue = null): bool
    {
        $prompt = $this->workPrompt->setPerson($person);
        $result = $this->client->query($prompt, $queue);

        if ($result->isSuccess()) {
            $this->save($person);
        }
        return $result->isSuccess();
    }

    private function death(PersonLocale $person, ?ChatGptQueue $queue = null): bool
    {
        $prompt = $this->deathPrompt->setPerson($person);
        $result = $this->client->query($prompt, $queue);

        if ($result->isSuccess()) {
            $this->save($person);
        }
        return $result->isSuccess();
    }

    private function biography(PersonLocale $person, ?ChatGptQueue $queue = null): bool
    {
        $prompt = $this->biographyPrompt->setPerson($person);
        $result = $this->client->query($prompt, $queue);

        if ($result->isSuccess()) {
            $this->save($person);
        }
        return $result->isSuccess();
    }

    private function canQueue(PersonLocale $personLocale, string $type, ?string $target): bool
    {
        if ($target && $target !== $type) {
            // type must be the same than target or target must be null to queue all types
            return false;
        }
        if ($this->isPreviouslySearched($personLocale, $type)) {
            return false;
        }
        if ($this->isQueued($personLocale, $type)) {
            return false;
        }
        return true;
    }

    private function isProcessable(PersonLocale $personLocale, string $type, ?string $target, bool $force = false): bool
    {
        if ($target && $target !== $type) {
            // type must be the same than target or target must be null to queue all types
            return false;
        }
        if (!$force && $this->isPreviouslySearched($personLocale, $type)) {
            return false;
        }
        return true;
    }

    private function isPreviouslySearched(PersonLocale $person, string $type): bool
    {
        if ($this->repository->findOneBy(['personLocale' => $person->getId(), 'type' => $type])) {
            $this->logger->warning(
                sprintf(
                    'PersonLocale [%d] %s ignored because %s already searched in ChatGPT, please delete chatgpt_prompt if you want perform new searches', // phpcs:ignore
                    $person->getId(),
                    $person->getName(),
                    $type
                )
            );
            return true;
        }
        return false;
    }


    private function isQueued(PersonLocale $person, string $type): bool
    {
        if ($this->queueRepository->findOneBy([
            'personLocale' => $person->getId(),
            'type' => $type,
            'isHandled' => false
        ])) {
            $this->logger->warning(
                sprintf(
                    'PersonLocale [%d] %s ignored because already in queue for type %s',
                    $person->getId(),
                    $person->getName(),
                    $type
                )
            );
            return true;
        }
        return false;
    }
}
