<?php

namespace App\Util\Json;

final class ExtractJsonFromString
{
    private const REGEX = <<<EOF
            /
            \{              # { character
                (?:         # non-capturing group
                    [^{}]   # anything that is not a { or }
                    |       # OR
                    (?R)    # recurses the entire pattern
                )*          # previous group zero or more times
            \}              # } character
            /x
        EOF;

    /**
     * Receive a string with a JSON inside and return a new string only with JSON
     * Example: 'This is a string with a {"key": "value"}' => '{"key": "value"}'
     * Return null if no valid JSON found inside string.
     */
    public static function json(string $string): ?string
    {
        preg_match_all(self::REGEX, $string, $matches);

        return $matches[0][0] ?? null;
    }
}
