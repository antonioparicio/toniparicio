<?php


namespace App\Util\Wikipedia;


use JMS\Serializer\Exception\Exception;

class WikiQuoteUtils
{
    private const LENGTH_LIMIT = 256;

    /**
     * WikiQuoteUtils constructor.
     * @param $html
     */
    public function __construct($html)
    {
        $this->html = $html;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function find(string $locale): array
    {
        if (!$this->html) {
            return [];
        }

        $dom = new \DOMDocument('1.0', 'UTF-8');
        try {
            $dom->loadHTML(mb_convert_encoding((string) $this->html, 'HTML-ENTITIES', 'UTF-8'));
        } catch (\Exception $exception) {
            throw  $exception;
        }
        $nodes = $dom->getElementsByTagName('li');
        $candidates = [];
        foreach ($nodes as $node) {
            /** @var \DOMElement $node */
            $candidates[] = $node->nodeValue;
        }
        if ($locale === 'es') {
            // get only items between unicode quote chars
            $candidates = array_filter($candidates, static function ($candidate) {
                $char = mb_substr($candidate, 0, 1);
                return $char === '«' && strlen($candidate) <= self::LENGTH_LIMIT;
            });
            // get quote between unicode characteres («, »)
            $candidates = array_map(static function ($candidate) {
                if ($candidate[0] === '↑' || str_starts_with($candidate, 'Fuente')) {
                    return null;
                }
                if (preg_match('/«(.*?)»/u', $candidate, $match) === 1) {
                    return $match[1] ?? null;
                }
                return null;
            }, $candidates);
        } else if ($locale === 'en') {
            $candidates = array_map(static function ($candidate) {
                $parts = explode(PHP_EOL, $candidate);
                return count($parts) > 1 && strlen($parts[0]) <= self::LENGTH_LIMIT ? $parts[0] : null;
            }, $candidates);
        }
        // finally remove possible empty quotes
        return array_filter($candidates);
    }
}