<?php

namespace App\Util\Wikipedia;

use JMS\Serializer\Exception\Exception;

class WikipediaPerson
{
    use WikiDataUtils;

    final public const IMAGE_CDN = 'https://upload.wikimedia.org/wikipedia/commons';
    final public const LANGS = ['es', 'en'];
    // https://www.wikidata.org/wiki/Wikidata:List_of_properties/es
    // https://www.wikidata.org/wiki/Property:P570
    private const PROP_REAL_NAME = 'P1477';
    private const PROP_NATIVE_NAME = 'P1559';
    private const PROP_BIRTH_DATE = 'P569';
    private const PROP_BIRTH_PLACE = 'P19';
    private const PROP_DEATH_DATE = 'P570';
    private const PROP_DEATH_PLACE = 'P20';
    private const PROP_DEATH_CAUSE = 'P509';
    private const PROP_DEATH_MANNER = 'P1196';
    final public const PROP_IMAGE = 'P18';
    private const PROP_GENDER = 'P21';
    private const PROP_CATEGORIES = 'P106';
    private const PROP_WORKS = 'P800';
    final public const PROP_COUNTRY = 'P17';
    final public const PROP_COUNTRY_ISO3 = 'P298';
    final public const PROP_PROVINCE = 'P131';
    final public const PROP_WORK_TYPE = 'P31';
    final public const PROP_AWARDS = 'P166';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var int
     */
    protected $pageId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $titles = [];

    /**
     * @var array
     */
    protected $extract = [];

    /**
     * @var array
     */
    protected $localities = [];

    /**
     * @var array
     */
    protected $provinces = [];

    /**
     * @var array
     */
    protected $region = [];

    /**
     * @var array
     */
    protected $countries = [];

    /**
     * @var string
     */
    protected $image;

    /**
     * @var int
     */
    protected $year;

    /**
     * @var int
     */
    protected $month;

    /**
     * @var int
     */
    protected $day;

    /**
     * @var string
     */
    protected $continent;

    /**
     * @var string
     */
    protected $birthPlace;

    /**
     * @var array
     */
    protected $death = [];

    /**
     * @var string
     */
    protected $deathDate;

    /**
     * @var string
     */
    protected $deathPlace;

    /**
     * @var string
     */
    protected $deathCause;

    /**
     * @var string
     */
    protected $deathManner;

    /**
     * @var int
     */
    protected $gender = 0;

    /**
     * @var array
     */
    protected $categories = [];

    /**
     * @var array
     */
    protected $works = [];

    private array $quotes = [];

    /**
     * @return WikipediaPerson|null
     */
    public static function hydrate(array $array): ?WikipediaPerson
    {
        $query = $array['query'] ?? [];
        $pages = $query['pages'] ?? [];
        $page = array_shift($pages);
        if ($page) {
            $item = new static;
            $item->pageId = $page['pageid'] ?? null;
            $item->name = $page['title'] ?? null;
            $item->setImage($page['pageprops']['page_image_free'] ?? null);
            $item->id = $page['pageprops']['wikibase_item'] ?? null;
            $item->setExtract($page['extract'] ?? null, 'es');
            $links = $page['langlinks'] ?? [];
            foreach ($links as $lang) {
                if (in_array($lang['lang'], self::LANGS, true)) {
                    $item->titles[$lang['lang']] = $lang['*'];
                }
            }

            if ($item->id) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @param $data
     * @return WikipediaPerson
     */
    public function setWikidata(array $data): WikipediaPerson
    {
        $claims = $data['claims'] ?? [];
        try {
            $this->processName($claims);
        } catch (Exception $exception) {

        }
        try {
            $this->processBirthDate($claims);
        } catch (Exception $exception) {

        }
        try {
            $this->processBirthPlace($claims);
        } catch (Exception $exception) {

        }
        try {
            $this->processDeathDate($claims);
        } catch (Exception $exception) {

        }
        try {
            $this->processDeathPlace($claims);
        } catch (Exception $exception) {

        }
        try {
            $this->processDeathManner($claims);
        } catch (Exception $exception) {

        }
        try {
            $this->processDeathCause($claims);
        } catch (Exception) {

        }
        try {
            $this->processGender($claims);
        } catch (Exception) {

        }
        try {
            $this->processCategories($claims);
        } catch (Exception) {

        }
        try {
            $this->processWorks($claims);
        } catch (Exception) {

        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processName(array $claims): WikipediaPerson
    {
        $snak = $this->getSnak(self::PROP_REAL_NAME, $claims);
        if (!$snak) {
            $snak = $this->getSnak(self::PROP_NATIVE_NAME, $claims);
        }
        $this->name = $snak['datavalue']['value']['text'] ?? $this->name;

        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processBirthDate(array $claims): WikipediaPerson
    {
        if ($snak = $this->getSnak(self::PROP_BIRTH_DATE, $claims)) {
            $value = $snak['datavalue']['value']['time'] ?? null;
            if ($value && $date = strtok($value, 'T')) {
                $arr = explode('-', $date);
                if (count($arr) === 3) {
                    [$year, $month, $day] = $arr;
                    if ((int)$year !== 0) {
                        $this->setYear((int)$year);
                    }
                    if ((int)$month !== 0) {
                        $this->setMonth((int)$month);
                    }
                    if ((int)$day !== 0) {
                        $this->setDay((int)$day);
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processBirthPlace(array $claims): WikipediaPerson
    {
        if ($snak = $this->getSnak(self::PROP_BIRTH_PLACE, $claims)) {
            $this->birthPlace = $snak['datavalue']['value']['id'] ?? null;
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processDeathDate(array $claims): WikipediaPerson
    {
        if ($snak = $this->getSnak(self::PROP_DEATH_DATE, $claims)) {
            $value = $snak['datavalue']['value']['time'] ?? null;
            $this->deathDate = strtok($value, 'T');
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processDeathPlace(array $claims): WikipediaPerson
    {
        if ($snak = $this->getSnak(self::PROP_DEATH_PLACE, $claims)) {
            $this->deathPlace = $snak['datavalue']['value']['id'] ?? null;
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processDeathCause(array $claims): WikipediaPerson
    {
        if ($snak = $this->getSnak(self::PROP_DEATH_CAUSE, $claims)) {
            $this->deathCause = $snak['datavalue']['value']['id'] ?? null;
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processDeathManner(array $claims): WikipediaPerson
    {
        if ($snak = $this->getSnak(self::PROP_DEATH_MANNER, $claims)) {
            $this->deathManner = $snak['datavalue']['value']['id'] ?? null;
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function processGender(array $claims): WikipediaPerson
    {
        if ($snak = $this->getSnak(self::PROP_GENDER, $claims)) {
            $value = $snak['datavalue']['value']['numeric-id'];
            if ($value === 6_581_097) { // https://www.wikidata.org/wiki/Q6581097
                $this->setGender(1);   // male
            } elseif ($value === 6_581_072) {
                $this->setGender(2);   // female
            } else {
                $this->setGender(0);   //
            }
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    private function processCategories(array $claims): WikipediaPerson
    {
        if ($property = $this->getProperty(self::PROP_CATEGORIES, $claims)) {
            $categories = $this->getIds($property);
            if ($categories) {
                $this->setCategories($categories);
            }
        }
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    private function processWorks(array $claims): WikipediaPerson
    {
        if ($property = $this->getProperty(self::PROP_WORKS, $claims)) {
            $works = $this->getIds($property);
            if ($works) {
                $this->setWorks($works);
            }
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return WikipediaPerson
     */
    public function setId(string $id): WikipediaPerson
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getPageId(): int
    {
        return $this->pageId;
    }

    /**
     * @return WikipediaPerson
     */
    public function setPageId(int $pageId): WikipediaPerson
    {
        $this->pageId = $pageId;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return WikipediaPerson
     */
    public function setImage(?string $image): WikipediaPerson
    {
        if ($image) {
            $this->image = $this->getImagePath($image);
        }
        return $this;
    }

    /**
     * @return int|null
     */
    public function getYear(): ?int
    {
        return $this->year;
    }

    /**
     * @return WikipediaPerson
     */
    public function setYear(int $year): WikipediaPerson
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMonth(): ?int
    {
        return $this->month;
    }

    /**
     * @return WikipediaPerson
     */
    public function setMonth(int $month): WikipediaPerson
    {
        $this->month = $month;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDay(): ?int
    {
        return $this->day;
    }

    /**
     * @return WikipediaPerson
     */
    public function setDay(int $day): WikipediaPerson
    {
        $this->day = $day;
        return $this;
    }

    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }

    /**
     * @return WikipediaPerson
     */
    public function setGender(int $gender): WikipediaPerson
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @return WikipediaPerson
     */
    public function setCategories(array $categories): WikipediaPerson
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return array
     */
    public function getWorks(): array
    {
        return $this->works;
    }

    /**
     * @return WikipediaPerson
     */
    public function setWorks(array $works): WikipediaPerson
    {
        $this->works = $works;
        return $this;
    }

    /**
     * @return array
     */
    public function getExtracts(): array
    {
        return $this->extract;
    }

    /**
     * @return string
     */
    public function getExtract(string $lang = 'es'): ?string
    {
        return $this->extract[$lang] ?? null;
    }

    /**
     * @return string
     */
    public function getTitle(string $lang = 'es'): ?string
    {
        return $this->titles[$lang] ?? $this->getName();
    }

    /**
     * @param string $extract
     * @return WikipediaPerson
     */
    public function setExtract(?string $extract, string $lang = 'es'): WikipediaPerson
    {
        if ($extract) {
            // remove references [1],[10],[a] but no [a text]
            $pattern = '/\[(\d+|[a-z])\]/';
            $extract = preg_replace($pattern, '', $extract);
        }

        $this->extract[$lang] = $extract;
        return $this;
    }

    /**
     * @return array
     */
    public function getTitles(): array
    {
        return $this->titles;
    }

    /**
     * @return WikipediaPerson
     */
    public function setTitles(array $titles): WikipediaPerson
    {
        $this->titles = $titles;
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function setTitle(string $title, string $locale): WikipediaPerson
    {
        $this->titles[$locale] = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    /**
     * @return WikipediaPerson
     */
    public function setBirthPlace(string $birthPlace): WikipediaPerson
    {
        $this->birthPlace = $birthPlace;
        return $this;
    }

    /**
     * @return string
     */
    public function getContinent(): ?string
    {
        return $this->continent;
    }

    /**
     * @return WikipediaPerson
     */
    public function setContinent(string $continent): WikipediaPerson
    {
        $this->continent = $continent;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeathDate(): ?string
    {
        return $this->deathDate;
    }

    /**
     * @return WikipediaPerson
     */
    public function setDeathDate(string $deathDate): WikipediaPerson
    {
        $this->deathDate = $deathDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeathPlace(): ?string
    {
        return $this->deathPlace;
    }

    /**
     * @return WikipediaPerson
     */
    public function setDeathPlace(string $deathPlace): WikipediaPerson
    {
        $this->deathPlace = $deathPlace;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeathCause(): ?string
    {
        return $this->deathCause;
    }

    /**
     * @return WikipediaPerson
     */
    public function setDeathCause(string $deathCause): WikipediaPerson
    {
        $this->deathCause = $deathCause;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeathManner(): string
    {
        return $this->deathManner;
    }

    /**
     * @return WikipediaPerson
     */
    public function setDeathManner(string $deathManner): WikipediaPerson
    {
        $this->deathManner = $deathManner;
        return $this;
    }

    /**
     * @return array
     */
    public function getLocalities(): array
    {
        return $this->localities;
    }

    /**
     * @return string|null
     */
    public function getLocality(string $locale): ?string
    {
        return $this->localities[$locale] ?? null;
    }

    /**
     * @return string|null
     */
    public function getProvince(string $locale): ?string
    {
        return $this->provinces[$locale] ?? null;
    }

    /**
     * @return string|null
     */
    public function getCountry(string $locale): ?string
    {
        return $this->countries[$locale] ?? null;
    }

    /**
     * @return WikipediaPerson
     */
    public function setLocalities(array $localities): WikipediaPerson
    {
        $this->localities = $localities;
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function setLocality(string $locality, string $locale): WikipediaPerson
    {
        $this->localities[$locale] = $locality;
        return $this;
    }

    /**
     * @return array
     */
    public function getProvinces(): array
    {
        return $this->provinces;
    }

    /**
     * @return WikipediaPerson
     */
    public function setProvinces(array $provinces): WikipediaPerson
    {
        $this->provinces = $provinces;
        return $this;
    }

    /**
     * @return array
     */
    public function getRegion(): array
    {
        return $this->region;
    }

    /**
     * @return WikipediaPerson
     */
    public function setRegion(array $region): WikipediaPerson
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return array
     */
    public function getCountries(): array
    {
        return $this->countries;
    }

    /**
     * @return WikipediaPerson
     */
    public function setCountries(array $countries): WikipediaPerson
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return WikipediaPerson
     */
    public function setName(string $name): WikipediaPerson
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function setQuotes(array $quotes, string $locale): WikipediaPerson
    {
        $this->quotes[$locale] = $quotes;
        return $this;
    }

    /**
     * @return WikipediaPerson
     */
    public function setDeath(string $death, string $locale): WikipediaPerson
    {
        $this->death[$locale] = $death;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeath(string $locale): ?string
    {
        return $this->death[$locale] ?? null;
    }

    /**
     * @return array
     */
    public function getQuotes(string $locale): array
    {
        return $this->quotes[$locale] ?? [];
    }

    /**
     * @return int|null
     */
    public function getDeathAge(): ?int
    {
        if ($this->year && $this->month && $this->day && $this->deathDate) {
            $parts = explode('-', $this->deathDate);
            $year = (int)$parts[0];

            $years = abs($year - $this->year);
            if ($parts[1] < $this->month || ((int)$parts[1] === $this->month && (int)$parts[2] < $this->day)) {
                $years--;
            }
            return $years;
        }
        return null;
    }
}