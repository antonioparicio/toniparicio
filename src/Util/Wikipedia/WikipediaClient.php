<?php

namespace App\Util\Wikipedia;

use App\Entity\Country;
use App\Entity\WhoIAm\PersonWorkType;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

class WikipediaClient
{
    use WikiDataUtils;

    private const WIKIPEDIA = 'https://%s.wikipedia.org/w/api.php';
    private const WIKIQUOTE = 'https://%s.wikiquote.org/w/api.php';
    private const WIKIDATA = 'https://www.wikidata.org/w/api.php';

    /**
     * WikipediaClient constructor.
     */
    public function __construct(private readonly EntityManagerInterface $manager, private readonly LoggerInterface $logger)
    {
    }

    /** @throws JsonException|RuntimeException */
    public function processUrl(string $url, string $locale = 'es'): ?WikipediaPerson
    {
        // https://en.wikipedia.org/wiki/Enrico_Fermi
        if (!str_contains($url, 'wikipedia.org')) {
            $url = 'https://'.$locale.'.wikipedia.org/wiki/' . rawurlencode($url);
            if (!$this->urlExists($url)) {
                throw new RuntimeException('Not a wikipedia valid link or cannot be found');
            }
        }
        $locale = null;
        $title = null;
        if (preg_match('/\/\/(.*?)\.wikipedia/', $url, $match) === 1) {
            $locale = $match[1];
        }
        if (!$locale) {
            throw new RuntimeException('Wikipedia locale not found');
        }
        if (preg_match('/wiki\/(.*?)(\?|\z)/', $url, $match) === 1) {
            $title = urldecode($match[1]);
        }
        if (!$title) {
            throw new RuntimeException('Wikipedia title not found');
        }
        return $this->getPerson($title, $locale);
    }

    private function urlExists($url): bool
    {
        return str_contains($url, 'wikipedia.org') && curl_init($url) !== false;
    }


    /** @throws JsonException|RuntimeException */
    public function getPerson(string $name, string $locale = 'es'): WikipediaPerson
    {
        $client = new Client();
        $params = [
            'format' => 'json',
            'action' => 'query',
            'prop' => 'extracts|pageprops|langlinks',
            'exintro' => 1,
            'explaintext' => 1,
            'titles' => mb_convert_encoding($name, 'ISO-8859-1'),
            'lllimit' => 500
        ];
        $url = $this->getWikipedia($locale) . '?' . http_build_query($params);
        try {
            $res = $client->request('GET', $url);
        } catch (GuzzleException) {
            throw new RuntimeException(sprintf('Wikipedia page %s not found', $url));
        }
        $code = (int)$res->getStatusCode();  // "200"
        if ($code === Response::HTTP_OK) {
            $body = $res->getBody();
            $json = $body->getContents();
            $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            if ($person = WikipediaPerson::hydrate($data)) {
                $person->setTitle($name, $locale);
                foreach ($person->getTitles() as $lang => $title) {
                    // es already processed
                    if ($title !== $locale && $title) {
                        $person->setExtract($this->getExtract($title, $lang), $lang);
                    }
                    $person->setQuotes($this->getQuotes($title, $lang), $lang);
                }
                $person = $this->getPersonData($person);
                $person = $this->getBirthPlaceData($person);
                $person = $this->getDeathData($person);
                $person = $this->getWorksData($person);

                return $person;
            }
            throw new RuntimeException(sprintf('Person Hydrate error with data: %s', $json));
        }
        throw new RuntimeException(sprintf('Wikipedia request error: %d', $code));
    }

    /**
     * @return array
     */
    public function getQuotes(string $name, string $locale = 'es'): array
    {
        $client = new Client();
        $params = [
            'format' => 'json',
            'action' => 'parse',
            'page' => $name,
            'origin' => '*',
            'prop' => 'text',
            'section' => 1,
            'disablelimitreport' => 1,
            'disabletoc' =>1
        ];
        $url = $this->getWikiquote($locale) . '?' . http_build_query($params);
        try {
            $res = $client->request('GET', $url);
        } catch (GuzzleException) {
            return [];
        }
        $code = (int)$res->getStatusCode();  // "200"
        if ($code === Response::HTTP_OK) {
            $body = $res->getBody();
            $json = $body->getContents();
            $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            try {
                $html = $data['parse']['text']['*'] ?? null;
                return (new WikiQuoteUtils($html))->find($locale);
            } catch (\Exception) {
                return [];
            }
        }

        return [];
    }

    /**
     * @return string|null
     */
    public function getExtract(string $name, string $locale = 'es'): ?string
    {
        $client = new Client();
        $params = [
            'format' => 'json',
            'action' => 'query',
            'prop' => 'extracts',
            'exintro' => 1,
            'explaintext' => 1,
            'titles' => $name   // titles:
        ];
        $url = $this->getWikipedia($locale) . '?' . http_build_query($params);
        try {
            $res = $client->request('GET', $url);
        } catch (GuzzleException) {
            return null;
        }
        $code = (int)$res->getStatusCode();  // "200"
        if ($code === Response::HTTP_OK) {
            $body = $res->getBody();
            $json = $body->getContents();
            $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            $pages = $data['query']['pages'] ?? [];
            $page = array_shift($pages);
            if ($page) {
                return $page['extract'] ?? null;
            }
        }

        return null;
    }

    /**
     * @return WikipediaPerson
     */
    public function getPersonData(WikipediaPerson $person): ?WikipediaPerson
    {
        $client = new Client();
        $params = [
            'action' => 'wbgetclaims',
            'entity' => $person->getId(),
            'format' => 'json'
        ];
        $url = self::WIKIDATA . '?' . http_build_query($params);
        try {
            $res = $client->request('GET', $url);
        } catch (GuzzleException) {
            return null;
        }
        $code = (int)$res->getStatusCode();  // "200"
        if ($code === Response::HTTP_OK) {
            $body = $res->getBody();
            $json = $body->getContents();
            $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            $person = $person->setWikidata($data);
            return $person;
        }

        return $person;
    }

    /**
     * @return array
     */
    public function getEntity(string $entity): ?array
    {
        $client = new Client();
        $params = [
            'format' => 'json',
            'action' => 'wbgetentities',
            'ids' => $entity,
            'languages' => implode('|', WikipediaPerson::LANGS)
        ];
        $url = self::WIKIDATA . '?' . http_build_query($params);
        try {
            $res = $client->request('GET', $url);
        } catch (GuzzleException) {
            return null;
        }
        $code = (int)$res->getStatusCode();  // "200"
        if ($code === Response::HTTP_OK) {
            $body = $res->getBody();
            $json = $body->getContents();
            $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            return $data['entities'][$entity] ?? null;
        }

        return null;
    }

    /**
     * @return WikipediaPerson
     */
    public function getBirthPlaceData(WikipediaPerson $person): WikipediaPerson
    {
        if (! $id = $person->getBirthPlace()) {
            return $person;
        }
        if (! $entity =  $this->getEntity($id)) {
            return $person;
        }
        if ($labels = $this->getEntityLabels($entity)) {
            $person->setLocalities($labels);
        }
        $claims = $entity['claims'] ?? [];
        try {
            $country = $claims[WikipediaPerson::PROP_COUNTRY] ?? null;
            if ($country) {
                $country = array_shift($country);
                $id = $country['mainsnak']['datavalue']['value']['id'] ?? null;
                $entity = $this->getEntity($id);
                if ($labels = $this->getEntityLabels($entity)) {
                    $person->setCountries($labels);
                }
                $claims = $entity['claims'] ?? [];
                $country = $claims[WikipediaPerson::PROP_COUNTRY_ISO3] ?? [];
                $country = array_shift($country);
                if ($iso = $country['mainsnak']['datavalue']['value'] ?? null) {
                    $repo = $this->manager->getRepository(Country::class);
                    if (($item = $repo->findOneBy(['iso3' => $iso])) && $continent = $item->getContinent()) {
                        $person->setContinent($continent->getModel());
                    }
                }
            }
        } catch (\Exception) {

        }
        try {
            $province = $claims[WikipediaPerson::PROP_PROVINCE] ?? null;
            if ($province) {
                $province = array_shift($province);
                $id = $province['mainsnak']['datavalue']['value']['id'] ?? null;
                $entity = $this->getEntity($id);
                if ($labels = $this->getEntityLabels($entity)) {
                    $person->setProvinces($labels);
                }
            }
        } catch (\Exception) {

        }
        return $person;
    }

    /**
     * @return WikipediaPerson
     */
    public function getDeathData(WikipediaPerson $person): WikipediaPerson
    {
        $death = ['es' => '', 'en' => ''];

        if (!$date = $person->getDeathDate()) {
            return $person;
        }

        $parts = explode('-', $date);
        $months = [
            'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ];
        $month = $months[(int)$parts[1]] ?? null;
        $death['es'] = sprintf('el día %d de %s de %d', (int)$parts[2], $month, (int)$parts[0]);

        $months = [
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
        ];
        $month = $months[(int)$parts[1]] ?? null;
        $death['en'] = sprintf('%s %d, %d', $month, (int)$parts[0], (int)$parts[2]);

        if ($age = $person->getDeathAge()) {
            $death['es'] .= " ($age años)";
            $death['en'] .= " (aged $age)";
        }

        if ($id = $person->getDeathPlace()) {
            $entity = $this->getEntity($id);
            if ($places = $this->getEntityLabels($entity)) {
                if ($place = $places['es'] ?? null) {
                    $death['es'] .= " en $place";
                }
                if ($place = $places['en'] ?? null) {
                    $death['en'] .= " in $place";
                }
            }
        }

        foreach ($death as $lang => $item) {
            $person->setDeath($item, $lang);
        }

        $cause = null;
        if ($id = $person->getDeathCause()) {
            $entity = $this->getEntity($id);
            if ($cause = $this->getEntityLabels($entity)) {
                if ($cause = $cause['es'] ?? null) {
                    $death['es'] .= " a causa de $cause";
                }
                if ($cause = $cause['en'] ?? null) {
                    $death['en'] .= " due to $cause";
                }
            }
        }

//        $manner = null;
//        $id = $person->getDeathManner();
//        $entity =  $this->getEntity($id);
//        if ($manner = $this->getEntityLabels($entity)) {
//
//        }

        return $person;
    }

    /**
     * @return WikipediaPerson
     */
    public function getWorksData(WikipediaPerson $person): WikipediaPerson
    {
        $arr = [];
        foreach ($person->getWorks() as $id) {
            try {
                $entity = $this->getEntity($id);
                $workType = null;
                $types = $entity['claims'][WikipediaPerson::PROP_WORK_TYPE] ?? [];
                $type = array_shift($types);
                $type = $type['mainsnak']['datavalue']['value']['id'] ?? null;
                if ($type) {
                    $workType = $this->manager->getRepository(PersonWorkType::class)->findOneBy(['wikipedia' => $type, 'deleted_at' => null]);
                }
                if (!$workType) {
                    $this->logger->warning(sprintf('[Wikipedia][%s] WorkType not exists: https://www.wikidata.org/wiki/%s', $type, $type));
                    continue;
                }
                // P31 instance of
                if ($labels = $this->getEntityLabels($entity)) {
                    $item = [
                        'type' => $workType->getName(),
                        'labels' => $labels
                    ];
                    if ($workType->isImage()) {
                        $images = $entity['claims'][WikipediaPerson::PROP_IMAGE] ?? [];
                        $image = array_shift($images);
                        if ($title = $image['mainsnak']['datavalue']['value'] ?? null) {
                            $item['image'] = $this->getImagePath($title);
                        }
                    }
                    $arr[] = $item;
                }
            } catch (\Exception $exception) {
                $this->logger->warning(sprintf('[Wikipedia] WorkType exception: %s', $exception->getMessage()));
            }
        }
        $person->setWorks($arr);
        return $person;
    }

    /**
     * @return string
     */
    public function getWikipedia(string $locale = 'es'): string
    {
        return sprintf(self::WIKIPEDIA, $locale);
    }

    /**
     * @return string
     */
    public function getWikiquote(string $locale = 'es'): string
    {
        return sprintf(self::WIKIQUOTE, $locale);
    }

    /**
     * return associative arry with key as lang and label as value
     * @param array $entity
     * @return array|null
     */
    private function getEntityLabels(?array $entity): ?array
    {
        if (!$entity) {
            return null;
        }
        $labels = $entity['labels'] ?? [];
        if ($labels) {
            $labels = array_combine(array_column($labels, 'language'), array_column($labels, 'value'));
            return $labels;
        }
        return null;
    }
}