<?php


namespace App\Util\Wikipedia;


trait WikiDataUtils
{
    /**
     * @param $property
     * @param $claims
     * @return mixed|null
     */
    protected function getProperty($property, $claims)
    {
        return $claims[$property] ?? null;
    }

    /**
     * snak return first ocurrent of main entity information for a single property
     * @param $property
     * @param $claims
     * @return |null
     */
    protected function getSnak($property, $claims)
    {
        $item = $claims[$property] ?? [];
        $item = array_shift($item);
        return $item['mainsnak'] ?? null;
    }

    /**
     * @param $snak
     * @return mixed|null
     */
    protected function getNumericId($snak): ?int
    {
        return $snak['mainsnak']['datavalue']['value']['numeric-id'] ?? null;
    }

    /**
     * @param $snak
     * @return mixed|null
     */
    protected function getPropertyId($snak): ?string
    {
        return $snak['mainsnak']['datavalue']['value']['id'] ?? null;
    }

    /**
     * @return mixed|null
     */
    protected function getNumericIds(array $property)
    {
        $self = $this;
        return array_map(static fn($item) => $self->getNumericId($item), $property);
    }

    /**
     * @return mixed|null
     */
    protected function  getIds(array $property)
    {
        $self = $this;
        return array_map(static fn($item) => $self->getPropertyId($item), $property);
    }

    /**
     * @return string
     */
    public function getImagePath(string $image): string
    {
        // https://stackoverflow.com/questions/34393884/how-to-get-image-url-property-from-wikidata-item-by-api
        $image = str_replace(' ', '_', $image);
        $md5 = md5($image);
        $image = str_replace([' ', ',', '\''], ['_', '%2C', '%27'], $image);
        $arr = [];
        $arr[] = WikipediaPerson::IMAGE_CDN;
        $arr[] = $md5[0];
        $arr[] = substr($md5, 0, 2);
        $arr[] = $image;
        return implode('/', $arr);
    }
}