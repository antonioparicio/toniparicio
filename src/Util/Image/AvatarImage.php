<?php


namespace App\Util\Image;

use App\Entity\WhoIAm\Player;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

class AvatarImage
{
    /**
     * AvatarImage constructor.
     */
    public function __construct(private readonly KernelInterface $kernel, private readonly ImageResize $imageResize)
    {
    }

    /**
     * @param string $projectDir
     * @return bool
     * @throws \ImagickException
     */
    public function submit(Player $player, UploadedFile $avatar): bool
    {
        $webPath = $this->kernel->getProjectDir() . '/public';
        $id = $player->getId();
        $subfolder = implode('/', str_split((string) $id));
        $relative = '/img/whoiam/avatar/' . $subfolder;
        $folder = $webPath . $relative;
        $timestamp = time();
        $original = $webPath . $relative . '/' . $timestamp . '.jpg';
        if (!is_dir($folder) && !mkdir($folder, 0775, true) && !is_dir($folder)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $folder));
        }
        try {

            $avatar->move($webPath . $relative, $timestamp . '.jpg');
        } catch (\Exception $exception) {
            throw new \RuntimeException(sprintf('Error on image %s : %s', $avatar, $exception->getMessage()));
        }
        // do resize
        $this->imageResize->avatar($player, $original);
        return true;
    }
}