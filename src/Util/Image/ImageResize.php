<?php

namespace App\Util\Image;

use App\Entity\WhoIAm\Image;
use App\Entity\WhoIAm\ImageQueue;
use App\Entity\WhoIAm\ImageVersion;
use App\Entity\WhoIAm\PersonImage;
use App\Entity\WhoIAm\PersonWork;
use App\Entity\WhoIAm\PersonWorkType;
use App\Entity\WhoIAm\Player;
use Doctrine\ORM\EntityManagerInterface;
use Imagick;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Resize image class will allow you to resize an image.
 *
 * Can resize to exact size
 * Max width size while keep aspect ratio
 * Max height size while keep aspect ratio
 * Automatic while keep aspect ratio
 */
class ImageResize
{
    private const MAX_RESOLUTION = 2440;

    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ImageResize constructor.
     *
     * @param $kernel
     */
    public function __construct(EntityManagerInterface $manager, KernelInterface $kernel, LoggerInterface $wikipediaLogger)
    {
        $this->manager = $manager;
        $this->kernel = $kernel;
        $this->logger = $wikipediaLogger;
    }

    /**
     * process one image in queue.
     */
    public function queue(int $id = null): void
    {
        $queue = $this->manager->getRepository(ImageQueue::class);
        if ($id) {
            $item = $queue->find($id);
        } else {
            $item = $queue->findOneBy(['status' => ImageQueue::STATUS_QUEUED]);
        }
        if ($item) {
            $item->setStatus(ImageQueue::STATUS_PROCESSING);
            /** @var ImageQueue $item */
            $metadata = $item->getMetadata();
            $person = $item->getPerson();
            /** @var ImageQueue $item */
            if ($item && ImageQueue::TYPE_PERSON === $item->getType()) {
                $image = new Image();
                $image->setPerson($person);
                $image->setPath($item->getPath());
                $this->manager->persist($image);
                $this->manager->flush();
                $item->setImage($image);
                try {
                    $portrait = $metadata['portrait'] ?? false;
                    $image = $this->handle($image, 'person');
                    $personImage = new PersonImage();
                    $personImage->setPerson($person);
                    $personImage->setImage($image);
                    $personImage->setPortrait($portrait);
                    $person->addImage($personImage);
                    $this->manager->persist($personImage);
                    $item->setStatus(ImageQueue::STATUS_PROCESSED);
                } catch (\Exception $exception) {
                    $item->setStatus(ImageQueue::STATUS_ERROR)
                       ->setError($exception->getMessage());
                    $this->logger->error(sprintf('Error on process ImageQueue %d: %s', $item->getId(), $exception->getMessage()));
                }
            } elseif (ImageQueue::TYPE_WORK === $item->getType()) {
                $image = new Image();
                $image->setPerson($person);
                $image->setPath($item->getPath());
                $this->manager->persist($image);
                $this->manager->flush();
                $item->setImage($image);
                try {
                    $typeName = $metadata['type'] ?? null;
                    if ($type = $this->manager->getRepository(PersonWorkType::class)->findOneBy(['name' => $typeName])) {
                        $image = $this->handle($image, 'work');
                        foreach ($person->getLocales() as $locale) {
                            $lang = $locale->getLocale();
                            if (isset($metadata[$lang])) {
                                $work = new PersonWork();
                                $work->setPerson($locale);
                                $work->setType($type);
                                $work->setTitle($metadata[$lang]['title'] ?? null);
                                $work->setUrl($metadata[$lang]['url'] ?? null);
                                $work->setImage($image);
                                $locale->addWork($work);
                                $this->manager->persist($work);
                            }
                        }
                        $item->setStatus(ImageQueue::STATUS_PROCESSED);
                    } else {
                        $item->setStatus(ImageQueue::STATUS_ERROR)
                           ->setError(sprintf('WorkType %s not found', $typeName));
                        $this->logger->error(sprintf('WorkType %s not found', $typeName));
                    }
                } catch (\Exception $exception) {
                    $item->setStatus(ImageQueue::STATUS_ERROR)
                       ->setError($exception->getMessage());
                    $this->logger->error(sprintf('Error on process ImageQueue %d: %s', $item->getId(), $exception->getMessage()));
                }
            }
            $this->manager->persist($item);
            $this->manager->flush();
        }
    }

    /**
     * @throws \ImagickException
     */
    public function handle(Image $image, string $package): ?Image
    {
        $webPath = $this->kernel->getProjectDir().'/public';
        $relative = '/img/whoiam/person/'.$image->getPerson()->getId().'/'.$package.'/'.$image->getId();
        $folder = $webPath.$relative;

        if (!is_dir($folder) && !mkdir($folder, 0775, true) && !is_dir($folder)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $folder));
        }
        if (!is_writable($folder)) {
            throw new \RuntimeException(sprintf('Folder %s is not writable', $webPath));
        }

        $original = $folder.'/original.jpg';
        $this->copyImage($image, $original);

        if (!extension_loaded('imagick')) {
            throw new \RuntimeException('Imagick extension is not installed');
        }

        $filter = Imagick::FILTER_CATROM;
        // pixel cache max size
        Imagick::setResourceLimit(imagick::RESOURCETYPE_MEMORY, 134217728);
//        Imagick::setResourceLimit(imagick::RESOURCETYPE_MAP, 134217728);
//        Imagick::setResourceLimit(imagick::RESOURCETYPE_AREA, 134217728);
        Imagick::setResourceLimit(imagick::RESOURCETYPE_FILE, 5242880);
        Imagick::setResourceLimit(imagick::RESOURCETYPE_DISK, -1);
        [$oWidth, $oHeight, $type] = getimagesize($original);

        $path = realpath(realpath($original));
        $imagick = new Imagick();
        try {
            $imagick->pingImage($path);
        } catch (\ImagickException $e) {
            throw new \RuntimeException(sprintf('Invalid or corrupted image file %s, please try uploading another image alternative to %s.', $path, $image->getPath()));
        }

        $width = $imagick->getImageWidth();
        $height = $imagick->getImageHeight();
        $mimeType = $imagick->getImageMimeType();

        if (!in_array($mimeType, Image::MIME_TYPE_ALLOWED, true)) {
            throw new \RuntimeException(sprintf('MimeType %s is not a valid origin MimeType', $mimeType));
        }

        if ($width > self::MAX_RESOLUTION || $height > self::MAX_RESOLUTION) {
            $wRatio = self::MAX_RESOLUTION / $width;
            $hRatio = self::MAX_RESOLUTION / $height;
            if ($wRatio > $hRatio) {
                $width *= $hRatio;
                $height *= $hRatio;
            } else {
                $width *= $wRatio;
                $height *= $wRatio;
            }
            $imagick->setSize(round($width), round($height));
        }
        $imagick->readImage($path);

        foreach (Image::FORMATS as $format => [$width, $height]) {
            if ($height <= $oHeight) {
                $imagick->stripImage();
                $imagick->resizeImage(0, $height, $filter, 1);
                $imagick->setImageCompressionQuality(85);
                $extension = 'jpg';
                // Set image as based its own type
                if (IMAGETYPE_JPEG === $type) {
                    $imagick->setImageFormat('jpeg');
                    $imagick->setSamplingFactors(['2x2', '1x1', '1x1']);
                    $profiles = $imagick->getImageProfiles('icc', true);
                    $imagick->stripImage();
                    if (!empty($profiles)) {
                        $imagick->profileImage('icc', $profiles['icc']);
                    }
                    $imagick->setInterlaceScheme(Imagick::INTERLACE_JPEG);
                    $imagick->setColorspace(Imagick::COLORSPACE_SRGB);
                    $extension = 'jpg';
                } elseif (IMAGETYPE_PNG === $type) {
                    $imagick->setImageFormat('png');
                    $extension = 'png';
                } elseif (IMAGETYPE_GIF === $type) {
                    $imagick->setImageFormat('gif');
                    $extension = 'gif';
                }
                $output = $folder.'/'.$format.'.'.$extension;

                $imagick->writeImage($output);
                if (file_exists($output)) {
                    $version = new ImageVersion();
                    $version->setImage($image);
                    $version->setHeight($imagick->getImageHeight());
                    $version->setWidth($imagick->getImageWidth());
                    $version->setMime($imagick->getImageMimeType());
                    $version->setPath($relative.'/'.$format.'.'.$extension);
                    $version->setSize($imagick->getImageLength());
                    $version->setType($format);
                    $image->addVersion($version);
                    $this->manager->persist($version);
                }
            }
        }
        $image->setOriginal($relative.'/original.jpg');
        $this->manager->persist($image);
//        $image->setPath($relative . '/original.jpg');
//        $image->setSource($image->getPath());
//        $this->manager->persist($image);
//        $this->manager->flush();
        return $image;
    }

    private function copyImage(Image $remote, string $local): void
    {
        try {
            if (null === $remote->getPath() && $remote->getOriginal()) {
                copy($remote->getOriginal(), $local);

                return;
            }
            // image is remote and I need download to local
//            copy($remote, $local);
            // if image is base64
            if (0 === strpos('data:image', $remote->getPath())) {
                file_put_contents($local, file_get_contents($remote->getPath()));

                return;
            }
            $ch = curl_init($remote->getPath());
            $fp = fopen($local, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            // set a useragent or some sources as wikipedia could block download
            $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
            curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
        } catch (\Exception $exception) {
            throw new \RuntimeException(sprintf('Error on image %s : %s', $remote->getPath(), $exception->getMessage()));
        }
        if (!file_exists($local)) {
            throw new \RuntimeException(sprintf('Error on save image in route %s', $local));
        }
    }

    /**
     * @param $original
     *
     * @throws \ImagickException
     */
    public function avatar(Player $player, $original): bool
    {
        $id = $player->getId();
        $subfolder = implode('/', str_split($id));
        $relative = '/public/img/whoiam/avatar/'.$subfolder;
        $webPath = $this->kernel->getProjectDir();
        $folder = $webPath.$relative;

        $filter = Imagick::FILTER_CATROM;
        // pixel cache max size
        Imagick::setResourceLimit(imagick::RESOURCETYPE_MEMORY, 134217728);
        Imagick::setResourceLimit(imagick::RESOURCETYPE_FILE, 5242880);
        Imagick::setResourceLimit(imagick::RESOURCETYPE_DISK, -1);
        [$oWidth, $oHeight, $type] = getimagesize($original);

        $path = realpath(realpath($original));

        $imagick = new Imagick();
        try {
            $imagick->pingImage($path);
        } catch (\ImagickException $e) {
            throw new \RuntimeException('Invalid or corrupted image file, please try uploading another image.');
        }

        $width = $imagick->getImageWidth();
        $height = $imagick->getImageHeight();
        $mimeType = $imagick->getImageMimeType();

        if (!in_array($mimeType, Image::MIME_TYPE_ALLOWED, true)) {
            throw new \RuntimeException(sprintf('MimeType %s is not a valid origin MimeType', $mimeType));
        }

        if ($width > self::MAX_RESOLUTION || $height > self::MAX_RESOLUTION) {
            $wRatio = self::MAX_RESOLUTION / $width;
            $hRatio = self::MAX_RESOLUTION / $height;
            if ($wRatio > $hRatio) {
                $width *= $hRatio;
                $height *= $hRatio;
            } else {
                $width *= $wRatio;
                $height *= $wRatio;
            }
            $imagick->setSize(round($width), round($height));
        }
        $imagick->readImage($path);

        foreach (Image::AVATARS as $format => [$width, $height]) {
            $imagick->stripImage();
            $imagick->resizeImage(0, $height, $filter, 1);
            $imagick->setImageCompressionQuality(85);
            $extension = 'jpg';
            // Set image as based its own type
            if (IMAGETYPE_JPEG === $type) {
                $imagick->setImageFormat('jpeg');
                $imagick->setSamplingFactors(['2x2', '1x1', '1x1']);
                $profiles = $imagick->getImageProfiles('icc', true);
                $imagick->stripImage();
                if (!empty($profiles)) {
                    $imagick->profileImage('icc', $profiles['icc']);
                }
                $imagick->setInterlaceScheme(Imagick::INTERLACE_JPEG);
                $imagick->setColorspace(Imagick::COLORSPACE_SRGB);
                $extension = 'jpg';
            } elseif (IMAGETYPE_PNG === $type) {
                $imagick->setImageFormat('png');
                $extension = 'png';
            } elseif (IMAGETYPE_GIF === $type) {
                $imagick->setImageFormat('gif');
                $extension = 'gif';
            }
            $output = $folder.'/'.$format.'.'.$extension;

            $imagick->writeImage($output);
        }

        return true;
    }
}
