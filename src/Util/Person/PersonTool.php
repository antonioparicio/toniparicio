<?php

namespace App\Util\Person;

use App\Entity\WhoIAm\Category;
use App\Entity\WhoIAm\ImageQueue;
use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLink;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\PersonLinkType;
use App\Entity\WhoIAm\PersonQuote;
use App\Entity\WhoIAm\PersonWork;
use App\Entity\WhoIAm\PersonWorkType;
use App\Entity\WhoIAm\QuoteQueue;
use App\Util\Image\ImageResize;
use App\Util\Wikipedia\WikipediaPerson;
use Doctrine\ORM\EntityManagerInterface;
use Entity\Repository\CategoryRepository;
use Psr\Log\LoggerInterface;

class PersonTool
{
    /**
     * PersonTool constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly ImageResize $resize,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @throws \Exception
     */
    public function updateFromWikipediaPerson(WikipediaPerson $wikipedia): ?Person
    {
        $name = $wikipedia->getName();
        $repo = $this->manager->getRepository(Person::class);
        if (!($person = $repo->findOneBy(['realName' => $name]))) {
            $person = new Person();
            $person->setRealName($name);
        }
        $person = $this->updatePersonFromWikipedia($person, $wikipedia);
        $this->manager->persist($person);
        $titles = $wikipedia->getTitles();
        foreach ($titles as $lang => $titleLocale) {
            if ($item = $this->getOrCreateLocale($person, $lang)) {
                $item = $this->updatePersonLocaleFromWikipedia($item, $wikipedia, $lang);
                $this->manager->persist($item);
            }
        }
        $this->queuePersonWorkImages($person, $wikipedia);
        $this->manager->flush();
//        if ($person) {
//            $this->processImages($person);
//            $this->processWorks($person);
//            $this->manager->flush();
//        }
        return $person;
    }

    /**
     * @return PersonLocale|object|null
     */
    private function getOrCreateLocale(Person $person, string $lang) {
        $locale = $person->getLocale($lang);
        if (!$locale) {
            $locale = new PersonLocale();
            $locale->setLocale($lang);
            $locale->setPerson($person);
        }
        return $locale;
    }

    /**
     * @return PersonLocale
     */
    private function updatePersonLocaleFromWikipedia(PersonLocale $person, WikipediaPerson $wikipedia, string $locale): PersonLocale
    {
        if ($title = $wikipedia->getTitle($locale)) {
            $person->setName($title);

            $duplicated = $this->manager->getRepository(PersonLocale::class)
                ->findOneBy(['name' => $title, 'locale' => $person->getLocale()]);
            if ($duplicated && $duplicated->getPerson()->getId() !== $person->getPerson()->getId()) {
                $parentDuplicated = $duplicated->getPerson();
                if (count($parentDuplicated->getLocales()) === 1) {
                    $this->manager->remove($parentDuplicated);
                }
                $this->manager->remove($duplicated);
                $this->manager->flush();
            }
        }

        if (!$person->getDescription() && $extract = $wikipedia->getExtract($locale)) {
            $person->setDescription($extract);
        }
        if (!$person->getCity() && $city = $wikipedia->getLocality($locale)) {
            $province = $wikipedia->getProvince($locale);
            if ($province && $province !== $city) {
                $city .= ', ' . $province;
            }
            $person->setCity($city);
        }
        if (!$person->getCountry() && $country = $wikipedia->getCountry($locale)) {
            $person->setCountry($country);
        }
        if (!$person->getDeath() && $death = $wikipedia->getDeath($locale)) {
            $person->setDeath($death);
        }

        foreach ($wikipedia->getQuotes($locale) as $quote) {
            if ($quote && !$person->hasQuote($quote) && strlen((string) $quote) < 128) {
                // quotes need approve because not secure service
                $queue = new QuoteQueue();
                $queue->setPerson($person);
                $queue->setQuote($quote);

                $this->manager->persist($queue);
            }
        }

        $wikipediaType = $this->manager->getRepository(PersonLinkType::class)->findOneBy(['name' => 'wikipedia']);
        $links = array_filter($person->getLinks()->toArray(), static fn(PersonLink $link) => $link->getType() && $link->getType()->getName() === 'wikipedia');
        if (!$links || !count($links)) {
            $personLink = new PersonLink();
            $personLink->setType($wikipediaType);
            $personLink->setPerson($person);
            $url = 'https://'. $locale .'.wikipedia.org/wiki/' . urlencode($wikipedia->getTitle($locale));
            $personLink->setUrl($url);
            $person->addLink($personLink);
            $this->manager->persist($personLink);
        }

        return $person;
    }

    /**
     * @return Person
     * @throws \Exception
     */
    private function updatePersonFromWikipedia(Person $person, WikipediaPerson $wikipedia): Person
    {
        if (!$person->getYear() && $year = $wikipedia->getYear()) {
            $person->setYear($year);
        }
        if (!$person->getMonth() && $month = $wikipedia->getMonth()) {
            $person->setMonth($month);
        }
        if (!$person->getDay() && $day = $wikipedia->getDay()) {
            $person->setDay($day);
        }
        if (!$person->getGender() && $gender = $wikipedia->getGender()) {
            $person->setGender($gender);
        }
        if ($continent = $wikipedia->getContinent()) {
            $person->setContinent($continent);
        }
        $categories = $person->getCategories();
        if (!$categories || !(is_countable($categories) ? count($categories) : 0)) {
            $repo = $this->manager->getRepository(Category::class);
            /** @var CategoryRepository $repo */
            if ($categories = $repo->findBy(['wikipedia' => $wikipedia->getCategories()])) {
                $person->setCategories($categories);
            }
        }
        if (($image = $wikipedia->getImage()) && !$person->hasImage($image)) {
            $queue = new ImageQueue();
            $queue->setPerson($person);
            $queue->setType(ImageQueue::TYPE_PERSON);
            $queue->setPath($image);
            $queue->setMetadata(['portrait' => true]);
            $this->manager->persist($queue);
        }
        return $person;
    }

    /**
     * @return Person
     */
    private function queuePersonWorkImages(Person $person, WikipediaPerson $wikipedia): Person
    {
        foreach ($wikipedia->getWorks() as $item) {
            $metadata = [
                'type' => $item['type']
            ];
            foreach ($person->getLocales() as $locale) {
                $url = $item['image'] ?? null;
                $title = $item['labels'][$locale->getLocale()] ?? null;
                if ($title && !$locale->hasWork($title)) {
                    if ($url) {
                        // works with images will be queued for process
                        $metadata[$locale->getLocale()] = [
                            'title' => $item['labels'][$locale->getLocale()]
                        ];
                    } else {
                        $work = new PersonWork();
                        if ($type = $this->manager->getRepository(PersonWorkType::class)->findOneBy(['name' => $item['type']])) {
                            $work->setType($type);
                            $work->setTitle($item['labels'][$locale->getLocale()]);
                            $work->setPerson($locale);
                            $locale->addWork($work);
                            $this->manager->persist($work);
                        }
                    }
                }
            }
            if ($metadata && count($metadata) > 1) {    // type is mandatory
                // only queue if at least one locale needs generate this work
                $queue = new ImageQueue();
                $queue->setPerson($person);
                $queue->setType(ImageQueue::TYPE_WORK);
                $queue->setPath($item['image']);
                $queue->setMetadata($metadata);
                $this->manager->persist($queue);
            }
        }

        return $person;
    }


    /**
     * @param $work
     * @return PersonLocale
     */
    private function processPersonWork(PersonLocale $person, $work): PersonLocale
    {
        return $person;
    }


    /**
     * @return Person
     */
    private function processImages(Person $person): Person {
        foreach ($person->getImages() as $image)
        {
            if (!$image->isHandled()) {
                try {
                    $this->resize->handle($person, $image);
                    $this->manager->persist($person);
                    $this->manager->persist($image);
                    $this->manager->flush();
                } catch (\Exception $exception) {
                    $image->setUrl(null);
                    $image->setSource($image->getPath());
                    $image->setDeletedAt();
                    $this->logger->error(sprintf('[processImages] Exception %s', $exception->getMessage()));
                    continue;
                }
            }
        }
        return $person;
    }

    /**
     * @return Person
     */
    private function processWorks(Person $person): Person
    {
        foreach ($person->getLocales() as $locale) {
            /** @var PersonLocale $person */
            foreach ($locale->getWorks() as $work) {
                if ($work->getImage() && !$work->isHandled()) {
                    try {
                        $this->resize->handle($person, $work);
                        $this->manager->persist($person);
                        $this->manager->persist($work);
                        $this->manager->flush();
                        $this->logger->error(sprintf('[processWork] Exception'));
                    } catch (\Exception $exception) {
                        $work->setImage(null);
                        $this->logger->error(sprintf('[processWork] Exception %s', $exception->getMessage()));
                        continue;
                    }
                }
            }
        }
        return $person;
    }

}