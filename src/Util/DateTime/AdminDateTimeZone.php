<?php

namespace App\Util\DateTime;

trait AdminDateTimeZone
{
    public function getAdminDateTime(): array
    {
        return [
            'format' => 'Y-m-d H:i',
            'locale' => 'es',
            'timezone' => 'Europe/Madrid',
        ];
    }
}