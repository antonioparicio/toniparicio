<?php

namespace App\Util\Email;

use App\Entity\WhoIAm\Player;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

class Email
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var ?Player
     */
    private $user;
    /**
     * @var string
     */
    private $template;
    /**
     * @var string
     */
    private $subject;
    /**
     * @var array
     */
    private $body;

    /**
     * @var Environment
     */
    private $templating;


    public function __construct(MailerInterface $mailer, Environment $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function setUser(?Player $user): Email
    {
        $this->user = $user;
        return $this;
    }

    public function setTemplate(string $template): Email
    {
        $this->template = $template;
        return $this;
    }

    public function setSubject(string $subject): Email
    {
        $this->subject = $subject;
        return $this;
    }

    public function setBody(array $body): Email
    {
        $this->body = $body;
        return $this;
    }

    public function send(): array
    {
        $data = [];
        try {
            $env = new Dotenv();
            $env->load(__DIR__.'/../../../.env');

            $sender = trim(getenv('MAILER_WHOIAM_SENDER'));
            $receiver = trim(getenv('MAILER_WHOIAM_RECEIVER'));
            $subject = '[' . strtoupper($this->template) . '] ' . $this->subject;

            $body = $this->body;
            $body['user'] = $this->user;

            $email = (new \Symfony\Component\Mime\Email())
                ->from($sender)
                ->to($receiver)
                ->subject($subject)
                ->html(
                    $this->templating->render(
                        sprintf('email/whoiam_%s.html.twig', $this->template),
                        $body
                    )
                );
            $this->mailer->send($email);
        } catch (\Exception $exception) {
            $data = [
                'warning' => $exception->getMessage()
            ];
        } catch (TransportExceptionInterface $exception) {
            $data = [
                'warning' => $exception->getMessage()
            ];
        }
        return $data;
    }
}