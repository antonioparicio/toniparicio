<?php


namespace App\Entity\Interfaces;


interface ImageVersion
{
    /**
     * @var string
     */
    public const CDN = '/img/';


    // https://medium.com/@albertcito/tama%C3%B1o-im%C3%A1genes-en-pixeles-para-android-b7a6c4a9d97
    public const FORMATS = [
        'xxxhdpi' => [1440, 810],
        'xxhdpi' => [1080, 607],
        'xhdpi' => [720, 405],
        'hdpi' => [480, 270],
        'mdpi' => [320, 180],
        'ldpi' => [240, 135]
    ];

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getPackage(): string;

    /**
     * @return string
     */
    public function getPath(): ?string;

    /**
     * @return ImageVersion
     */
    public function setPath(string $path): self;

    /**
     * @return ImageVersion
     */
    public function setSource(string $source): self;

    /**
     * @return string
     */
    public function getVersionClass(): string;

    /**
     * @return bool
     */
    public function isHandled(): bool;
}