<?php

// src/App/Entity/Traits/Image.php

namespace App\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait Image
{
    /**
    * @ORM\Column(name="path", type="string")
    */
    protected $path;

    /**
    * @ORM\Column(name="filename", type="string")
    */
    protected $filename;

    protected $file;

    /**
    * @return mixed
    */
    public function getPath()
    {
        return $this->path;
    }

    public function setPath(mixed $path)
    {
        $this->path = $path;
    }

    /**
    * @return mixed
    */
    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename(mixed $filename)
    {
        $this->filename = $filename;
    }

    /**
    * @return mixed
    */
    public function getFile()
    {
        return $this->file;
    }

    /**
    * Set file for upload
    *
    * @param $file
    */
    public function setFile($file)
    {
        $this->file = $file;
        $fileName = uniqid().'.'.$file->guessExtension();
        $file->move($this->getUploadRootDir(), $fileName);
        $this->path = $this->uploadPath;
        $this->filename = $fileName;
        $this->file = null;
    }

    public function getUploadRootDir()
    {
        return __DIR__."/../../../public/img/".$this->uploadPath;
    }

    /**
    * Get id
    *
    * @return integer
    */
    public function getId()
    {
        return $this->id;
    }

    public function getWebPath()
    {
        $webPath = "/image/".$this->uploadPath."/".$this->filename;
        return $webPath ;
    }
}
