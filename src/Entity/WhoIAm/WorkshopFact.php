<?php

namespace App\Entity\WhoIAm;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_workshop_fact")
 */
class WorkshopFact
{
    use PersonClue;
    use WorkshopItem;

    final public const NAME = 'fact';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api", "workshop"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="workshopFacts",
     *     cascade={},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=false)
     * @Groups("facts")
     */
    private ?\App\Entity\WhoIAm\PersonLocale $person = null;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups({"api", "workshop"})
     */
    private $fact;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @ORM\OneToMany(targetEntity=PersonClueLike::class, mappedBy="workshop_facts")
     * @ORM\JoinColumn(name="id", nullable=true, referencedColumnName="clue")
     * @Groups("api")
     */
    private ArrayCollection|PersistentCollection $likes;

    /**
     * WorkshopFact constructor.
     */
    public function __construct()
    {
        $this->likes = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return self
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return self
     */
    public function setPerson(PersonLocale $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFact()
    {
        return $this->fact;
    }

    public function setFact(mixed $fact): self
    {
        $this->fact = $fact;

        return $this;
    }

    /**
     * Return likes/dislikes discriminated for workshop facts.
     *
     * @return mixed
     */
    public function getLikes()
    {
        $criteria = Criteria::create();
        try {
            $criteria->where(Criteria::expr()->eq('type', self::NAME))
                ->andWhere(Criteria::expr()->eq('workshop', 1));
        } catch (\Exception) {
            return new ArrayCollection();
        }

        return $this->likes->matching($criteria);
    }

    /**
     * Return and indexed array with [likes, dislikes] counts.
     *
     * @return mixed
     */
    public function getLikesCount(): array
    {
        $likes = 0;
        $dislikes = 0;
        foreach ($this->getLikes() as $like) {
            /* @var PersonClueLike $like */
            $likes += $like->getLikes();
            $dislikes += $like->getDislikes();
        }

        return [$likes, $dislikes];
    }

    /**
     * return a sort version for api response.
     */
    public function toApi(): array
    {
        [$likes, $dislikes] = $this->getLikesCount();

        return [
            'id' => $this->getId(),
            'fact' => $this->getFact(),
            'likes' => $likes,
            'dislikes' => $dislikes,
            'author' => $this->getAuthor() ? $this->getAuthor()->toApi() : null,
        ];
    }
}
