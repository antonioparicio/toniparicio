<?php


namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_person_work_type")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonWorkType implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Category::class,
     *     inversedBy="work_types",
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     * @Groups("api")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=64)
     * @Groups("api")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups("api")
     */
    private $description;

    /**
     * P31 instance of Painting (),..
     * @ORM\Column(type="string", nullable=true)
     * @Groups("api")
     */
    private $wikipedia;

    /**
     * Define if this work must include image as clue (painting,,...)
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups("api")
     */
    private ?bool $image = false;

    /**
     * Icon for app
     * @ORM\Column(type="string", nullable=true)
     * @Groups("api")
     */
    private $icon;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonWorkType
     */
    public function setId(mixed $id): PersonWorkType
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return PersonWorkType
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return PersonWorkType
     */
    public function setName(mixed $name): PersonWorkType
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return PersonWorkType
     */
    public function setDescription(mixed $description): PersonWorkType
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWikipedia(): ?string
    {
        return $this->wikipedia;
    }

    /**
     * @return PersonWorkType
     */
    public function setWikipedia(mixed $wikipedia)
    {
        $this->wikipedia = $wikipedia;
        return $this;
    }

    /**
     * @return bool
     */
    public function isImage(): bool
    {
        return $this->image ?? false;
    }

    /**
     * @return PersonWorkType
     */
    public function setImage(bool $image): PersonWorkType
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @return PersonWorkType
     */
    public function setIcon(mixed $icon): PersonWorkType
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonWorkType
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonWorkType
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonWorkType
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }
}
