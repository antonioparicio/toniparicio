<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\ImageQueueRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=ImageQueueRepository::class)
 * @ORM\Table(
 *     name="who_image_queue",
 * )
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class ImageQueue
{
    final public const TYPE_PERSON = 'person';
    final public const TYPE_WORK = 'work';
    final public const STATUS_QUEUED = 'queued';
    final public const STATUS_PROCESSING = 'processing';
    final public const STATUS_PROCESSED = 'processed';
    final public const STATUS_ERROR = 'error';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="versions", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $image;

    /**
     * @ORM\Column(
     *     type="string",
     *      length=16
     * )
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="locales", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?\App\Entity\WhoIAm\Person $person = null;

    /**
     * @ORM\Column(type="string", length=2048, nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=2096, nullable=true)
     */
    private $metadata;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=16
     * )
     */
    private string $status = self::STATUS_QUEUED;

    /**
     * @ORM\Column(type="string", length=2096, nullable=true)
     */
    private $error;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @var string
     */
    public $filename;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ImageQueue
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function setType(mixed $type): ImageQueue
    {
        $this->type = $type;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): ImageQueue
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(mixed $path): ImageQueue
    {
        $this->path = $path;

        return $this;
    }

    public function getMetadata(): array
    {
        return $this->metadata ? json_decode((string) $this->metadata, true, 512, JSON_THROW_ON_ERROR) : [];
    }

    public function setMetadata(array $metadata): ImageQueue
    {
        try {
            $this->metadata = json_encode($metadata, JSON_THROW_ON_ERROR) ?? [];
        } catch (\JsonException $exception) {
            $this->metadata = [];
        }

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(mixed $status): ImageQueue
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    public function setImage(mixed $image): ImageQueue
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    public function setError(mixed $error): ImageQueue
    {
        $this->error = $error;

        return $this;
    }

    public function getFilename(): ?string
    {
        $arr = explode('/', (string) $this->path);

        return array_pop($arr);
    }
}
