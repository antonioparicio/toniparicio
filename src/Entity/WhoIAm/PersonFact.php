<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_person_fact")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonFact
{
    use PersonClue;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="facts",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     * @Groups("facts")
     */
    private ?PersonLocale $person = null;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups("api")
     */
    private $fact;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonFact
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return self
     */
    public function setPerson(?PersonLocale $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFact()
    {
        return $this->fact;
    }

    /**
     * @return PersonFact
     */
    public function setFact(mixed $fact): self
    {
        $this->fact = $fact;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonFact
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonFact
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonFact
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
