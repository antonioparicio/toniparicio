<?php


namespace App\Entity\WhoIAm;


interface PersonClueInterface
{
    public const STATUS_WAITING = 0;
    public const STATUS_ACTIVE = 1;
}