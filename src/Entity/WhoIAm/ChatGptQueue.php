<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\ChatGptQueueRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=ChatGptQueueRepository::class)
 * @ORM\Table(
 *     name="chatgpt_queue",
 *     indexes={@Index(name="search_idx", columns={"person_id", "person_locale_id"})}
 * )
 */
class ChatGptQueue implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Person::class,
     *     inversedBy="chatGptQueues",
     *     cascade={"persist"},
     *     fetch="LAZY"
     *     )
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private Person $person;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="chatGptQueues",
     *     cascade={"persist"},
     *     fetch="LAZY"
     *     )
     * @ORM\JoinColumn(nullable=true)
     */
    private PersonLocale $personLocale;

    /**
     * @ORM\OneToOne(
     *     targetEntity=ChatGptPrompt::class,
     *     inversedBy="queue",
     *     cascade={"persist"},
     *     fetch="LAZY"
     *     )
     * @ORM\JoinColumn(nullable=true)
     */
    private ?ChatGptPrompt $prompt = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private string $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isHandled = false;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return ChatGptQueue
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): ChatGptQueue
    {
        $this->person = $person;

        return $this;
    }

    public function getPersonLocale(): PersonLocale
    {
        return $this->personLocale;
    }

    public function setPersonLocale(PersonLocale $personLocale): ChatGptQueue
    {
        $this->personLocale = $personLocale;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): ChatGptQueue
    {
        $this->type = $type;

        return $this;
    }

    public function isHandled(): bool
    {
        return $this->isHandled;
    }

    public function setIsHandled(bool $isSuccess): ChatGptQueue
    {
        $this->isHandled = $isSuccess;

        return $this;
    }

    public function getPrompt(): ?ChatGptPrompt
    {
        return $this->prompt;
    }

    public function setPrompt(?ChatGptPrompt $prompt): ChatGptQueue
    {
        $this->prompt = $prompt;
        return $this;
    }



    public function setCreatedAt(mixed $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(mixed $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * return name when object is tried to use as string.
     *
     * @return string full author name
     *
     * @author Toni Paricio <toniparicio@gmail.com>
     *
     * @since  2018-06-08
     */
    public function __toString(): string
    {
        return "{$this->id}";
    }

    public function toApi(): array
    {
        return [
            'id' => $this->getId(),
            'person' => $this->person->getId(),
            'personLocale' => $this->personLocale->getId(),
            'type' => $this->type,
            'is_handled' => $this->isHandled,
        ];
    }
}
