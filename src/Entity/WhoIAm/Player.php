<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PlayerRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 * @ORM\Table(
 *     name="who_player",
 *     indexes={@Index(name="search_idx", columns={"uuid", "email"})}
 * )
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class Player implements \Stringable
{
    /**
     * This user cant update content to workshop.
     *
     * @var int
     */
    final public const WORKSHOP_DISABLED = 0;

    /**
     * This user can update content to workshop.
     *
     * @var int
     */
    final public const WORKSHOP_ENABLED = 1;

    /**
     * This user workshop content is automatically approved.
     *
     * @var int
     */
    final public const WORKSHOP_PUBLISHER = 2;

    public const PROVIDER_BASIC = 'BASIC';
    public const PROVIDER_GOOGLE = 'GOOGLE';
    public const PROVIDER_ANONYMOUS = 'ANONYMOUS';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"player"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups({"player"})
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", nullable=true, length=64)
     */
    private $email;

    /**
     * username in game
     *
     * @ORM\Column(type="string", nullable=true, length=64)
     * @Groups({"player"})
     */
    private $display;

    /**
     * Define true if user has avatar in /img/whoiam/avatar/<id>.jpg; (id exploded by each cipher).
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"player"})
     */
    private bool $avatar = false;

    /**
     * @ORM\Column(type="string", nullable=true, length=16)
     */
    private $provider;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $birthDate = null;

    /**
     * @ORM\Column(type="string", nullable=true, length=2)
     * @Groups({"player"})
     */
    private $country;

    /**
     * Define if this player can comment.
     *
     * @ORM\Column(type="boolean")
     * @Groups({"player"})
     */
    private bool $comments = true;

    /**
     * User with workshop true no need confirmation for person workshop suggestions.
     *
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $workshop = self::WORKSHOP_ENABLED;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"player"})
     */
    private int $trophies = 0;

    /**
     * Trophies in season, this value will be reset each season (month).
     *
     * @ORM\Column(type="integer")
     * @Groups({"player"})
     */
    private int $season = 0;

    /**
     * @ORM\OneToMany(
     *     targetEntity=Game::class,
     *     mappedBy="player",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     */
    private $games;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonLike::class,
     *     mappedBy="player",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     */
    private $likes;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonComment::class,
     *     mappedBy="player",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     */
    private $personComments;

    /**
     * Player difficulty reached
     *
     * @ORM\Column(type="integer")
     * @Groups({"player"})
     */
    private int $difficulty = 1;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * Author constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Player
     */
    public function setId(mixed $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return Player
     */
    public function setLocale(mixed $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return Player
     */
    public function setUuid(mixed $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return Player
     */
    public function setEmail(mixed $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplay(): ?string
    {
        return $this->display;
    }

    public function setDisplay(mixed $display): Player
    {
        $this->display = $display;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    public function setProvider(mixed $provider): Player
    {
        $this->provider = $provider;

        return $this;
    }

    public function isAvatar(): bool
    {
        return $this->avatar;
    }

    public function setAvatar(bool $avatar): Player
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getComments(): bool
    {
        return $this->comments;
    }

    public function setComments(bool $comments): Player
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return Player
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return Player
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return Player
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getWorkshop(): int
    {
        return $this->workshop;
    }

    public function isWorkshop(): bool
    {
        return self::WORKSHOP_DISABLED !== $this->workshop;
    }

    public function isPublisher(): bool
    {
        if ('toniparicio@gmail.com' === $this->email && self::PROVIDER_GOOGLE === $this->provider) {
            return true;
        }

        return self::WORKSHOP_PUBLISHER === $this->workshop;
    }

    public function setWorkshop(bool $workshop): Player
    {
        $this->workshop = $workshop;

        return $this;
    }

    public function getTrophies(): int
    {
        return $this->trophies;
    }

    public function setTrophies(int $trophies): Player
    {
        $this->trophies = $trophies;

        return $this;
    }

    public function addTrophies(int $trophies): Player
    {
        $this->trophies += $trophies;
        $this->season += $trophies;

        return $this;
    }

    public function getSeason(): int
    {
        return $this->season;
    }

    public function setSeason(int $season): Player
    {
        $this->season = $season;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate(?DateTime $birthDate): Player
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(mixed $country): Player
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * @param mixed $games
     */
    public function setGames($games): Player
    {
        $this->games = $games;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     *
     * @return Player
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonComments()
    {
        return $this->personComments;
    }

    /**
     * @param mixed $personComments
     *
     * @return Player
     */
    public function setPersonComments($personComments)
    {
        $this->personComments = $personComments;

        return $this;
    }
    public  function getDifficulty(): int
    {
        return $this->difficulty > 0 ? $this->difficulty : 1;
    }

    public function setDifficulty(int $difficulty): Player
    {
        $this->difficulty = $difficulty;
        return $this;
    }

    public function isTester(): bool
    {
        if ($this->getEmail() === 'tester@toniparicio.com') {
            return true;
        }
        // other tester validations as domain
        return false;
    }

    /**
     * return name when object is tried to use as string.
     *
     * @return string full author name
     *
     * @author Toni Paricio <toniparicio@gmail.com>
     *
     * @since  2018-06-08
     */
    public function __toString(): string
    {
        $id = $this->email ?? $this->display ?? $this->uuid;

        return "[{$this->id}] $id";
    }

    public function toApi(): array
    {
        return [
            'id' => $this->getId(),
            'display' => $this->getDisplay(),
            'avatar' => $this->avatar,
            'comments' => $this->comments,
            'difficulty' => $this->getDifficulty(),
            'country' => $this->country,
            'birthdate' => $this->birthDate instanceof DateTime ? $this->birthDate->getTimestamp() : null,
        ];
    }
}
