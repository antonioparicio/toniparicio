<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\WhoIAm\PlayerSeasonRepository;

/**
 * @ORM\Entity(repositoryClass=PlayerSeasonRepository::class)
 * @ORM\Table(name="who_player_season")
 */
class PlayerSeason
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups("api")
     */
    private ?\App\Entity\WhoIAm\Player $player = null;

    /**
     * @ORM\ManyToOne(targetEntity=Season::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups("api")
     */
    private ?\App\Entity\WhoIAm\Season $season = null;


    /**
     * League (language) where this player is ranking
     * @ORM\Column(type="string", length=2)
     * @var string
     */
    private $league;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $trophies = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $total = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $position = null;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * Author constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PlayerSeason
     */
    public function setId(mixed $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayer(): Player
    {
        return $this->player;
    }

    /**
     * @return PlayerSeason
     */
    public function setPlayer(Player $player): PlayerSeason
    {
        $this->player = $player;
        $this->league = $player->getLocale();
        $this->trophies = $player->getSeason();
        $this->total = $player->getTrophies();
        return $this;
    }

    /**
     * @return Season
     */
    public function getSeason(): Season
    {
        return $this->season;
    }

    /**
     * @return PlayerSeason
     */
    public function setSeason(Season $season): PlayerSeason
    {
        $this->season = $season;
        return $this;
    }

    /**
     * @return string
     */
    public function getLeague(): string
    {
        return $this->league;
    }

    /**
     * @return PlayerSeason
     */
    public function setLeague(string $league): PlayerSeason
    {
        $this->league = $league;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return PlayerSeason
     */
    public function setPosition(int $position): PlayerSeason
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return int
     */
    public function getTrophies(): int
    {
        return $this->trophies;
    }

    /**
     * @return PlayerSeason
     */
    public function setTrophies(int $trophies): PlayerSeason
    {
        $this->trophies = $trophies;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return PlayerSeason
     */
    public function setTotal(int $total): PlayerSeason
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PlayerSeason
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }
}
