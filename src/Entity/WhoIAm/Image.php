<?php

namespace App\Entity\WhoIAm;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_image")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class Image implements \Stringable
{
    /**
     * @var string
     */
    final public const CDN = '/img/';

    final public const MIME_TYPE_ALLOWED = [
        'image/jpeg',
        'image/jpg',
        'image/png',
    ];
    // https://medium.com/@albertcito/tama%C3%B1o-im%C3%A1genes-en-pixeles-para-android-b7a6c4a9d97
    final public const FORMATS = [
        'xxxhdpi' => [1440, 810],
        'xxhdpi' => [1080, 607],
        'xhdpi' => [720, 405],
        'hdpi' => [480, 270],
        'mdpi' => [320, 180],
        'ldpi' => [240, 135],
    ];

    // https://medium.com/@albertcito/tama%C3%B1o-im%C3%A1genes-en-pixeles-para-android-b7a6c4a9d97
    final public const AVATARS = [
        '120' => [120, 120],
        '240' => [240, 240],
        '480' => [480, 480],
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $person;

    /**
     * remote path where download source image.
     *
     * @ORM\Column(type="string", length=2048, nullable=true)
     * @Groups("api")
     */
    private ?string $path = null;

    /**
     * local path where original is stored.
     *
     * @ORM\Column(type="string", length=2048, nullable=true)
     * @Groups("api")
     */
    private ?string $original = null;

    /**
     * @ORM\OneToMany(targetEntity=ImageVersion::class, mappedBy="image", cascade={"persist", "remove"})
     * @Groups("api")
     */
    private $versions;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * PersonImage constructor.
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): Image
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(mixed $person): Image
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(mixed $path): Image
    {
        $this->path = $path;

        return $this;
    }

    public function getOriginal(): ?string
    {
        return $this->original;
    }

    public function setOriginal(mixed $original): Image
    {
        $this->original = $original;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getVersions()
    {
        return $this->versions;
    }

    public function setVersions(array $versions): Image
    {
        $this->versions = $versions;

        return $this;
    }

    /**
     * @param ImageVersion $version
     *
     * @return $this
     */
    public function addVersion($version): Image
    {
        if (!$this->versions->contains($version)) {
            $this->versions[] = $version;
            $version->setImage($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return Image
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return Image
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return Image
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function __toString(): string
    {
        return (string) ($this->path ?? '');
    }
}
