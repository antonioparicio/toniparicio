<?php

namespace App\Entity\WhoIAm;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\Index;
use App\Repository\WhoIAm\PersonLocaleRepository;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;



/**
 * @ORM\Entity(repositoryClass=PersonLocaleRepository::class)
 * @ORM\Table(
 *     name="who_person_locale",
 *     indexes={@Index(name="updated_idx", columns={"updated_at"})},
 *     uniqueConstraints={
 *        @UniqueConstraint(name="name_unique", columns={"locale", "name"})
 *    }
 * )
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 */
class PersonLocale implements \Stringable
{
    /**
     * number of clues to be a playable person
     * @var int
     */
    public const PLAYABLE_CLUES = 8;
    public const DEFAULT_DIFFICULTY = 5;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Person::class,
     *     inversedBy="locales",
     *     cascade={"persist"},
     *     fetch="LAZY"
     *     )
     * @ORM\JoinColumn(nullable=true)
     * @var Person
     * @Groups("api")
     */
    private $person;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups("api")
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=64)
     * @Groups("api")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", length=64)
     * @Groups("api")
     */
    private bool $is_clue_name = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("api")
     */
    private $description;

    /**
     * birthplace country
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups("api")
     */
    private $country;

    /**
     * birthplace city
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups("api")
     */
    private $city;

    /**
     * death date and/or place, whatever be more relevant to person
     * @ORM\Column(type="string", nullable=true, length=256)
     * @Groups("api")
     */
    private $death;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonLink::class,
     *     mappedBy="person",
     *     cascade={"remove", "persist"},
     *     fetch="LAZY"
     * )
     * @Groups("api")
     */
    private $links;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonWork::class,
     *     mappedBy="person",
     *     cascade={"remove", "persist"},
     *     fetch="LAZY"
     * )
     * @Groups("api")
     * @var PersonWork[] $works
     */
    private $works;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonFact::class,
     *     mappedBy="person",
     *     cascade={"remove", "persist"},
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true
     * )
     * @Groups("api")
     */
    private $facts;

    /**
     * @ORM\OneToMany(
     *     targetEntity=WorkshopFact::class,
     *     mappedBy="person",
     *     cascade={"remove", "persist"},
     *     fetch="LAZY"
     * )
     * @Groups("workshop")
     */
    private $workshopFacts;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonQuote::class,
     *     mappedBy="person",
     *     cascade={"remove", "persist"},
     *     fetch="LAZY"
     * )
     * @Groups("api")
     */
    private $quotes;

    /**
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private int $difficulty = 10;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("api")
     */
    private bool $isPlayable = false;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $clues = 1;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @ORM\OneToMany(
     *     targetEntity=ChatGptQueue::class,
     *     mappedBy="person",
     *     cascade={"remove"},
     *     orphanRemoval=true,
     *     fetch="LAZY"
     * )
     *
     * @var ChatGptQueue[]
     */
    private $chatGptQueues;

    /**
     * @ORM\OneToMany(
     *     targetEntity=ChatGptPrompt::class,
     *     mappedBy="person",
     *     cascade={"remove"},
     *     orphanRemoval=true,
     *     fetch="LAZY"
     * )
     *
     * @var ChatGptPrompt[]
     */
    private $chatGptPrompts;

    /**
     * PersonLocale constructor.
     */
    public function __construct()
    {
        $this->facts = new ArrayCollection();
        $this->works = new ArrayCollection();
        $this->quotes = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Person
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return PersonLocale
     */
    public function setPerson($person): self
    {
        $this->person = $person;
        if ($person && !$this->name) {
            $this->name = $person->getRealName();
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return Person
     */
    public function setLocale(mixed $locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        if (!$this->name && $this->person) {
            return $this->person->getRealName();
        }
        return $this->name;
    }

    public function setName(mixed $name): PersonLocale
    {
        $this->name = str_replace(['_'], [' '], (string) $name);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(mixed $description): PersonLocale
    {
        if (strlen((string) $description) > 4096) {
            $description = substr((string) $description, 0, 4096);
            $length = strrpos($description, PHP_EOL) ?? 4096;
            $description = substr($description, 0 , $length);
        }
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(mixed $country): PersonLocale
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    public function setCity(mixed $city): PersonLocale
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeath()
    {
        return $this->death;
    }

    public function setDeath(mixed $death): PersonLocale
    {
        $this->death = $death;

        return $this;
    }

    /**
     * @return Collection|PersonFact[]
     */
    public function getFacts(): Collection
    {
        return $this->facts;
    }

    /**
     * @param $facts
     * @return $this
     */
    public function setFacts($facts): PersonLocale
    {
        if ((is_countable($facts) ? count($facts) : 0) > 0) {
            foreach ($facts as $fact) {
                $this->addFact($fact);
            }
        }

        return $this;
    }

    /**
     * @param PersonFact $fact
     * @return $this
     */
    public function addFact($fact): PersonLocale
    {
        if (!$this->facts->contains($fact)) {
            $this->facts[] = $fact;
            $fact->setPerson($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeFact(PersonFact $fact): PersonLocale
    {
        if ($this->facts->contains($fact)) {
            $this->facts->removeElement($fact);
            // set the owning side to null (unless already changed)
            if ($fact->getPerson() === $this) {
                $fact->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkshopFacts()
    {
        return $this->workshopFacts;
    }

    /**
     * @return PersonLocale
     */
    public function setWorkshopFacts(mixed $workshopFacts)
    {
        $this->workshopFacts = $workshopFacts;
        return $this;
    }

    /**
     * @return Collection|PersonQuote[]
     */
    public function getQuotes(): Collection
    {
        return $this->quotes;
    }

    /**
     * @param $quotes
     * @return $this
     */
    public function setQuotes($quotes): PersonLocale
    {
        if ((is_countable($quotes) ? count($quotes) : 0) > 0) {
            foreach ($quotes as $quote) {
                $this->addQuote($quote);
            }
        }

        return $this;
    }

    /**
     * @param PersonQuote $quote
     * @return $this
     */
    public function addQuote($quote): PersonLocale
    {
        if (!$this->quotes->contains($quote)) {
            $this->quotes[] = $quote;
            $quote->setPerson($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeQuote(PersonQuote $quote): PersonLocale
    {
        if ($this->quotes->contains($quote)) {
            $this->quotes->removeElement($quote);
            // set the owning side to null (unless already changed)
            if ($quote->getPerson() === $this) {
                $quote->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PersonWork[]
     */
    public function getWorks(): Collection
    {
        return $this->works;
    }

    /**
     * @param $works
     * @return $this
     */
    public function setWorks($works): PersonLocale
    {
        if ((is_countable($works) ? count($works) : 0) > 0) {
            foreach ($works as $work) {
                $this->addWork($work);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addWork(PersonWork $new): PersonLocale
    {
        if ($this->hasWork($new->getTitle())) {
            return $this;
        }
        $this->works[] = $new;
        $new->setPerson($this);
        return $this;
    }

    /**
     * @return bool
     */
    public function hasWork(string $name): bool
    {
        foreach ($this->works as $work) {
            /** @var PersonWork $work */
            if ($work->getTitle() === $name) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function hasQuote(string $name): bool
    {
        foreach ($this->quotes as $quote) {
            /** @var PersonQuote $quote */
            if ($quote->getQuote() === $name) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return $this
     */
    public function removeWork(PersonWork $work): PersonLocale
    {
        if ($this->works->contains($work)) {
            $this->works->removeElement($work);
            // set the owning side to null (unless already changed)
            if ($work->getPerson() === $this) {
                $work->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PersonLink]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    /**
     * @param $links
     * @return $this
     */
    public function setLinks($links): PersonLocale
    {
        if ((is_countable($links) ? count($links) : 0) > 0) {
            foreach ($links as $link) {
                $this->addLink($link);
            }
        }

        return $this;
    }

    /**
     * @param PersonLink $link
     * @return $this
     */
    public function addLink($link): PersonLocale
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setPerson($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeLink(PersonLink $link): PersonLocale
    {
        if ($this->links->contains($link)) {
            $this->links->removeElement($link);
            // set the owning side to null (unless already changed)
            if ($link->getPerson() === $this) {
                $link->setPerson(null);
            }
        }

        return $this;
    }

    public function getLink($type) {

    }

    /**
     * @return int
     */
    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    /**
     * @return Person
     */
    public function setDifficulty(int $difficulty): PersonLocale
    {
        $this->difficulty = $difficulty;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPlayable(): bool
    {
        return $this->isPlayable;
    }

    /**
     * @return Person
     */
    public function setIsPlayable(bool $isPlayable): PersonLocale
    {
        $this->isPlayable = $isPlayable;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function evaluateIsPlayable(): void
    {
        $i  = 1;    // name letters is always a clue
        $i += $this->death ? 1 : 0;
        $i += $this->is_clue_name ? 1 : 0;
        if ($this->person) {
            $i += $this->person->getYear() ? 1 : 0;
            $i += $this->person->getContinent() || $this->country || $this->city ? 1 : 0;
            $i += $this->person->getGender() === 1 || $this->person->getGender() === 2 ? 1 : 0;
            $i += count($this->person->getImages()) > 0 ? 1 : 0;
            $i += (is_countable($this->person->getCategories()) ? count($this->person->getCategories()) : 0) > 0 ? 1 : 0;
        }
        $i += count($this->quotes) > 0 ? 1 : 0;
        $i += count($this->facts) > 0 ? 1 : 0;
        $i += count($this->works) > 0 ? 1 : 0;
        $this->isPlayable = ($i >= self::PLAYABLE_CLUES);
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt(mixed $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(mixed $updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(mixed $deleted_at): void
    {
        $this->deleted_at = $deleted_at;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->deleted_at === null;
    }

    /**
     * @return bool
     */
    public function isIsClueName(): bool
    {
        return $this->is_clue_name;
    }

    /**
     * @return PersonLocale
     */
    public function setIsClueName(bool $is_clue_name): PersonLocale
    {
        $this->is_clue_name = $is_clue_name;
        return $this;
    }

    public function totalFacts(): int
    {
        return count($this->facts);
    }

    public function totalWorks(): int
    {
        return count($this->works);
    }

    public function totalQuotes(): int
    {
        return count($this->quotes);
    }

    /**
     * return name when object is tried to use as string
     *
     * @return string full author name
     * @author Toni Paricio <toniparicio@gmail.com>
     * @since  2018-06-08
     */
    public function __toString(): string
    {
      return (string) $this->name;
    }

    /**
     * @return array
     */
    public function toApi(): array
    {
        return [
            'id' => $this->id,
            'person' => $this->person->toApi(),
            'locale' => $this->getLocale(),
            'name' => $this->getName(),
            'is_clue_name' => $this->isIsClueName(),
            'description' => $this->getDescription(),
            'country' => $this->getCountry(),
            'city' => $this->getCity(),
            'death' => $this->getDeath(),
            'links' => $this->getLinks(),
            'works' => $this->getWorks(),
            'facts' => $this->getFacts(),
            'quotes' => $this->getQuotes(),
            'difficulty' => $this->getDifficulty(),
            'is_playable' => $this->isPlayable(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt()
        ];
    }

    public function getClues(): int
    {
        if (!$this->clues) {
            $this->setClues();
        }
        return $this->clues;
    }

    /**
     * @return PersonLocale
     */
    public function setClues(): PersonLocale
    {
        $clues = 1; // Letters are always a clue
        $gender = $this->person->getGender();
        if ($gender > 0 && $gender != 9) {
            $clues++;
        }
        if (count($this->person->getCategories()) > 0) {
            $clues++;
        }
        if (!empty($this->person->getYear())) {
            $clues++;
        }
        $images = $this->person->getImages()->toArray();
        if (count($images) > 0) {
            $clues++;
            $count = count($images);
            array_filter($images, static function (PersonImage $image) {
                return $image->isPortrait();
            });
            if ($images && $count != count($images)) {
                $clues++;
            }
        }
        if ($this->is_clue_name) {
            $clues++;
        }
        if (!empty($this->person->getContinent()) || !empty($this->getCountry()) || !empty($this->getCity())) {
            $clues++;
        }
        if (!empty($this->getDeath())) {
            $clues++;
        }
        if (count($this->getWorks()) > 0) {
            $clues++;
        }
        if (count($this->getFacts()) > 0) {
            $clues++;
        }
        if (count($this->getQuotes()) > 0) {
            $clues++;
        }
        $this->clues = $clues;
        return $this;
    }
}
