<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_game_round_clue")
 */
class GameRoundClue implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Game $game = null;

    /**
     * @ORM\ManyToOne(targetEntity=GameRound::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?GameRound $round = null;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Player $player = null;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Person $person = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $type = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $identifier = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $free = false;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return GameRoundClue
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): GameRoundClue
    {
        $this->game = $game;

        return $this;
    }

    public function getRound(): GameRound
    {
        return $this->round;
    }

    /**
     * @param GameRound $round
     */
    public function setRound(?GameRound $round): GameRoundClue
    {
        $this->round = $round;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): GameRoundClue
    {
        $this->player = $player;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): GameRoundClue
    {
        $this->person = $person;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): GameRoundClue
    {
        $this->type = $type;

        return $this;
    }

    public function getIdentifier(): ?int
    {
        return $this->identifier;
    }

    public function setIdentifier(?int $identifier): GameRoundClue
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function isFree(): bool
    {
        return $this->free;
    }

    public function setFree(bool $free): GameRoundClue
    {
        $this->free = $free;

        return $this;
    }

    public function __toString()
    {
        return $this->type;
    }
}
