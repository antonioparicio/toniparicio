<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\ChatGptPromptRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=ChatGptPromptRepository::class)
 * @ORM\Table(
 *     name="chatgpt_prompt",
 *     indexes={@Index(name="search_idx", columns={"person_id", "person_locale_id"})}
 * )
 */
class ChatGptPrompt implements \Stringable
{
    /** @see https://openai.com/pricing#language-models */
    public const INPUT_TOKENS_COST = 0.0015 / 1000;
    public const OUTPUT_TOKENS_COST = 0.002 / 1000;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Person::class,
     *     inversedBy="chatGptPrompts",
     *     cascade={"persist"},
     *     fetch="LAZY"
     *     )
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private Person $person;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="chatGptPrompts",
     *     cascade={"persist"},
     *     fetch="LAZY"
     *     )
     * @ORM\JoinColumn(nullable=true)
     */
    private PersonLocale $personLocale;

    /**
     * @ORM\OneToOne(
     *     targetEntity=ChatGptQueue::class,
     *     inversedBy="prompt",
     *     cascade={"persist"},
     *     fetch="LAZY"
     *     )
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?ChatGptQueue $queue = null;

    /**
     * @ORM\Column(type="string")
     */
    private string $type;

    /**
     * @ORM\Column(type="integer")
     */
    private int $inputTokens;

    /**
     * @ORM\Column(type="integer")
     */
    private int $outputTokens;

    /**
     * ChatGptPrompt cost in cents.
     *
     * @ORM\Column(type="float")
     */
    private float $cost;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isSuccess = false;

    /**
     * @ORM\Column(type="text")
     */
    private string $response;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return ChatGptPrompt
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): ChatGptPrompt
    {
        $this->person = $person;

        return $this;
    }

    public function getPersonLocale(): PersonLocale
    {
        return $this->personLocale;
    }

    public function setPersonLocale(PersonLocale $personLocale): ChatGptPrompt
    {
        $this->personLocale = $personLocale;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): ChatGptPrompt
    {
        $this->type = $type;

        return $this;
    }

    public function getInputTokens(): string
    {
        return $this->inputTokens;
    }

    public function setInputTokens(string $inputTokens): ChatGptPrompt
    {
        $this->inputTokens = $inputTokens;

        return $this;
    }

    public function getOutputTokens(): string
    {
        return $this->outputTokens;
    }

    public function setOutputTokens(string $outputTokens): ChatGptPrompt
    {
        $this->outputTokens = $outputTokens;

        return $this;
    }

    public function getCost(): string
    {
        return $this->cost;
    }

    public function setCost(string $cost): ChatGptPrompt
    {
        $this->cost = $cost;

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess ?? false;
    }

    public function setIsSuccess(bool $isSuccess): ChatGptPrompt
    {
        $this->isSuccess = $isSuccess;

        return $this;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function setResponse(string $response): ChatGptPrompt
    {
        $this->response = $response;

        return $this;
    }

    public function getQueue(): ?ChatGptQueue
    {
        return $this->queue;
    }

    public function setQueue(?ChatGptQueue $queue): ChatGptPrompt
    {
        $this->queue = $queue;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(mixed $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function cost(): float
    {
        return $this->inputTokens * self::INPUT_TOKENS_COST +
                $this->outputTokens * self::OUTPUT_TOKENS_COST;
    }

    /**
     * return name when object is tried to use as string.
     *
     * @return string full author name
     *
     * @author Toni Paricio <toniparicio@gmail.com>
     *
     * @since  2018-06-08
     */
    public function __toString(): string
    {
        return "{$this->id}";
    }

    public function toApi(): array
    {
        return [
            'id' => $this->getId(),
            'person' => $this->person->getId(),
            'personLocale' => $this->personLocale->getId(),
            'tokens' => [
                'input' => $this->inputTokens,
                'output' => $this->outputTokens,
                'total' => $this->inputTokens + $this->outputTokens,
                'cost' => $this->cost(),
            ],
            'success' => $this->isSuccess,
            'response' => $this->response,
        ];
    }
}
