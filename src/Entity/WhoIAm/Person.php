<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 * @ORM\Table(
 *     name="who_person",
 *     indexes={@Index(name="updated_idx", columns={"updated_at"})}
 * )
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class Person implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, nullable=true, unique=true)
     * @Groups("api")
     */
    private $realName;

    /**
     * https://en.wikipedia.org/wiki/ISO/IEC_5218
     * 0 => not know
     * 1 => male
     * 2 => female
     * 9 => not applicable.
     *
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $gender;

    /**
     * birthplace continent.
     *
     * @ORM\Column(type="string", nullable=true)
     * @Groups("api")
     */
    private ?string $continent = 'europe';

    /**
     * avoid datetime because we need very old dates
     * date('Y').
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("api")
     */
    private $year;
    /**
     * date('m').
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("api")
     */
    private $month;

    /**
     * date('d').
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("api")
     */
    private $day;

    /**
     * @ORM\ManyToMany(
     *     targetEntity=Category::class,
     *     inversedBy="persons",
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinTable(name="who_person_category")
     * @ORM\OrderBy({"name" = "ASC"})
     * @Groups("api")
     */
    private $categories;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonImage::class,
     *     mappedBy="person",
     *     cascade={"remove", "persist"},
     *     fetch="LAZY"
     * )
     * @Groups("api")
     */
    private $images;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonLocale::class,
     *     mappedBy="person",
     *     cascade={"remove"},
     *     fetch="LAZY"
     * )
     * @Groups("person")
     *
     * @var PersonLocale[]
     */
    private $locales;

    /**
     * Consolidated number of likes for this person.
     *
     * @ORM\Column(type="integer")
     * @Groups("likes")
     */
    private int $likes = 0;

    /**
     * Consolidated number of dislikes for this person.
     *
     * @ORM\Column(type="integer")
     * @Groups("likes")
     */
    private int $dislikes = 0;

    /**
     * @ORM\OneToMany(
     *     targetEntity=PersonComment::class,
     *     mappedBy="person",
     *     cascade={"remove"},
     *     fetch="LAZY"
     * )
     *
     * @var PersonComment[]
     */
    private $comments;

    /**
     * @ORM\OneToMany(
     *     targetEntity=ChatGptQueue::class,
     *     mappedBy="person",
     *     cascade={"remove"},
     *     orphanRemoval=true,
     *     fetch="LAZY"
     * )
     *
     * @var ChatGptQueue[]
     */
    private $chatGptQueues;

    /**
     * @ORM\OneToMany(
     *     targetEntity=ChatGptPrompt::class,
     *     mappedBy="person",
     *     cascade={"remove"},
     *     orphanRemoval=true,
     *     fetch="LAZY"
     * )
     *
     * @var ChatGptPrompt[]
     */
    private $chatGptPrompts;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * Author constructor.
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->locales = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRealName()
    {
        return $this->realName;
    }

    public function setRealName(mixed $realName): void
    {
        $this->realName = $realName;
    }

    /**
     * alias for setRealName for workshop.
     */
    public function setReal(mixed $realName): void
    {
        $this->realName = $realName;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    public function setGender(mixed $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getContinent()
    {
        return $this->continent;
    }

    public function setContinent(mixed $continent): void
    {
        $this->continent = $continent;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    public function setYear(mixed $year): void
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    public function setMonth(mixed $month): void
    {
        $this->month = $month;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    public function setDay(mixed $day): void
    {
        $this->day = $day;
    }

    /**
     * @return Collection|PersonImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param $images
     *
     * @return $this
     */
    public function setImages($images): self
    {
        if ((is_countable($images) ? count($images) : 0) > 0) {
            foreach ($images as $image) {
                $this->addImage($image);
            }
        }

        return $this;
    }

    /**
     * @param PersonImage $image
     *
     * @return $this
     */
    public function addImage($image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setPerson($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeImage(PersonImage $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getPerson() === $this) {
                $image->setPerson(null);
            }
        }

        return $this;
    }

    public function hasImage(string $name): bool
    {
        foreach ($this->images as $image) {
            /** @var PersonImage $image */
            if ($image->getImage() && $image->getImage()->getOriginal() === $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Collection|PersonLocale[]
     */
    public function getLocales(): Collection
    {
        return $this->locales;
    }

    public function getLocale(string $lang): ?PersonLocale
    {
        foreach ($this->getLocales() as $locale) {
            if ($locale->getLocale() === $lang) {
                return $locale;
            }
        }

        return null;
    }

    /**
     * @param $locales
     *
     * @return $this
     */
    public function setLocales($locales): self
    {
        if ((is_countable($locales) ? count($locales) : 0) > 0) {
            foreach ($locales as $locale) {
                $this->addLocale($locale);
            }
        }

        return $this;
    }

    /**
     * @param PersonLocale $locale
     *
     * @return $this
     */
    public function addLocale($locale): self
    {
        if (!$this->locales->contains($locale)) {
            $this->locales[] = $locale;
            $locale->setPerson($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeLocale(PersonLocale $locale): self
    {
        if ($this->locales->contains($locale)) {
            $this->locales->removeElement($locale);
            // set the owning side to null (unless already changed)
            if ($locale->getPerson() === $this) {
                $locale->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * check if some locale is defined for this person.
     *
     * @param PersonLocale $locale
     *
     * @return $this
     */
    public function hasLocale($lang): bool
    {
        foreach ($this->locales as $locale) {
            if ($locale->getLocale() === $lang) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return Person
     */
    public function setCategories(mixed $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return Person
     */
    public function addCategories(mixed $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt(mixed $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(mixed $updated_at): void
    {
        $this->updated_at = $updated_at;
        foreach ($this->locales as $locale) {
            $locale->setUpdatedAt($updated_at);
        }
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(mixed $deleted_at): void
    {
        $this->deleted_at = $deleted_at;
        foreach ($this->locales as $locale) {
            $locale->setDeletedAt($deleted_at);
        }
    }

    public function isActive(): bool
    {
        return null === $this->deleted_at;
    }

    public function getLikes(): int
    {
        return $this->likes;
    }

    public function setLikes(int $likes): Person
    {
        $this->likes = $likes;

        return $this;
    }

    public function getDislikes(): int
    {
        return $this->dislikes;
    }

    public function setDislikes(int $dislikes): Person
    {
        $this->dislikes = $dislikes;

        return $this;
    }

    public function addLike(bool $like = true): Person
    {
        if ($like) {
            ++$this->likes;
        } else {
            // dislike
            ++$this->dislikes;
        }

        return $this;
    }

    public function removeLike(bool $like = true): Person
    {
        if ($like) {
            --$this->likes;
        } else {
            // dislike
            --$this->dislikes;
        }

        return $this;
    }

    public function totalCategories(): int
    {
        return count($this->categories);
    }

    public function totalComments(): int
    {
        return count($this->comments);
    }

    public function __toString(): string
    {
        return $this->realName ?? '';
    }

    public function toApi()
    {
        return [
            'id' => $this->id,
            'realName' => $this->realName,
            'gender' => $this->gender,
            'continent' => $this->continent,
            'year' => $this->year,
            'month' => $this->month,
            'day' => $this->day,
            'categories' => $this->getCategories(),
            'images' => $this->getImages(),
        ];
    }
}
