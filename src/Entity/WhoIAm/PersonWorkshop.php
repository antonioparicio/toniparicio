<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_form_workshop")
 */
class PersonWorkshop
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Player $user = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $locale = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $reason = null;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $isHandled = false;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonWorkshop
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getUser(): ?Player
    {
        return $this->user;
    }

    public function setUser(?Player $user): PersonWorkshop
    {
        $this->user = $user;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): PersonWorkshop
    {
        $this->locale = $locale;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): PersonWorkshop
    {
        $this->name = $name;

        return $this;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function setReason(string $reason): PersonWorkshop
    {
        $this->reason = $reason;

        return $this;
    }

    public function isHandled(): bool
    {
        return $this->isHandled;
    }

    public function setIsHandled(bool $isHandled): PersonWorkshop
    {
        $this->isHandled = $isHandled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonWorkshop
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonWorkshop
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
