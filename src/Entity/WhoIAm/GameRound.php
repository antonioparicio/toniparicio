<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\GameRoundRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=GameRoundRepository::class)
 * @ORM\Table(name="who_game_round")
 */
class GameRound implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Game $game = null;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups("player")
     */
    private ?Player $player = null;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups("api")
     */
    private ?Person $person = null;

    /**
     * @ORM\ManyToOne(targetEntity=PersonLocale::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups("api")
     */
    private ?PersonLocale $personLocale = null;

    /**
     * @ORM\Column(type="string", nullable=true, length=2)
     */
    private ?string $locale = null;

    /**
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private int $score = 0;

    /**
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private int $trophies = 0;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("api")
     */
    private bool $isSuccess = false;

    /**
     * @ORM\OneToMany(
     *     targetEntity=GameRoundClue::class,
     *      mappedBy="round",
     *      cascade={"persist", "remove"},
     *      fetch="EXTRA_LAZY"
     * )
     */
    private $clues;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return GameRound
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): GameRound
    {
        $this->game = $game;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): GameRound
    {
        $this->player = $player;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): GameRound
    {
        $this->person = $person;

        return $this;
    }

    public function getPersonLocale(): ?PersonLocale
    {
        return $this->personLocale;
    }

    public function setPersonLocale(PersonLocale $person): GameRound
    {
        $this->personLocale = $person;
        $this->person = $person->getPerson();
        $this->locale = $person->getLocale();

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $score): GameRound
    {
        $this->locale = $score;

        return $this;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function setScore(int $score): GameRound
    {
        $this->score = $score;

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function setIsSuccess(bool $isSuccess): GameRound
    {
        $this->isSuccess = $isSuccess;

        return $this;
    }

    public function getTrophies(): int
    {
        return $this->trophies;
    }

    public function setTrophies(int $trophies): GameRound
    {
        $this->trophies = $trophies;
        $this->game->addTrophies($trophies);
        $this->player->addTrophies($trophies);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClues()
    {
        return $this->clues;
    }

    public function setClues(mixed $clues): GameRound
    {
        $this->clues = $clues;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function __toString(): string
    {
        $arr = [
            $this->isSuccess ? '✔' : '✖',
            $this->score,
            $this->getPersonLocale()?->getName() ?? $this->getPerson()?->getRealName(),
        ];
        return implode(' ', $arr);
    }
}
