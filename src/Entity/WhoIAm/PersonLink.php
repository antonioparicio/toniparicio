<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_person_link")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonLink implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="works",
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=false)
     * @Groups("links")
     */
    private $person;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLinkType::class,
     *     inversedBy="works",
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=false)
     * @Groups("api")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Groups("api")
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonLink
     */
    public function setId(mixed $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return PersonLink
     */
    public function setPerson(mixed $person): self
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return PersonLinkType|null
     */
    public function getType(): ?PersonLinkType
    {
        return $this->type;
    }

    /**
     * @return PersonLink
     */
    public function setType(mixed $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return PersonLink
     */
    public function setUrl(mixed $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonLink
     */
    public function setCreatedAt(mixed $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonLink
     */
    public function setUpdatedAt(mixed $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonLink
     */
    public function setDeletedAt(mixed $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString(): string
    {
        return (string) $this->url;
    }
}
