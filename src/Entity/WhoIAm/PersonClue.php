<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait PersonClue.
 */
trait PersonClue
{
    /**
     * Player who submit this clue.
     *
     * @ORM\ManyToOne(
     *     targetEntity=Player::class,
     *     cascade={},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"player"})
     *
     * @var Player|null
     */
    protected $author;

    public function getAuthor(): ?Player
    {
        return $this->author;
    }

    /**
     * @return PersonClue
     */
    public function setAuthor(?Player $author): self
    {
        $this->author = $author;

        return $this;
    }
}
