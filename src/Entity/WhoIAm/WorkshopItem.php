<?php

namespace App\Entity\WhoIAm;

/**
 * Trait WorkshopItem.
 */
trait WorkshopItem
{
    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    protected $status = PersonClueInterface::STATUS_ACTIVE;

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
