<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PersonCommentLikeRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=PersonCommentLikeRepository::class)
 * @ORM\Table(
 *     name="who_person_comment_like",
 *     indexes={
 *       @Index(name="search_idx", columns={"type"})
 *     }
 * )
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonCommentLike
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, inversedBy="likes", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity=PersonComment::class, inversedBy="likes", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $comment;

    /**
     * true = like
     * false = dislike.
     *
     * @ORM\Column(type="boolean")
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonCommentLike
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @return PersonCommentLike
     */
    public function setPlayer(mixed $player)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return PersonCommentLike
     */
    public function setComment(mixed $comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return PersonCommentLike
     */
    public function setType(mixed $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonCommentLike
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonCommentLike
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonCommentLike
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
