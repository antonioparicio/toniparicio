<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PersonCommentRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=PersonCommentRepository::class)
 * @ORM\Table(
 *      name="who_person_comment",
 *      indexes={@Index(name="search_idx", columns={"person_locale_id"})}
 *  )
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonComment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, inversedBy="likes", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="comments", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity=PersonLocale::class, inversedBy="comments", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $personLocale;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="comments", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Game $game = null;

    /**
     * @ORM\ManyToOne(targetEntity=GameRound::class, inversedBy="comments", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?GameRound $gameRound = null;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private string $lang = 'es';

    /**
     * @ORM\Column(type="string")
     */
    private $comment;

    /**
     * Consolidated number of likes for this comment.
     *
     * @ORM\Column(type="integer")
     */
    private int $likes = 0;

    /**
     * Consolidated number of dislikes for this comment.
     *
     * @ORM\Column(type="integer")
     */
    private int $dislikes = 0;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): PersonComment
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        return $this->player;
    }

    public function setPlayer(mixed $player): PersonComment
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    public function setPerson(mixed $person): PersonComment
    {
        $this->person = $person;

        return $this;
    }

    public  function getPersonLocale(): ?PersonLocale
    {
        return $this->personLocale;
    }

    public function setPersonLocale(?PersonLocale $personLocale): PersonComment
    {
        $this->personLocale = $personLocale;
        $this->person = $personLocale->getPerson();
        $this->lang = $personLocale->getLocale();
        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): PersonComment
    {
        $this->game = $game;

        return $this;
    }

    public function getGameRound(): ?GameRound
    {
        return $this->gameRound;
    }

    public function setGameRound(?GameRound $gameRound): PersonComment
    {
        $this->gameRound = $gameRound;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    public function setComment(mixed $comment): PersonComment
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    public function setLang(mixed $lang): PersonComment
    {
        $this->lang = $lang;

        return $this;
    }

    public function getLikes(): int
    {
        return $this->likes;
    }

    public function setLikes(int $likes): PersonComment
    {
        $this->likes = $likes;

        return $this;
    }

    public function getDislikes(): int
    {
        return $this->dislikes;
    }

    public function setDislikes(int $dislikes): PersonComment
    {
        $this->dislikes = $dislikes;

        return $this;
    }

    public function addLike(bool $like = true): PersonComment
    {
        if ($like) {
            ++$this->likes;
        } else {
            // dislike
            ++$this->dislikes;
        }

        return $this;
    }

    public function removeLike(bool $like = true): PersonComment
    {
        if ($like) {
            --$this->likes;
        } else {
            // dislike
            --$this->dislikes;
        }

        return $this;
    }

    public function __toString()
    {
        $arr = [
            (string) $this->person,
            $this->comment,
        ];

        return implode(': ', $arr);
    }
}
