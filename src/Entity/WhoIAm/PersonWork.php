<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_person_work")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonWork implements \Stringable
{
    use PersonClue;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="works",
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     * @Groups("works")
     */
    private ?\App\Entity\WhoIAm\PersonLocale $person = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonWorkType::class,
     *     inversedBy="works",
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=false)
     * @Groups("api")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups("api")
     */
    private $title;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Image::class,
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     * @Groups("api")
     */
    private ?\App\Entity\WhoIAm\Image $image = null;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Groups("api")
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * dynamic field for new image in works.
     *
     * @var string
     */
    public $preview;

    public ?UploadedFile $file = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonWork
     */
    public function setId(mixed $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return PersonWork
     */
    public function setPerson(?PersonLocale $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return PersonWork
     */
    public function setType(mixed $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return PersonWork
     */
    public function setTitle(mixed $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return PersonWork
     */
    public function setUrl(mixed $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    public function setImage(?Image $image): PersonWork
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonWork
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonWork
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonWork
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    public function setFile(?UploadedFile $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getPreview(): ?string
    {
        return $this->image ? $this->image->getOriginal() : null;
    }

    public function setPreview(string $preview): PersonWork
    {
        $this->preview = $preview;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->title;
    }
}
