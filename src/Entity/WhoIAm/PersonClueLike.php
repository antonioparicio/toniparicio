<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PersonClueLikeRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Table for store player like/dislike for person clues.
 *
 * @ORM\Entity(repositoryClass=PersonClueLikeRepository::class)
 * @ORM\Table(name="who_person_clue_like")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonClueLike
{
    final public const LIKES_TO_ACCEPT = 10;
    final public const DISLIKES_TO_REJECT = 10;
    final public const MINIMAL_LIKES_RELATION = 0.5;

    final public const CLUE_ACCEPTED = 1;
    final public const CLUE_REJECTED = -1;
    final public const CLUE_WAITING = 0;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true)
     * @Groups("player_id")
     */
    private ?\App\Entity\WhoIAm\Player $player = null;

    /**
     * @ORM\ManyToOne(targetEntity=PersonLocale::class, inversedBy="works", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true)
     */
    private $person;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $type = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $workshop = false;

    /**
     * @ORM\Column(type="integer")
     */
    private int $clue = 0;

    /**
     * Separate like and dislike to better sum and have chance for consolidate.
     *
     * @ORM\Column(type="integer")
     * @Groups("workshop")
     */
    private int $likes = 0;

    /**
     * Separate like and dislike to better sum and have chance for consolidate.
     *
     * @ORM\Column(type="integer")
     * @Groups("workshop")
     */
    private int $dislikes = 0;

//    /**
//     * @ORM\ManyToOne(targetEntity=WorkshopFact::class, inversedBy="likes")
//     * @ORM\JoinColumn(name="clue", referencedColumnName="id")
//     */
//    private $workshop_facts;
//
//    /**
//     * @ORM\ManyToOne(targetEntity=WorkshopDeath::class, inversedBy="likes")
//     * @ORM\JoinColumn(name="clue", referencedColumnName="id")
//     */
//    private $workshop_death;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonClueLike
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): PersonClueLike
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return PersonClueLike
     */
    public function setPerson(mixed $person)
    {
        $this->person = $person;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): PersonClueLike
    {
        $this->type = $type;

        return $this;
    }

    public function getClue(): ?int
    {
        return $this->clue;
    }

    public function setClue(int $clue): PersonClueLike
    {
        $this->clue = $clue;

        return $this;
    }

    public function isWorkshop(): bool
    {
        return $this->workshop;
    }

    public function setWorkshop(bool $workshop): PersonClueLike
    {
        $this->workshop = $workshop;

        return $this;
    }

    public function isLike(): bool
    {
        return $this->likes;
    }

    /**
     * @param int $like : 1 for like, otherwise dislike
     */
    public function setLike(int $like): PersonClueLike
    {
        if ($like > 0) {
            $this->likes = 1;
            $this->dislikes = 0;
        } else {
            $this->likes = 0;
            $this->dislikes = 1;
        }

        return $this;
    }

    public function isDislike(): bool
    {
        return $this->dislikes;
    }

    public function getLikes(): int
    {
        return $this->likes;
    }

    public function setLikes(int $likes): PersonClueLike
    {
        $this->likes = $likes;

        return $this;
    }

    public function getDislikes(): int
    {
        return $this->dislikes;
    }

    public function setDislikes(int $dislikes): PersonClueLike
    {
        $this->dislikes = $dislikes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonClueLike
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonClueLike
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonClueLike
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function handlePublisher(array $result): array
    {
        if ($this->player->isPublisher() && $this->isWorkshop()) {
            if ($this->isLike()) {
                // approve
                $resume['approved'] = self::CLUE_ACCEPTED;
                // generate real clue and delete workshop clue and likes
            }
            if ($this->isDislike()) {
                // refuse
                $resume['approved'] = self::CLUE_REJECTED;
                // delete workshop clue and likes
            }
        }

        return $result;
    }
}
