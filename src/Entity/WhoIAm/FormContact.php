<?php


namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_form_contact")
 */
class FormContact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?\App\Entity\WhoIAm\Player $user = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $locale = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $subject = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $body = null;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return FormContact
     */
    public function setId(mixed $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Player|null
     */
    public function getUser(): ?Player
    {
        return $this->user;
    }

    /**
     * @param Player|null $user
     * @return FormContact
     */
    public function setUser(?Player $user): FormContact
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @return FormContact
     */
    public function setLocale(string $locale): FormContact
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return FormContact
     */
    public function setSubject(string $subject): FormContact
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return FormContact
     */
    public function setBody(string $body): FormContact
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return FormContact
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return FormContact
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }
}