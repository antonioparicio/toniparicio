<?php


namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_category")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class Category implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Groups("api")
     */
    private $name;

    /**
     * wikidata category ID : i.e. Q123456
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Groups("api")
     */
    private ?string $wikipedia = null;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function setName(mixed $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Category
     */
    public function addPerson(mixed $persons)
    {
        $this->persons[] = $persons;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(mixed $deleted_at): void
    {
        $this->deleted_at = $deleted_at;
    }

    /**
     * @return string
     */
    public function getWikipedia(): ?string
    {
        return $this->wikipedia;
    }

    /**
     * @return Category
     */
    public function setWikipedia(string $wikipedia): Category
    {
        $this->wikipedia = $wikipedia;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }
}