<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use \DateTime;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\WhoIAm\SeasonRepository;

/**
 * @ORM\Entity(repositoryClass=SeasonRepository::class)
 * @ORM\Table(name="who_season")
 */
class Season
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private ?int $name = null;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"api"})
     */
    private $started_at;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"api"})
     */
    private $finished_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * Author constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Season
     */
    public function setId(mixed $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getName(): int
    {
        return $this->name;
    }

    /**
     * @return Season
     */
    public function setName(int $name): Season
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartedAt()
    {
        return $this->started_at;
    }

    /**
     * @return Season
     */
    public function setStartedAt(mixed $started_at)
    {
        $this->started_at = $started_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFinishedAt(): DateTime
    {
        return $this->finished_at;
    }

    /**
     * @return Season
     */
    public function setFinishedAt(mixed $finished_at)
    {
        $this->finished_at = $finished_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return Season
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }
}
