<?php

namespace App\Entity\WhoIAm;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_workshop_difficulty")
 */
class WorkshopDifficulty
{
    use PersonClue;
    use WorkshopItem;

    final public const NAME = 'difficulty';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api", "workshop"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="workshopDifficulty",
     *     cascade={},
     *     fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Groups("facts")
     */
    private ?PersonLocale $person = null;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"api", "workshop"})
     */
    private int $difficulty = 1;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @ORM\OneToMany(targetEntity=PersonClueLike::class, mappedBy="workshop_difficulty")
     * @ORM\JoinColumn(name="id", nullable=true, referencedColumnName="clue")
     * @Groups("api")
     */
    private ArrayCollection|PersistentCollection $likes;

    /**
     * WorkshopFact constructor.
     */
    public function __construct()
    {
        $this->likes = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return self
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): ?PersonLocale
    {
        return $this->person;
    }

    public function setPerson(PersonLocale $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        if ($difficulty > 0 && $difficulty <= 10) {
            $this->difficulty = $difficulty;
        }

        return $this;
    }

    /**
     * Return likes/dislikes discriminated for workshop facts.
     *
     * @return mixed
     */
    public function getLikes()
    {
        $criteria = Criteria::create();
        try {
            $criteria->where(Criteria::expr()->eq('type', self::NAME))
                ->andWhere(Criteria::expr()->eq('workshop', 1));
        } catch (\Exception) {
            return new ArrayCollection();
        }

        return $this->likes->matching($criteria);
    }

    /**
     * Return and indexed array with [likes, dislikes] counts.
     *
     * @return mixed
     */
    public function getLikesCount(): array
    {
        $likes = 0;
        $dislikes = 0;
        foreach ($this->getLikes() as $like) {
            /* @var PersonClueLike $like */
            $likes += $like->getLikes();
            $dislikes += $like->getDislikes();
        }

        return [$likes, $dislikes];
    }

    /**
     * return a sort version for api response.
     */
    public function toApi(): array
    {
        [$likes, $dislikes] = $this->getLikesCount();

        return [
            'id' => $this->getId(),
            'difficulty' => $this->getDifficulty(),
            'likes' => $likes,
            'dislikes' => $dislikes,
            'author' => $this->getAuthor()?->toApi(),
        ];
    }
}
