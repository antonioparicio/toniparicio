<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_person_image")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonImage implements \Stringable
{
    use PersonClue;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="images", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups("images")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups("api")
     */
    private ?Image $image = null;

    /**
     * Define if this image is a portrait or a representation of the related person.
     *
     * @ORM\Column(type="boolean")
     * @Groups("api")
     */
    private bool $portrait = false;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @var string
     */
    public $preview;

    /**
     * @var UploadedFile|null
     */
    public ?UploadedFile $file = null;

    /**
     * PersonImage constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): PersonImage
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return PersonImage
     */
    public function setPerson(mixed $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): PersonImage
    {
        $this->image = $image;

        return $this;
    }

    public function isPortrait(): bool
    {
        return $this->portrait;
    }

    public function setPortrait(bool $portrait): PersonImage
    {
        $this->portrait = $portrait;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonImage
     */
    public function setCreatedAt(mixed $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonImage
     */
    public function setUpdatedAt(mixed $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonImage
     */
    public function setDeletedAt(mixed $deleted_at = null): self
    {
        $this->deleted_at = $deleted_at ?? new DateTime();

        return $this;
    }

    public function getPreview(): ?string
    {
        return $this->image ? $this->image->getOriginal() : null;
    }

    public function setPreview(string $preview): PersonImage
    {
        $this->preview = $preview;

        return $this;
    }

    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    public function setFile(?UploadedFile $file): PersonImage
    {
        $this->file = $file;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getId();
    }
}
