<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 * @ORM\Table(name="who_game")
 */
class Game implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups("player")
     */
    private ?Player $player = null;

    /**
     * @ORM\Column(type="string")
     * @Groups("api")
     */
    private ?string $locale = null;

    /**
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private int $score = 0;

    /**
     * @ORM\OneToMany(
     *     targetEntity=GameRound::class,
     *     mappedBy="game",
     *     cascade={"remove"},
     *     fetch="EXTRA_LAZY"
     * )
     * @Groups("api")
     */
    private $rounds;

    /**
     * How many hearts lose player on this game.
     *
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private int $hearts = 0;

    /**
     * How many trophies wins this player on game.
     *
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private int $trophies = 0;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("api")
     */
    private bool $isSuccess = false;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): Game
    {
        $this->id = $id;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): Game
    {
        $this->locale = $locale;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): Game
    {
        $this->player = $player;

        return $this;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function setScore(int $score): Game
    {
        $this->score = $score;

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function setIsSuccess(bool $success): Game
    {
        $this->isSuccess = $success;

        return $this;
    }

    public function getRounds(): array
    {
        if ($this->rounds instanceof ArrayCollection) {
            return $this->rounds->toArray();
        }
        if ($this->rounds instanceof PersistentCollection) {
            return (array)$this->rounds->getIterator();
        }

        return $this->rounds ?? [];
    }

    public function setRounds(mixed $rounds): Game
    {
        $this->rounds = $rounds;

        return $this;
    }

    public function getHearts(): int
    {
        return $this->hearts;
    }

    public function setHearts(int $hearts): Game
    {
        $this->hearts = $hearts;

        return $this;
    }

    public function getTrophies(): int
    {
        return $this->trophies;
    }

    public function setTrophies(int $trophies): Game
    {
        $this->trophies = $trophies;

        return $this;
    }

    public function addTrophies(int $trophies): Game
    {
        $this->trophies += $trophies;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return Game
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return Game
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function __toString()
    {
        $arr = [
            $this->isSuccess ? '✔' : '✖',
            $this->id,
            'Trophies: '.$this->trophies,
        ];

        return implode(' ', $arr);
    }
}
