<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PersonClueShareRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Table for store player like/dislike for person clues.
 *
 * @ORM\Entity(repositoryClass=PersonClueShareRepository::class)
 * @ORM\Table(name="who_person_clue_share")
 */
class PersonClueShare
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true)
     * @Groups("player_id")
     */
    private ?Player $player = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *      inversedBy="clue_shares",
     *      cascade={"persist"},
     *      fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     */
    private $person;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $type = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $clue = null;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(mixed $id): PersonClueShare
    {
        $this->id = $id;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): PersonClueShare
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    public function setPerson(mixed $person): PersonClueShare
    {
        $this->person = $person;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): PersonClueShare
    {
        $this->type = $type;

        return $this;
    }

    public function getClue(): ?int
    {
        return $this->clue;
    }

    public function setClue(?int $clue = null): PersonClueShare
    {
        $this->clue = $clue;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt(mixed $created_at): PersonClueShare
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getType().' '.$this->getClue();
    }
}
