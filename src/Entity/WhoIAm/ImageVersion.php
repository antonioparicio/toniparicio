<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_image_version")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class ImageVersion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="versions", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $image;

    /**
     * @ORM\Column(name="type", type="string")
     * @Groups("api")
     */
    private $type;

    /**
     * @ORM\Column(name="mime", type="string")
     * @Groups("api")
     */
    private ?string $mime = null;

    /**
     * @ORM\Column(name="width", type="integer")
     * @Groups("api")
     */
    private $width;

    /**
     * @ORM\Column(name="height", type="integer")
     * @Groups("api")
     */
    private $height;

    /**
     * @ORM\Column(name="size", type="integer")
     * @Groups("api")
     */
    private $size;

    /**
     * @ORM\Column(name="path", type="string")
     * @Groups("api")
     */
    private $path;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ImageVersion
     */
    public function setId(int $id): ImageVersion
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return ImageVersion
     */
    public function setImage(mixed $image): ImageVersion
    {
        $this->image = $image;
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return ImageVersion
     */
    public function setType(mixed $type): ImageVersion
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getMime(): string
    {
        return $this->mime;
    }

    /**
     * @return ImageVersion
     */
    public function setMime(string $mime): ImageVersion
    {
        $this->mime = $mime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return ImageVersion
     */
    public function setWidth(mixed $width): ImageVersion
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return ImageVersion
     */
    public function setHeight(mixed $height): ImageVersion
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return ImageVersion
     */
    public function setSize(mixed $size): ImageVersion
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return ImageVersion
     */
    public function setPath(mixed $path): ImageVersion
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return ImageVersion
     */
    public function setCreatedAt(mixed $created_at): ImageVersion
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return ImageVersion
     */
    public function setUpdatedAt(mixed $updated_at): ImageVersion
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return ImageVersion
     */
    public function setDeletedAt(mixed $deleted_at): ImageVersion
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }
}
