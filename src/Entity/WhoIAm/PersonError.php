<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PersonErrorRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=PersonErrorRepository::class)
 * @ORM\Table(name="who_person_error")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonError implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Player $user = null;

    /**
     * @ORM\ManyToOne(targetEntity=PersonLocale::class, cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?PersonLocale $person = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $error;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $isHandled = false;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonError
     */
    public function setId(mixed $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUser(): ?Player
    {
        return $this->user;
    }

    public function setUser(?Player $user): PersonError
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return PersonLocale
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return PersonError
     */
    public function setPerson(PersonLocale $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return PersonError
     */
    public function setError(mixed $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function isHandled(): bool
    {
        return $this->isHandled;
    }

    public function setIsHandled(bool $isHandled): PersonError
    {
        $this->isHandled = $isHandled;

        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString(): string
    {
        return (string) $this->error;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonError
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonError
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonError
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
