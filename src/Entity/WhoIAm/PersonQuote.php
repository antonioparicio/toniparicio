<?php


namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_person_quote")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonQuote
{
    use PersonClue;

    /**
     * @var int
     */
    const QUOTE_LENGTH = 192;


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="quotes",
     *     cascade={"persist"},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=true)
     * @Groups("quote")
     */
    private ?\App\Entity\WhoIAm\PersonLocale $person = null;

    /**
     * @ORM\Column(type="string", length=192)
     * @Groups("api")
     */
    private $quote;

    /**
     * Original author when not person quote but a quote about person
     * Book, movie or work where quote was said
     * @ORM\Column(type="string", length=2048, nullable=true)
     * @Groups("api")
     */
    private $footer;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonQuote
     */
    public function setId(mixed $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param PersonLocale $person
     * @return PersonQuote
     */
    public function setPerson(?PersonLocale $person): self
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * @return PersonQuote
     */
    public function setQuote(mixed $quote): self
    {
        $this->quote = $quote;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @return PersonQuote
     */
    public function setFooter(mixed $footer)
    {
        $this->footer = $footer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonQuote
     */
    public function setCreatedAt(mixed $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonQuote
     */
    public function setUpdatedAt(mixed $updated_at): self
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonQuote
     */
    public function setDeletedAt(mixed $deleted_at): self
    {
        $this->deleted_at = $deleted_at;
        return $this;
    }
}
