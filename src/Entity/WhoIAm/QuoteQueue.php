<?php


namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_quote_queue")
 */
class QuoteQueue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=PersonLocale::class, inversedBy="quotes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $person;

    /**
     * @ORM\Column(type="string", length=2048)
     */
    private $quote;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return QuoteQueue
     */
    public function setId(mixed $id): QuoteQueue
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return PersonLocale|null
     */
    public function getPerson(): ?PersonLocale
    {
        return $this->person;
    }

    /**
     * @return QuoteQueue
     */
    public function setPerson(mixed $person): QuoteQueue
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuote(): string
    {
        return $this->quote;
    }

    /**
     * @return QuoteQueue
     */
    public function setQuote(mixed $quote): QuoteQueue
    {
        $this->quote = $quote;
        return $this;
    }
}
