<?php

namespace App\Entity\WhoIAm;

use App\Repository\WhoIAm\PersonLikeRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=PersonLikeRepository::class)
 * @ORM\Table(name="who_person_like")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class PersonLike implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, inversedBy="likes", cascade={"persist"})
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="likes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private Person $person;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="likes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Game $game = null;

    /**
     * @ORM\ManyToOne(targetEntity=GameRound::class, inversedBy="likes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?GameRound $gameRound = null;

    /**
     * true = like
     * false = dislike.
     *
     * @ORM\Column(type="boolean")
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PersonLike
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @return PersonLike
     */
    public function setPlayer(mixed $player)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return PersonLike
     */
    public function setPerson(mixed $person)
    {
        $this->person = $person;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): PersonLike
    {
        $this->game = $game;

        return $this;
    }

    public function getGameRound(): ?GameRound
    {
        return $this->gameRound;
    }

    public function setGameRound(?GameRound $gameRound): PersonLike
    {
        $this->gameRound = $gameRound;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return PersonLike
     */
    public function setType(mixed $like)
    {
        $this->type = $like;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return PersonLike
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return PersonLike
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return PersonLike
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    public function __toString()
    {
        $arr = [
            $this->type ? '✔' : '✖',
            (string) $this->person,
        ];

        return implode(' ', $arr);
    }
}
