<?php

namespace App\Entity\WhoIAm;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="who_workshop_attribute")
 */
class WorkshopAttribute
{
    use PersonClue;
    use WorkshopItem;

    final public const NAME = 'death';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api", "workshop"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=PersonLocale::class,
     *     inversedBy="workshopDeath",
     *     cascade={},
     *     fetch="LAZY"
     * )
     * @ORM\JoinColumn(nullable=false)
     * @Groups("facts")
     */
    private $person;

    /**
     * @ORM\Column(type="string", length=32)
     * @Groups({"api", "workshop"})
     */
    private $attribute;

    /**
     * More extense attribute is description with 4096 possible characters.
     *
     * @ORM\Column(type="string", length=4096)
     * @Groups({"api", "workshop"})
     */
    private $value;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Groups("api")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("api")
     */
    private $deleted_at;

    public function getAuthor(): ?Player
    {
        return $this->author;
    }

    public function setAuthor(?Player $author): WorkshopAttribute
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return WorkshopAttribute
     */
    public function setId(mixed $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return WorkshopAttribute
     */
    public function setPerson(mixed $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @return WorkshopAttribute
     */
    public function setAttribute(mixed $attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return WorkshopAttribute
     */
    public function setValue(mixed $value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return WorkshopAttribute
     */
    public function setCreatedAt(mixed $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return WorkshopAttribute
     */
    public function setUpdatedAt(mixed $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @return WorkshopAttribute
     */
    public function setDeletedAt(mixed $deleted_at)
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    /**
     * return a sort version for api response.
     */
    public function toApi(): array
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getAttribute(),
            'value' => $this->getValue(),
            'author' => $this->getAuthor() ? $this->getAuthor()->toApi() : null,
        ];
    }
}
