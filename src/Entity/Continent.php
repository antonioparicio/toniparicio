<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class Continent implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=2)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * @ORM\OneToMany(targetEntity=Continent::class, mappedBy="continent")
     */
    private $countries;

    /**
     * Continent constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Continent
     */
    public function setCode(mixed $code): Continent
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Continent
     */
    public function setName(mixed $name): Continent
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return Continent
     */
    public function setModel(mixed $model): Continent
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString(): string
    {
      return (string) $this->name;
    }
}
