<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicTacTocMoveRepository")
 */
class TicTacTocMove
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TicTacTocRound", inversedBy="moves")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?\App\Entity\TicTacTocRound $round = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $player = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $bitboard = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTimeInterface $created_at = null;

    public function getId()
    {
        return $this->id;
    }

    public function getRound(): ?TicTacTocRound
    {
        return $this->round;
    }

    public function setRound(?TicTacTocRound $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getPlayer(): ?int
    {
        return $this->player;
    }

    public function setPlayer(int $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getBitboard(): ?int
    {
        return $this->bitboard;
    }

    public function setBitboard(int $bitboard): self
    {
        $this->bitboard = $bitboard;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
