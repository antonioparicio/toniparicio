<?php

// src\App\Entity\Image

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Image as ImageTrait;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    use ImageTrait;

    public $uploadPath = 'upload'; //directory name which you upload your images. you can change it

    /**
    * @var integer
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
    * Get id
    *
    * @return integer
    */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Get full path to image
    */
    public function getWebPath()
    {
        $webPath = 'img/' . $this->uploadPath. '/' . $this->filename;

        return $webPath;
    }
}
