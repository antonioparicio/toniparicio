<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted_at", timeAware=false)
 */
class Country implements \Stringable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $full_name;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $iso3;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity=Continent::class, inversedBy="countries")
     * @ORM\JoinColumn(name="continent", referencedColumnName="code", nullable=false)
     */
    private $continent;

    /**
     * @ORM\Column(type="integer")
     */
    private $display_order;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Country
     */
    public function setId(mixed $id): Country
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Country
     */
    public function setCode(mixed $code): Country
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Country
     */
    public function setName(mixed $name): Country
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @return Country
     */
    public function setFullName(mixed $full_name): Country
    {
        $this->full_name = $full_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIso3()
    {
        return $this->iso3;
    }

    /**
     * @return Country
     */
    public function setIso3(mixed $iso3): Country
    {
        $this->iso3 = $iso3;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return Country
     */
    public function setNumber(mixed $number): Country
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return Continent
     */
    public function getContinent(): Continent
    {
        return $this->continent;
    }

    /**
     * @return Country
     */
    public function setContinent(mixed $continent): Country
    {
        $this->continent = $continent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplayOrder()
    {
        return $this->display_order;
    }

    /**
     * @return Country
     */
    public function setDisplayOrder(mixed $display_order): Country
    {
        $this->display_order = $display_order;
        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString(): string
    {
      return (string) $this->name;
    }
}
