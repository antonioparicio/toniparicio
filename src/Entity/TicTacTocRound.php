<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicTacTocRoundRepository")
 */
class TicTacTocRound
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $bitboard1 = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $bitboard2 = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $turn = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $result = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTimeInterface $created_at = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTimeInterface $updated_at = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $deleted_at = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TicTacTocMove", mappedBy="round", orphanRemoval=true)
     */
    private \Doctrine\Common\Collections\ArrayCollection|array $moves;

    public function __construct()
    {
        $this->moves = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBitboard1(): ?int
    {
        return $this->bitboard1;
    }

    public function setBitboard1(int $bitboard1): self
    {
        $this->bitboard1 = $bitboard1;

        return $this;
    }

    public function getBitboard2(): ?int
    {
        return $this->bitboard2;
    }

    public function setBitboard2(int $bitboard2): self
    {
        $this->bitboard2 = $bitboard2;

        return $this;
    }

    public function getTurn(): ?int
    {
        return $this->turn;
    }

    public function setTurn(int $turn): self
    {
        $this->turn = $turn;

        return $this;
    }

    public function getResult(): ?int
    {
        return $this->result;
    }

    public function setResult(?int $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }

    /**
     * @return Collection|TicTacTocMove[]
     */
    public function getMoves(): Collection
    {
        return $this->moves;
    }

    public function addMove(TicTacTocMove $move): self
    {
        if (!$this->moves->contains($move)) {
            $this->moves[] = $move;
            $move->setRound($this);
        }

        return $this;
    }

    public function removeMove(TicTacTocMove $move): self
    {
        if ($this->moves->contains($move)) {
            $this->moves->removeElement($move);
            // set the owning side to null (unless already changed)
            if ($move->getRound() === $this) {
                $move->setRound(null);
            }
        }

        return $this;
    }
}
