<?php
// src/Admin/BookAdmin.php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;

class BookAdmin extends AbstractAdmin
{
    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('title', TextType::class)
          ->add('link', TextType::class, ['required' => false])
          ->add('author', ModelType::class, [
              'by_reference' => false,
              'property' => 'name',
              'multiple' => false,
              'expanded' => false,
              'btn_add' => 'Crear Autor',
          ]);
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
        $datagridMapper->add('author');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('author')
            ->add('created_at')
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }
}
