<?php
// src/Admin/SkillAdmin.php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;

class PostAdmin extends AbstractAdmin
{
    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('author')
          ->add('title', TextType::class)
          ->add('subtitle', TextType::class)
          ->add('image', AdminType::class, [
                'required' => false,
                'btn_add' => false,
                'delete' => false,
                'label' => false,
          ])
          ->add('content', TextareaType::class, ['attr' => ['class' => 'tinymce', 'data-theme' => 'advanced']])
          ->add('tags', ModelType::class, [
              'by_reference' => false,
              'property' => 'name',
              'multiple' => true,
              'expanded' => false,
              'btn_add' => 'Crear Tag',
          ]);
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
        $datagridMapper->add('published', 'doctrine_orm_callback', [
            'callback' => function($queryBuilder, $alias, $field, $value) {
                    if (!$value['value']) {
                        return;
                    }

                    $queryBuilder->andWhere($alias . '.published_at IS NULL');

                    return true;
            },
            'field_type' => CheckboxType::class,
            'label' => 'No publicados'
        ]);
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('author')
            ->add('created_at')
            ->add('published_at')
            ->add('_actions', 'actions', [
                'actions' => [
                    'publish' => [
                        'template' => 'admin/CRUD/list_action_publish.html.twig'
                    ],
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }

    /**
     * add custom publish route for action button
     *
     * @param  RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('publish', $this->getRouterIdParameter().'/publish');
    }
}
