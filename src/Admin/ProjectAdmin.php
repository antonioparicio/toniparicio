<?php
// src/Admin/ProjectAdmin.php
namespace App\Admin;

use App\Entity\ProjectAchievement;
use App\Form\Type\PersonLinkType;
use App\Form\Type\ProjectAchievementType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\AdminType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

/**
 * Class ProjectAdmin
 * @package App\Admin
 */
class ProjectAdmin extends AbstractAdmin
{
    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('name', TextType::class)
          ->add('description', TextType::class, ['required' => false])
          ->add('url', TextType::class, ['required' => false])
          ->add('github', TextType::class, ['required' => false])
          ->add('image', AdminType::class, [
                'required' => false,
                'btn_add' => false,
                'delete' => false,
                'label' => false,
          ])
          ->add('achievements', CollectionType::class, [
              'entry_type' => ProjectAchievementType::class,
              'required' => false,
              'allow_add' => true,
//              'btn_add' => 'Añadir Logro',
              'label' => 'Logros',
              'help' => 'Crea, modifica o elimina logros para este proyecto',
              'allow_delete' => true, // True if you want to allow deleting entries
              'by_reference' => false,
          ]);
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('url')
            ->add('created_at')
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }

    public function prePersist($project)
    {
        $this->preUpdate($project);
    }

    public function preUpdate($project)
    {
        $project->setAchievements($project->getAchievements());
    }
}
