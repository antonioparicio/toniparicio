<?php

// src/Admin/AuthorAdmin.php

namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Class ChatGptPromptAdmin.
 */
class ChatGptPromptAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('retry', $this->getRouterIdParameter().'/retry')
            ->add('fix', $this->getRouterIdParameter().'/fix');
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id', 'integer')
            ->add('person', FieldDescriptionInterface::TYPE_MANY_TO_ONE)
            ->add('personLocale.locale', FieldDescriptionInterface::TYPE_STRING)
            ->add('personLocale', FieldDescriptionInterface::TYPE_MANY_TO_ONE)
            ->add('type', FieldDescriptionInterface::TYPE_STRING)
            ->add('inputTokens', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('outputTokens', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('cost', FieldDescriptionInterface::TYPE_CURRENCY)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME)
            ->add('isSuccess', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('response', 'json');
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component.
     *
     * @param FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('isSuccess', CheckboxType::class, ['required' => false])
            ->add('response', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => 8,
                ],
            ]);
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('person')
            ->add('personLocale')
            ->add('type')
            ->add('inputTokens')
            ->add('outputTokens')
            ->add('cost')
            ->add('isSuccess')
            ->add('response', StringFilter::class)
        ;
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('person', FieldDescriptionInterface::TYPE_MANY_TO_ONE, [
                'route' => ['name' => 'edit'],
            ])
            ->add('personLocale.locale', FieldDescriptionInterface::TYPE_STRING, ['label' => 'Locale'])
            ->add('personLocale', FieldDescriptionInterface::TYPE_MANY_TO_ONE, [
                'route' => ['name' => 'edit'],
            ])
            ->add('queue', FieldDescriptionInterface::TYPE_ONE_TO_ONE)
            ->add('type', FieldDescriptionInterface::TYPE_STRING)
            ->add('inputTokens', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('outputTokens', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('cost', FieldDescriptionInterface::TYPE_FLOAT)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('isSuccess', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'retry' => [
                        'template' => 'sonata/whoiam/chatgpt/retry_prompt.html.twig',
                    ],
                    'fix' => [
                        'template' => 'sonata/whoiam/chatgpt/fix_prompt.html.twig',
                    ],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureBatchActions($actions)
    {
        $actions['retry'] = [
            'ask_confirmation' => true
        ];

        return parent::configureBatchActions($actions);
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'id';
    }
}
