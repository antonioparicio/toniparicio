<?php

// src/Admin/AuthorAdmin.php

namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class PersonRoundAdmin.
 */
class GameRoundAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('id')
            ->add('game')
            ->add('player')
            ->add('person')
            ->add('personLocale')
            ->add('isSuccess', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('score', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('trophies', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('clues', FieldDescriptionInterface::TYPE_ONE_TO_MANY)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime());
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component.
     *
     * @param FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('player');
        $datagridMapper->add('person');
        $datagridMapper->add('score');
        $datagridMapper->add('success');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', FieldDescriptionInterface::TYPE_INTEGER, [
                'route' => ['name' => 'show'],
            ])
            ->add('game')
            ->add('player')
            ->add('person')
            ->add('personLocale')
            ->add('isSuccess', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('score', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('trophies', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('clues', FieldDescriptionInterface::TYPE_ONE_TO_MANY)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                ],
            ]);
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'id';
    }
}
