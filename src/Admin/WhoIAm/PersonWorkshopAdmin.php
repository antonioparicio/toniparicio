<?php

// src/Admin/AuthorAdmin.php

namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class PersonWorkshopAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('handle', $this->getRouterIdParameter().'/handle');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('user')
            ->add('locale', FieldDescriptionInterface::TYPE_STRING)
            ->add('name', FieldDescriptionInterface::TYPE_STRING)
            ->add('reason', FieldDescriptionInterface::TYPE_STRING)
            ->add('isHandled', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
        ;
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component.
     *
     * @param FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('user')
            ->add('locale')
            ->add('name')
            ->add('reason')
            ->add('created_at')
        ;
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('user');
        $datagridMapper->add('locale');
        $datagridMapper->add('name');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['route' => ['name' => 'show']])
            ->add('user', FieldDescriptionInterface::TYPE_MANY_TO_ONE, ['route' => ['name' => 'show']])
            ->add('locale', FieldDescriptionInterface::TYPE_STRING)
            ->add('name', FieldDescriptionInterface::TYPE_STRING)
            ->add('isHandled', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'handle' => [
                        'template' => 'sonata/whoiam/workshop/handle.html.twig',
                    ],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'id';
    }
}
