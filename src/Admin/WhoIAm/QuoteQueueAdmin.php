<?php
// src/Admin/AuthorAdmin.php
namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class QuoteQueueAdmin
 * @package App\Admin\WhoIAm
 */
class QuoteQueueAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('accept', $this->getRouterIdParameter().'/accept');
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('person', ModelType::class, [
                'by_reference' => false,
                'property' => 'name',
                'multiple' => false,
                'expanded' => false,
                'btn_add' => false,
            ])
            ->add('quote', TextType::class);
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('person');
        $datagridMapper->add('person.locale');
        $datagridMapper->add('quote');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('person')
            ->add('person.locale')
            ->add('quote')
            ->add('_actions', 'actions', [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                    'accept' => [
                        'template' => 'sonata/whoiam/quote_queue/process.html.twig',
                    ]
                ]
            ]);
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'created_at';
    }
}