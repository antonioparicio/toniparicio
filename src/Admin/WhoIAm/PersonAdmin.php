<?php
// src/Admin/AuthorAdmin.php
namespace App\Admin\WhoIAm;

use App\Entity\Image;
use App\Entity\WhoIAm\Category;
use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLocale;
use App\Form\Type\PersonImageType;
use App\Util\DateTime\AdminDateTimeZone;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class PersonAdmin
 * @package App\Admin\WhoIAm
 */
class PersonAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    private const LOCALES = ['es', 'en'];

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('wikipedia');
        $collection->add('wikipedia_add');
    }

    /**
     * @param string $action
     * @param null $object
     * @return array
     */
    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        $list['import']['template'] = 'sonata/wikipedia_button.html.twig';

        return $list;
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        /** @see Person::class */
        $show
            ->add('id')
            ->add('realName')
            ->add('gender')
            ->add('continent')
            ->add('year')
            ->add('month')
            ->add('day')
            ->add('locales')
            ->add('images')
            ->add('isActive', 'boolean')
            ->add('totalCategories', 'int', ['label' => 'Categories'])
            ->add('totalComments', 'int', ['label' => 'Comments'])
            ->add('likes', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('dislikes', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('created_at', FieldDescriptionInterface::TYPE_DATETIME)
            ->add('updated_at', FieldDescriptionInterface::TYPE_DATETIME);
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('realName', TextType::class, ['required' => false])
            ->add('gender', ChoiceType::class, [
                'empty_data' => 1,
                'choices' => [
                    'Male' => 1,
                    'Female' => 2,
                    'Not know' => 0,
                    'No applicable' => 9,
                ]
            ])
            ->add('continent', ChoiceType::class, [
              'required' => false,
              'empty_data' => null,
              'choices' => [
                  'Desconocido' => null,
                  'Europa' => 'europe',
                  'América' => 'america',
                  'Asia' => 'asia',
                  'África' => 'africa',
                  'Oceanía' => 'oceania',
              ]
            ])
            ->add('year', IntegerType::class, ['required' => false])
            ->add('month', IntegerType::class, ['required' => false])
            ->add('day', IntegerType::class, ['required' => false])
            ->add('categories', EntityType::class, [
                'class' => Category::class,
                'multiple' => true,
                'expanded' => true
            ])
            ->add('images', CollectionType::class, [
                'entry_type' => PersonImageType::class,
                'allow_add' => true,
                'allow_delete' => true, // True if you want to allow deleting entries
                'by_reference' => false,
            ])
            ;

    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('realName', null, [
            'show_filter' => true
        ]);
        $datagridMapper->add('categories');
        $datagridMapper->add('continent');
        $datagridMapper->add('gender');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('realName')
            ->add('locales', FieldDescriptionInterface::TYPE_ONE_TO_MANY, [
                'route' => ['name' => 'edit'],
            ])
            ->add('isActive', 'boolean')
            ->add('totalCategories', 'int', ['label' => 'Categories'])
            ->add('totalComments', 'int', ['label' => 'Comments'])
            ->add('likes', 'integer')
            ->add('dislikes', 'integer')
            ->add('updatedAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }

    /**
     * Kinda Hackish methods to fix potential bug with SonataAdminBundle. I have not
     * confirmed this is necessary but I've seen this implemented more than once.
     */
    /**
     * @param $person
     */
    public function prePersist($person)
    {
        /** @var Person $person */
        foreach ($person->getLocales() as $locale) {
            $this->getManager()->persist($locale);
        }
//        $this->preUpdate($person);
    }

    /**
     * @param $person
     */
    public function preUpdate($person)
    {
//        $person->setFacts($person->getFacts());
    }

    /**
     * @param $person
     * @throws \Exception
     */
    public function postUpdate($person)
    {
        $manager = $this->getManager();

        if ($service = $this->getConfigurationPool()->getContainer()->get('image.resize')) {
            /** @var Person $person */
            foreach ($person->getImages() as $personImage)
            {
                if ($preview = $personImage->preview) {
                    // if new images then process on the fly
                    $image = new \App\Entity\WhoIAm\Image();
                    $image->setPerson($person);
                    $image->setPath($preview);
                    $manager->persist($image);
                    $manager->flush();
                    $image = $service->handle($image, 'person');
                    $personImage->setImage($image);
                }
                $file = $personImage->getFile();
                if ($file && in_array($file->getMimeType(), \App\Entity\WhoIAm\Image::MIME_TYPE_ALLOWED)) {
                    $image = new \App\Entity\WhoIAm\Image();
                    $image->setPerson($person);
                    $image->setOriginal($file->getFileInfo()->getPathname());
                    $manager->persist($image);
                    $manager->flush();
                    $image = $service->handle($image, 'person');
                    $personImage->setImage($image);
                }
            }
        }

        foreach (self::LOCALES as $lang) {
            $locale = $person->getLocale($lang);
            if (!$locale) {
                $locale = new PersonLocale();
                $locale->setPerson($person);
                $locale->setLocale($lang);
            }
            // always persist locale to update updated_at time for synchronizations
            $locale->setUpdatedAt($person->getUpdatedAt());
            $manager->persist($locale);
        }
        $manager->flush();

        parent::postUpdate($person);
    }

    /**
     * @return EntityManager
     */
    private function getManager(): EntityManager
    {
        $container = $this->getConfigurationPool()->getContainer();

        return $container->get('doctrine.orm.entity_manager');
    }

    /**
     * @param array $sortValues
     */
    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'ASC';
        $sortValues['_sort_by'] = 'realName';
    }
}