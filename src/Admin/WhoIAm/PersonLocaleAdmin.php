<?php

namespace App\Admin\WhoIAm;

use App\Entity\WhoIAm\Image;
use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\PersonWork;
use App\Form\Type\PersonFactType;
use App\Form\Type\PersonLinkType;
use App\Form\Type\PersonQuoteType;
use App\Form\Type\PersonWorkType;
use App\Util\DateTime\AdminDateTimeZone;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PersonLocaleAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('chatGpt', $this->getRouterIdParameter().'/chatGpt')
            ->add('death', $this->getRouterIdParameter().'/chatGpt/death')
            ->add('works', $this->getRouterIdParameter().'/chatGpt/works')
            ->add('facts', $this->getRouterIdParameter().'/chatGpt/facts')
            ->add('quotes', $this->getRouterIdParameter().'/chatGpt/quotes')
            ->add('biography', $this->getRouterIdParameter().'/chatGpt/biography')
        ;
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component.
     *
     * @param FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('person', EntityType::class, [
                'class' => Person::class,
                'multiple' => false,
            ])
            ->add('locale', ChoiceType::class, [
                'empty_data' => 'es',
                'choices' => [
                    'Español' => 'es',
                    'English' => 'en',
                ],
            ])
            ->add('name', TextType::class)
            ->add('is_clue_name', CheckboxType::class, [
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => 8,
                ],
            ])
            ->add('difficulty', ChoiceType::class, [
                'empty_data' => '1',
                'choices' => array_combine(range(1, 10), range(1, 10)),
            ])
            ->add('country', TextType::class, [
                'required' => false,
                'attr' => ['class' => 'col-md-3'],
            ])
            ->add('city', TextType::class, ['required' => false])
            ->add('death', TextType::class, ['required' => false])
            ->add('links', CollectionType::class, [
                'entry_type' => PersonLinkType::class,
                'allow_add' => true,
                'allow_delete' => true, // True if you want to allow deleting entries
                'by_reference' => false,
            ])
            ->add('works', CollectionType::class, [
                'entry_type' => PersonWorkType::class,
                'allow_add' => true,
                'allow_delete' => true, // True if you want to allow deleting entries
                'by_reference' => false,
            ])
            ->add('facts', CollectionType::class, [
                'entry_type' => PersonFactType::class,
                'allow_add' => true,
                'allow_delete' => true, // True if you want to allow deleting entries
                'by_reference' => false,
            ])
            ->add('quotes', CollectionType::class, [
                'entry_type' => PersonQuoteType::class,
                'allow_add' => true,
                'allow_delete' => true, // True if you want to allow deleting entries
                'by_reference' => false,
            ]);
    }

    public function getActionButtons($action, $object = null)
    {
        $actions = parent::getActionButtons($action, $object);

        $actions['chatGpt'] = ['template' => 'sonata/whoiam/chatgpt/process.html.twig'];
        $actions['death'] = ['template' => 'sonata/whoiam/chatgpt/death.html.twig'];
        $actions['works'] = ['template' => 'sonata/whoiam/chatgpt/works.html.twig'];
        $actions['facts'] = ['template' => 'sonata/whoiam/chatgpt/facts.html.twig'];
        $actions['quotes'] = ['template' => 'sonata/whoiam/chatgpt/quotes.html.twig'];
        $actions['biography'] = ['template' => 'sonata/whoiam/chatgpt/biography.html.twig'];

        return $actions;
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('locale');
        $datagridMapper->add('person');
        $datagridMapper->add('difficulty');
        $datagridMapper->add('name', null, [
            'show_filter' => true,
        ]);
        $datagridMapper->add('isPlayable');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('person', FieldDescriptionInterface::TYPE_MANY_TO_ONE, [
                'route' => ['name' => 'edit'],
            ])
            ->add('locale')
            ->addIdentifier('name', 'string')
            ->add('difficulty', 'integer')
            ->add('isPlayable', 'boolean')
            ->add('totalFacts', 'int', ['label' => 'Facts'])
            ->add('totalWorks', 'int', ['label' => 'Works'])
            ->add('totalQuotes', 'int', ['label' => 'Quotes'])
            ->add('updated_at', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('isActive', 'boolean')
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ],
            ]);
    }

    /**
     * Kinda Hackish methods to fix potential bug with SonataAdminBundle. I have not
     * confirmed this is necessary but I've seen this implemented more than once.
     */
    /**
     * @param $person
     */
    public function prePersist($person)
    {
//        $this->preUpdate($person);
    }

    /**
     * @param $person
     */
    public function preUpdate($person)
    {
//        $person->setFacts($person->getFacts());
        /* @var PersonLocale $person */
        $person->evaluateIsPlayable();
    }

    /**
     * @param $person
     *
     * @throws \Exception
     */
    public function postUpdate($person)
    {
        if (!$service = $this->getConfigurationPool()->getContainer()->get('image.resize')) {
            return;
        }
        $manager = $this->getManager();
        /** @var PersonLocale $person */
        foreach ($person->getWorks() as $work) {
            $preview = $work->preview;
            $file = $work->getFile();

            if ($preview || $file) {
                // if new images then process on the fly
                $image = new Image();
                $image->setPerson($person->getPerson());

                if ($file && in_array($file->getMimeType(), Image::MIME_TYPE_ALLOWED)) {
                    $image->setOriginal($file->getFileInfo()->getPathname());
                } else {
                    $image->setPath($preview);
                }
                $manager->persist($image);
                $manager->flush();
                $image = $service->handle($image, 'work');
                $work->setImage($image);

                if ($parent = $person->getPerson()) {
                    foreach ($parent->getLocales() as $locale) {
                        if ($locale->getLocale() !== $person->getLocale()) {
                            // add same work to another locales TODO translation
                            $localeWork = new PersonWork();
                            $localeWork->setImage($image);
                            $localeWork->setTitle($work->getTitle());
                            $localeWork->setType($work->getType());
                            $localeWork->setUrl($work->getUrl());
                            $localeWork->setPerson($locale);
                            $manager->persist($localeWork);
                        }
                    }
                }
            }
        }
        $manager->flush();
        parent::postUpdate($person);
    }

    private function getManager(): EntityManager
    {
        $container = $this->getConfigurationPool()->getContainer();

        return $container->get('doctrine.orm.entity_manager');
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'id';
    }
}
