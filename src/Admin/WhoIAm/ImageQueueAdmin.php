<?php
// src/Admin/AuthorAdmin.php
namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ImageQueueAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('process', $this->getRouterIdParameter().'/process');
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('person', ModelType::class, [
                'by_reference' => false,
                'property' => 'realName',
                'multiple' => false,
                'expanded' => false,
                'btn_add' => false,
            ])
            ->add('path', TextType::class)
            ->add('type', ChoiceType::class, [
                'empty_data' => 1,
                'choices' => [
                    'person' => 'person',
                    'work' => 'work'
                ]
            ])
            ->add('status', ChoiceType::class, [
                'empty_data' => 1,
                'choices' => [
                    'queued' => 'queued',
                    'processing' => 'processing',
                    'processed' => 'processed',
                    'error' => 'error'
                ]
            ]);

    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('person');
        $datagridMapper->add('path');
        $datagridMapper->add('type');
        $datagridMapper->add('status');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('person')
            ->add('filename')
            ->add('type')
            ->add('status')
            ->add('updatedAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                    'process' => [
                        'template' => 'sonata/whoiam/image_queue/process.html.twig',
                    ]
                ]
            ]);
    }

    /**
     * @param array $sortValues
     */
    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'updated_at';
    }
}