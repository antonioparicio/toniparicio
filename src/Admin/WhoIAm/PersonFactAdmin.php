<?php
// src/Admin/AuthorAdmin.php
namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PersonFactAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('person', ModelType::class, [
              'by_reference' => false,
              'property' => 'name',
              'multiple' => false,
              'expanded' => false,
              'btn_add' => false,
          ])
          ->add('name', TextType::class)
        ;

    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }

    /**
     * Kinda Hackish methods to fix potential bug with SonataAdminBundle. I have not
     * confirmed this is necessary but I've seen this implemented more than once.
     */
    /**
     * @param $person
     */
    public function prePersist($person)
    {
        $this->preUpdate($person);
    }

    /**
     * @param $person
     */
    public function preUpdate($person)
    {
        $person->setImages($person->getFacts());
    }
}