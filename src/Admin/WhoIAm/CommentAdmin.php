<?php

// src/Admin/AuthorAdmin.php

namespace App\Admin\WhoIAm;

use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\Player;
use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class CommentAdmin.
 */
class CommentAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component.
     *
     * @param FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('player', EntityType::class, [
                'class' => Player::class,
                'multiple' => false,
            ])
            ->add('person', Person::class, [
                'class' => Player::class,
                'multiple' => false,
            ])
            ->add('lang', TextType::class)
            ->add('comment', TextType::class)
            ->add('likes', IntegerType::class)
            ->add('dislikes', IntegerType::class);
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('player');
        $datagridMapper->add('person');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('player', FieldDescriptionInterface::TYPE_MANY_TO_ONE)
            ->add(
                'person',
                FieldDescriptionInterface::TYPE_MANY_TO_ONE,
                ['route' => ['name' => 'edit'], 'header_style' => 'width: 15%;']
            )
            ->add('lang', FieldDescriptionInterface::TYPE_STRING)
            ->add('comment', FieldDescriptionInterface::TYPE_TEXTAREA)
            ->add('likes', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('dislikes', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'created_at';
    }
}
