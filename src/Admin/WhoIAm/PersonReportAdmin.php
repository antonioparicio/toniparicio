<?php

// src/Admin/AuthorAdmin.php

namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class PersonReportAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('handle', $this->getRouterIdParameter().'/handle');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('user')
            ->add('person')
            ->add('error')
            ->add('createdAt')
        ;
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component.
     *
     * @param FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('user')
            ->add('person')
            ->add('error')
            ->add('isHandled')
            ->add('createdAt')
        ;
    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('user');
        $datagridMapper->add('person');
        $datagridMapper->add('error');
        $datagridMapper->add('isHandled');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['route' => ['name' => 'show']])
            ->add('user', null, ['route' => ['name' => 'show']])
            ->add(
                'person.person',
                FieldDescriptionInterface::TYPE_MANY_TO_ONE,
                ['route' => ['name' => 'edit'], 'label' => 'Person']
            )
            ->add(
                'person',
                FieldDescriptionInterface::TYPE_MANY_TO_ONE,
                ['route' => ['name' => 'edit'], 'label' => 'Locale']
            )
            ->add('error')
            ->add('isHandled', 'boolean')
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'handle' => [
                        'template' => 'sonata/whoiam/error/handle.html.twig',
                    ],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'id';
    }
}
