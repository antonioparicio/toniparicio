<?php

namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Show\ShowMapper;

class PersonClueShareAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('player');
        $datagridMapper->add('person');
        $datagridMapper->add('type');
        $datagridMapper->add('clue');
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('id')
            ->add('player')
            ->add('person')
            ->add('type', FieldDescriptionInterface::TYPE_STRING)
            ->add('clue', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime());
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('player')
            ->add('person')
            ->add('type', FieldDescriptionInterface::TYPE_STRING)
            ->add('clue', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('createdAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
        ;
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'id';
    }
}
