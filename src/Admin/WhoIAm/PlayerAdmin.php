<?php
// src/Admin/AuthorAdmin.php
namespace App\Admin\WhoIAm;

use App\Entity\Image;
use App\Entity\WhoIAm\Category;
use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLocale;
use App\Form\Type\PersonImageType;
use App\Util\DateTime\AdminDateTimeZone;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class PersonAdmin
 * @package App\Admin\WhoIAm
 */
class PlayerAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    protected function configureShowFields(ShowMapper $show){
        $show
            ->add('id', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('country', FieldDescriptionInterface::TYPE_STRING)
            ->add('locale', FieldDescriptionInterface::TYPE_STRING)
            ->add('email', FieldDescriptionInterface::TYPE_STRING)
            ->add('display', FieldDescriptionInterface::TYPE_STRING)
            ->add('provider', FieldDescriptionInterface::TYPE_STRING)
            ->add('birth_date', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('canComment', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('difficulty', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('trophies', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('season', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('likes', FieldDescriptionInterface::TYPE_ONE_TO_MANY)
            ->add('comments', FieldDescriptionInterface::TYPE_ONE_TO_MANY)
            ->add('games', FieldDescriptionInterface::TYPE_ONE_TO_MANY)
            ->add('updatedAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
        ;
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component
     *
     * @param  FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('display', TextType::class)
            ->add('difficulty', IntegerType::class)
            ->add('comments', CheckboxType::class, [
                'required' => false,
            ]);

    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models
     *
     * @param  DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('locale');
        $datagridMapper->add('email');
        $datagridMapper->add('display');
        $datagridMapper->add('provider');
        $datagridMapper->add('difficulty');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param  ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', FieldDescriptionInterface::TYPE_INTEGER, [
                'route' => ['name' => 'show'],
            ])
            ->add('locale', FieldDescriptionInterface::TYPE_STRING)
            ->add('email', FieldDescriptionInterface::TYPE_STRING, ['header_style' => 'width: 15%;'])
            ->add('display', FieldDescriptionInterface::TYPE_STRING)
            ->add('provider', FieldDescriptionInterface::TYPE_STRING)
            ->add('difficulty', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('trophies', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('season', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('updatedAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }

    /**
     * @param array $sortValues
     */
    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'id';
    }
}