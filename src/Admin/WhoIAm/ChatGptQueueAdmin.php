<?php

// src/Admin/AuthorAdmin.php

namespace App\Admin\WhoIAm;

use App\Util\DateTime\AdminDateTimeZone;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class ChatGptQueueAdmin.
 */
class ChatGptQueueAdmin extends AbstractAdmin
{
    use AdminDateTimeZone;

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('id', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('person', FieldDescriptionInterface::TYPE_MANY_TO_ONE, [
                'route' => ['name' => 'edit'],
            ])
            ->add('personLocale.locale', FieldDescriptionInterface::TYPE_STRING, [
                'label' => 'Locale',
            ])
            ->add('personLocale', FieldDescriptionInterface::TYPE_MANY_TO_ONE, [
                'route' => ['name' => 'edit'],
            ])
            ->add('type', FieldDescriptionInterface::TYPE_STRING)
            ->add('updatedAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('is_handled', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('prompt', FieldDescriptionInterface::TYPE_ONE_TO_ONE)
            ->add('prompt.response', 'json')
            ->add('prompt.input_tokens', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('prompt.output_tokens', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('prompt.cost', FieldDescriptionInterface::TYPE_CURRENCY)
            ->add('prompt.created_at', FieldDescriptionInterface::TYPE_DATETIME)
            ->add('prompt.is_success', FieldDescriptionInterface::TYPE_BOOLEAN)
        ;

        parent::configureShowFields($show);
    }

    /**
     * These lines configure which fields are displayed on the edit and create
     * actions. The FormMapper behaves similar to the FormBuilder of the Symfony
     * Form component.
     *
     * @param FormMapper $formMapper [description]
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

    }

    /**
     * This method configures the filters, used to filter and sort the list of
     * models.
     *
     * @param DatagridMapper $datagridMapper [description]
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('person');
        $datagridMapper->add('personLocale');
        $datagridMapper->add('type');
        $datagridMapper->add('isHandled');
    }

    /**
     * Here you specify which fields are shown when all models are listed (the
     * addIdentifier() method means that this field will link to the show/edit
     * page of this particular model).
     *
     * @param ListMapper $listMapper [description]
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', FieldDescriptionInterface::TYPE_INTEGER)
            ->add('person', FieldDescriptionInterface::TYPE_MANY_TO_ONE, [
                'route' => ['name' => 'edit'], 'header_style' => 'width: 15%;',
            ])
            ->add('personLocale.locale', FieldDescriptionInterface::TYPE_STRING, [
                'label' => 'Locale',
            ])
            ->add('personLocale', FieldDescriptionInterface::TYPE_MANY_TO_ONE, [
                'route' => ['name' => 'edit'], 'header_style' => 'width: 15%;',
            ])
            ->add('prompt', FieldDescriptionInterface::TYPE_ONE_TO_ONE)
            ->add('type', FieldDescriptionInterface::TYPE_STRING)
            ->add('updatedAt', FieldDescriptionInterface::TYPE_DATETIME, $this->getAdminDateTime())
            ->add('isHandled', FieldDescriptionInterface::TYPE_BOOLEAN)
            ->add('_actions', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureDefaultSortValues(array &$sortValues): void
    {
        $sortValues['_page'] = 1;
        $sortValues['_sort_order'] = 'DESC';
        $sortValues['_sort_by'] = 'created_at';
    }
}
