<?php
// src/Controller/LegalController.php
namespace App\Controller;

use App\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LegalController
 * @package App\Controller
 */
class TestController extends Controller
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        $service = $this->get('image.resize');
        $service->queue();
        return $this->render('legal/terms.html.twig', []);
    }
}
