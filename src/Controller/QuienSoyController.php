<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class QuienSoyController
 * @package App\Controller
 */
class QuienSoyController extends Controller
{
    /**
     * @Route("/politica-privacidad", name="politica_privacidad")
     */
    public function privacy()
    {

        return $this->render('quiensoy/privacy.html.twig', [

        ]);
    }
}
