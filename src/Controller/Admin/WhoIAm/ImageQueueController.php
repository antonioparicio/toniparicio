<?php

namespace App\Controller\Admin\WhoIAm;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ImageQueueController
 */
class ImageQueueController extends \Sonata\AdminBundle\Controller\CRUDController
{
    /**
     * publish post and make available on front
     * @param  integer $id
     * @return RedirectResponse
     */
    public function processAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        $service = $this->get('image.resize');
        $service->queue($id);

        $this->addFlash('sonata_flash_success', 'Image processed');

        return new RedirectResponse($this->admin->generateUrl('admin_app_whoiam_person_wikipedia'));
    }
}