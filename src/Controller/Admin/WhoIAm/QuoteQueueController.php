<?php

namespace App\Controller\Admin\WhoIAm;

use App\Entity\WhoIAm\PersonQuote;
use App\Entity\WhoIAm\QuoteQueue;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ImageQueueController
 */
class QuoteQueueController extends \Sonata\AdminBundle\Controller\CRUDController
{
    /**
     * @param $id
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function acceptAction($id): RedirectResponse
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        /** @var QuoteQueue $object */
        $manager = $this->get('doctrine.orm.entity_manager');

        /** @var QuoteQueue $quote */
        if ($locale = $object->getPerson() && strlen($object->getQuote()) <= PersonQuote::QUOTE_LENGTH) {
            $quote = new PersonQuote();
            $quote->setQuote($object->getQuote());
            $quote->setPerson($object->getPerson());
            $locale->addQuote($quote);

            $manager->persist($quote);
            $manager->persist($locale);
            $manager->remove($object);
            $manager->flush();

            $this->addFlash('sonata_flash_success', 'Quote added');
        }

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }
}