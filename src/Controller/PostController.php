<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;

/**
 * Class PostController
 * @package App\Controller
 */
class PostController extends Controller
{
    /**
     * @Route("/posts", name="post")
     */
    public function index(Request $request)
    {
        $em    = $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Post::class)->getPaginationQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,                                 /* query NOT result */
            $request->query->getInt('page', 1),     /*page number*/
            2                                       /*limit per page*/
        );

        return $this->render('post/index.html.twig', [
            'controller_name' => 'PostController',
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/post/{slug}", name="post")
     */
    public function show($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $post = $em->getRepository(Post::class)->findOneBySlug($slug);

        return $this->render('post/show.html.twig', [
            'controller_name' => 'PostController',
            'post' => $post
        ]);
    }
}
