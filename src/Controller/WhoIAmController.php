<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class QuienSoyController
 * @package App\Controller
 * @Route("/quiensoy")
 */
class WhoIAmController extends Controller
{
    /**
     * @Route("/", name="whoiam_index")
     */
    public function index()
    {

        return $this->render('quiensoy/index.html.twig', [

        ]);
    }
}
