<?php

// src/Admin/CRUDController.php

namespace App\Controller;

use App\Entity\WhoIAm\ChatGptPrompt;
use App\Entity\WhoIAm\ChatGptQueue;
use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLocale;
use App\Util\ChatGpt\ChatGptService;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Exception;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class CRUDController extends Controller
{
    public function __construct(
        private readonly ChatGptService $chatGptService
    ) {
    }

    /**
     * publish post and make available on front.
     */
    public function publishAction($id): RedirectResponse
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        $object->setPublishedAt();

        // get doctrine manager
        $em = $this->getDoctrine()->getManager();
        // save entity
        $em->persist($object);
        $em->flush();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function wikipediaAction(): Response
    {
        return $this->renderWithExtraParams('sonata/wikipedia.html.twig', [
        ]);
    }

    /**
     * @throws Exception
     */
    public function wikipediaAddAction(Request $request): Response
    {
        $client = $this->get('wikipedia.client');
        $wikipedia = $client->processUrl(rawurldecode((string) $request->get('wikipedia')));
        if ($wikipedia) {
            $service = $this->get('person.tools');
            $person = $service->updateFromWikipediaPerson($wikipedia);
            if ($person instanceof Person && [] !== $person->getLocales()) {
                $this->chatGptService->queue($person->getLocales()->toArray());
                $flashBag = $this->getRequest()->getSession()->getFlashBag();
                $flashBag->add('success', sprintf('%s added to ChatGPT queue', $person->getRealName()));
                foreach ($person->getLocales() as $personLocale) {
                    $this->chatGptService->queue([$personLocale]);
                }
            }
        }

        return new RedirectResponse($this->admin->generateUrl('wikipedia'));
    }

    public function chatGptAction($id): Response
    {
        $person = $this->admin->getSubject();

        if (!$person) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        /* @var PersonLocale $person */
        $this->chatGptService->queue([$person], null, true);

        $flashBag = $this->getRequest()->getSession()->getFlashBag();
        $flashBag->add('success', sprintf('%s added to ChatGPT queue', $person->getName()));

        return new RedirectResponse($this->admin->generateUrl('edit', ['id' => $id]));
    }

    public function deathAction($id): Response
    {
        return $this->chatGpt($id, ChatGptService::TYPE_DEATH);
    }

    public function worksAction($id): Response
    {
        return $this->chatGpt($id, ChatGptService::TYPE_WORKS);
    }

    public function factsAction($id): Response
    {
        return $this->chatGpt($id, ChatGptService::TYPE_FACTS);
    }

    public function biographyAction($id): Response
    {
        return $this->chatGpt($id, ChatGptService::TYPE_BIOGRAPHY);
    }

    public function quotesAction($id): Response
    {
        return $this->chatGpt($id, ChatGptService::TYPE_QUOTES);
    }

    private function chatGpt($id, $action): Response
    {
        $person = $this->admin->getSubject();
        $flashBag = $this->getRequest()->getSession()->getFlashBag();

        if (!$person instanceof PersonLocale) {
            $flashBag->add('error', sprintf('PersonLocale not found with id %d', $id));
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        $this->chatGptService->queue([$person], $action, true);

        $flashBag->add(
            'success',
            sprintf('Processing %s to get %s clue from ChatGPT', $person->getName(), $action)
        );

        return new RedirectResponse($this->admin->generateUrl('edit', ['id' => $id]));
    }

    public function handleAction($id): Response
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        $object->setIsHandled(true);

        $em = $this->getDoctrine()->getManager();
        // save entity
        $em->persist($object);
        $em->flush();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function batchActionRetry(ProxyQueryInterface $query, Request $request): Response
    {
        foreach ($query->execute() as $item) {
            if ($item instanceof ChatGptPrompt) {
                $this->retry($item);
            }
        }
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function retryAction($id): Response
    {
        $chatGptPrompt = $this->admin->getSubject();

        if (!$chatGptPrompt instanceof ChatGptPrompt) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        $this->retry($chatGptPrompt);

        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    private function retry(ChatGptPrompt $chatGptPrompt): void
    {
        $em = $this->getDoctrine()->getManager();

        $queue = $chatGptPrompt->getQueue();
        if (null === $queue) {
            $queue = (new ChatGptQueue())
                ->setPerson($chatGptPrompt->getPerson())
                ->setPersonLocale($chatGptPrompt->getPersonLocale())
                ->setType($chatGptPrompt->getType());
        }
        $queue->setIsHandled(false);
        $em->persist($queue);
        $em->remove($chatGptPrompt);
    }

    public function fixAction($id): Response
    {
        $chatGptPrompt = $this->admin->getSubject();

        if (!$chatGptPrompt) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }
        $application = new Application($this->get('kernel'));
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'whoiam:person:chatgpt:retry_prompt',
            'id' => $chatGptPrompt->getId(),
        ]);

        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();
        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
