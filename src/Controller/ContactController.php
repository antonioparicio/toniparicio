<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ContactForm;

/**
 * Class ContactController
 * @package App\Controller
 */
class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index()
    {
        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
        ]);
    }

    /**
     * receive contact form
     *
     * @return Response
     */
    public function submit(Request $request, \Swift_Mailer $mailer)
    {
        // get doctrince manager
        $entityManager = $this->getDoctrine()->getManager();

        $contact = new ContactForm();
        $contact->setName($request->request->get('name'))
                ->setEmail($request->request->get('email'))
                ->setSubject($request->request->get('subject'))
                ->setContent($request->request->get('content'));

        // save contact form
        $entityManager->persist($contact);
        $entityManager->flush();
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__.'/../../.env');

        $message = (new \Swift_Message($contact->getSubject()))
            ->setFrom(trim(getenv('MAILER_SENDER')))//$contact->getEmail())
            ->setReplyTo($contact->getEmail())
            ->setTo(trim(getenv('MAILER_RECEIVER')))
            ->setBody($contact->getContent())
        ;

        $result = $mailer->send($message, $failures);

        return $this->redirectToRoute('contact_thanks');
    }

    /**
     * show thanks message on submit
     *
     * @return Response
     */
    public function thanks()
    {
        return $this->render('contact/thanks.html.twig', [
            'controller_name' => 'ContactController'
        ]);
    }
}
