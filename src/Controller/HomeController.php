<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends Controller
{
    /**
     * Route("/home", name="home")
     */
    public function index()
    {
      $em = $this->getDoctrine()->getManager();

      $posts    = $em->getRepository(Post::class)->getPostsHome();

        return $this->render('home/index.html.twig', [
            'posts' => $posts,
            'controller_name' => 'HomeController',
        ]);
    }
}
