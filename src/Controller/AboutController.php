<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\SkillCategory;
use App\Entity\Project;
use App\Entity\SocialNetwork;
use App\Entity\Course;

class AboutController extends Controller
{
    /**
     * @Route("/about", name="about")
     * @Route("/",      name="root")
     * @Route("/home",  name="home")
     */
    public function index()
    {
      $em = $this->getDoctrine()->getManager();

      $categories = $em->getRepository(SkillCategory::class)->findAllOrderByPosition();
      $projects   = $em->getRepository(Project::class)->findAllOrderByDate();
      $socials    = $em->getRepository(SocialNetwork::class)->findAllOrderByPosition();
      $courses    = $em->getRepository(Course::class)->findAllOrderByDate();

        return $this->render('about/index.html.twig', [
            'controller_name' => 'AboutController',
            'categories' => $categories,
            'projects' => $projects,
            'socials' => $socials,
            'courses' => $courses
        ]);
    }
}
