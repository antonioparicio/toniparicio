<?php
// src/Controller/LegalController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LegalController
 * @package App\Controller
 */
class LegalController extends Controller
{
    /**
     * @Route("/terminos-y-condiciones-de-uso", name="terms")
     */
    public function terms()
    {
        return $this->render('legal/terms.html.twig', []);
    }

    /**
     * @Route("/politica-de-privacidad", name="privacy")
     */
    public function privacy()
    {
        return $this->render('legal/privacy.html.twig', []);
    }

    /**
     * @Route("/politica-de-cookies", name="cookies")
     */
    public function cookies()
    {
        return $this->render('legal/cookies.html.twig', []);
    }
}
