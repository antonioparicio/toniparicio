<?php

namespace App\Controller\API\v1;

use App\Entity\WhoIAm\Player;
use App\Repository\WhoIAm\PlayerRepository;
use App\Util\Image\AvatarImage;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{version}/user")
 */
class UserController extends AbstractFOSRestController
{
    public static function getSubscribedServices(): array
    {
        $services = parent::getSubscribedServices();
        $services['user.image.avatar'] = AvatarImage::class;

        return $services;
    }

    /**
     * @Rest\Post("")
     *
     * @throws Exception
     */
    public function add(EntityManagerInterface $manager, Request $request): Response
    {
        $id = $request->get('id', 0);
        $uuid = $request->get('uuid');
        $locale = $request->get('locale');
        $email = $request->get('email');
        $display = $request->get('display');
        $provider = $request->get('provider');

        $player = null;
        $repo = $manager->getRepository(Player::class);
        if (!empty($email)) {
            // try to find player by email
            $player = $repo->findOneBy(['email' => $email, 'provider' => $provider]);
        }
        if (!$player && !empty($uuid)) {
            // try to find player by APP UUID
            $player = $repo->findOneBy(['uuid' => $uuid]);
        }
        if (!$player && $id) {
            // try to find player by database ID (anonymous player)
            $player = $repo->find($id);
        }

        if (!$player) {
            // if still no player then create one
            $player = new Player();
        }
        if ($email) {
            $player->setEmail($email);
        }

        // locale, display and UUID could change with new login/register
        $player
            ->setUuid($uuid)
            ->setDisplay($player->getDisplay() ?? $display)
            ->setLocale($player->getLocale() ?? $locale)
            ->setComments(true)
            ->setProvider($provider);

        $manager->persist($player);
        $manager->flush();

        if ($player->getId() !== $id) {
            // $id should be an anonymous player and we can remove safety
            // check if there is any game or any linked to $id player
        }

        if ($player) {
            $code = 200;
            $data = [
                'items' => 1,
                'data' => $player,
            ];
        } else {
            $data = [
                'error' => [
                    'code' => 101,
                    'message' => 'Error on create user',
                ],
            ];
            $code = 404;
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/update")
     *
     * @throws Exception
     */
    public function update(EntityManagerInterface $manager, Request $request): Response
    {
        $id = $request->get('user', 0);
        $username = $request->get('name');
        $birthDate = $request->get('birthDate');
        $country = $request->get('country');

        $repo = $manager->getRepository(Player::class);
        $player = $id ? $repo->find($id) : null;

        if (!$player) {
            $data = [
                'error' => [
                    'code' => 101,
                    'message' => 'Error on update user',
                ],
            ];
            $code = 404;
            $view = $this->view($data, $code);
            return $this->handleView($view);
        }

        $birthDate = $birthDate ? (new DateTime())->setTimestamp($birthDate) : null;

        $player
            ->setDisplay($username)
            ->setBirthDate($birthDate)
            ->setCountry($country);

        $manager->persist($player);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $player,
        ];

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/avatar")
     *
     * @throws \ImagickException
     */
    public function avatar(
        EntityManagerInterface $manager,
        Request $request,
        AvatarImage $service
    ): Response {
        $id = $request->get('user', 0);
        $avatar = $request->files->get('imageFile');

        $repo = $manager->getRepository(Player::class);
        $player = $id ? $repo->find($id) : null;

        if (!$player) {
            $data = [
                'error' => [
                    'code' => 101,
                    'message' => 'Error on update user',
                ],
            ];
            $code = 404;
            $view = $this->view($data, $code);

            return $this->handleView($view);
        }
        if (!$service->submit($player, $avatar)) {
            throw new \RuntimeException(sprintf('Error on image upload'));
        }

        $player->setAvatar(true);
        $manager->persist($player);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $player,
        ];

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/difficulty")
     */
    public function difficulty(
        EntityManagerInterface $manager,
        Request $request,
    ): Response {
        $id = $request->get('user', 0);
        $difficulty = $request->get('difficulty', 0);

        $repo = $manager->getRepository(Player::class);
        $player = $id ? $repo->find($id) : null;

        if (!$player) {
            $data = [
                'error' => [
                    'code' => 101,
                    'message' => 'Error on update user',
                ],
            ];
            $code = 404;
            $view = $this->view($data, $code);

            return $this->handleView($view);
        }

        $player->setDifficulty($difficulty);
        $manager->persist($player);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $player,
        ];

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/ranking")
     */
    public function ranking(Request $request, EntityManagerInterface $manager): Response
    {
        $page = (int) $request->get('page', 0);
        $playerId = (int) $request->get('player');

        /** @var PlayerRepository $repo */
        $repo = $manager->getRepository(Player::class);
        $player = $repo->find($playerId);
        if (!$player) {
            throw new \RuntimeException(sprintf('Player %d not found', $playerId));
        }
        /** @var Player $player */
        $date = new \DateTime('now');
        $timeline = clone $date;
        $timeline->modify('first day of next month');
        $timeline->setTime(0, 0, 0);
        $minutes = round(($timeline->getTimestamp() - $date->getTimestamp()) / 60);

        $code = 200;
        $data = [
            'items' => 1,
            'data' => [
                'table' => $repo->getRankingPage($player, $page),
                'countdown' => $minutes,
            ],
        ];

        if (0 === $page) {
            $position = $repo->getPlayerSeasonPosition($player);
            $data['data']['player'] = [
                'id' => $player->getId(),
                'display' => $player->getDisplay(),
                'trophies' => $player->getTrophies(),
                'season' => $player->getSeason(),
                'avatar' => $player->isAvatar(),
                'position' => $position,
            ];
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }
}
