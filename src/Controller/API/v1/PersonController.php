<?php

namespace App\Controller\API\v1;

use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonClueLike;
use App\Entity\WhoIAm\PersonComment;
use App\Entity\WhoIAm\PersonCommentLike;
use App\Entity\WhoIAm\PersonError;
use App\Entity\WhoIAm\PersonLike;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\PersonScore;
use App\Entity\WhoIAm\Player;
use App\Repository\WhoIAm\PersonRepository;
use App\Util\Email\Email;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{version}/person")
 */
class PersonController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/")
     */
    public function all(EntityManagerInterface $manager): Response
    {
        /** @var PersonRepository $repo */
        $repo = $manager->getRepository(Person::class);
        if ($data = $repo->findAll()) {
            $code = 200;
            $data = [
                'items' => count($data),
                'data' => $data,
            ];
        } else {
            $data = [
                'error' => [
                    'code' => 1000,
                    'message' => 'There is no persons in database',
                ],
            ];
            $code = 404;
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/{id}")
     *
     * @param $id
     */
    public function single(EntityManagerInterface $manager, $id): Response
    {
        $repo = $manager->getRepository(Person::class);
        if ($data = $repo->find($id)) {
            $code = 200;
            $data = [
                'items' => 1,
                'data' => $data,
            ];
        } else {
            $data = [
                'error' => [
                    'code' => 1000,
                    'message' => sprintf('Person with %d not exists', $id),
                ],
            ];
            $code = 404;
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @throws \Exception
     */
    public function query(EntityManagerInterface $manager, Request $request): Response
    {
        /** @var PersonRepository $repo */
        $repo = $manager->getRepository(Person::class);
        $data = [];
        if ($date = $request->get('updated_at')) {
            if ($date = (new \DateTime())->setTimestamp($date)) {
                $persons = $repo->getUpdates($date);
                $data = [
                    'items' => count($persons),
                    'data' => $persons,
                ];
                $code = 200;
            } else {
                $data = [
                    'error' => [
                        'code' => 1000,
                        'message' => 'Date format is not valid',
                    ],
                ];
                $code = 400;
            }
        } else {
            $data = [
                'error' => [
                    'code' => 1001,
                    'message' => 'There is no persons in database',
                ],
            ];
            $code = 404;
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/score")
     */
    public function pushScore(Request $request, EntityManagerInterface $manager): Response
    {
        $repo = $manager->getRepository(Person::class);
        $person = $repo->find($request->get('person', 0));
        if (!$person) {
            $data = [
                'error' => [
                    'code' => 1100,
                    'message' => sprintf('Person %d not found', $request->get('person', 0)),
                ],
            ];
            $code = Response::HTTP_NOT_ACCEPTABLE;
        } else {
            $repo = $manager->getRepository(PersonScore::class);
            $score = new PersonScore();
            $score->setUser($request->get('user', 0));
            $score->setPerson($person);
            $score->setScore((float) $request->get('score', 0));
            $manager->persist($score);
            $manager->flush();

            if ($score->getId()) {
                $code = Response::HTTP_OK;
                $data = [
                    'items' => 1,
                    'data' => [
                        'score' => $avg = $repo->getAverage($person),
                    ],
                ];
            } else {
                $data = [
                    'error' => [
                        'code' => 1100,
                        'message' => sprintf('Score cannot be setted'),
                    ],
                ];
                $code = Response::HTTP_NOT_ACCEPTABLE;
            }
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * Push like/dislike for a person.
     *
     * @Rest\Post("/like")
     */
    public function pushLike(Request $request, EntityManagerInterface $manager): Response
    {
        $repo = $manager->getRepository(Person::class);
        $person = $repo->find($request->get('person', 0));
        $repo = $manager->getRepository(Player::class);
        $player = $repo->find($request->get('player', 0));
        if (!$person) {
            $data = [
                'error' => [
                    'code' => 1100,
                    'message' => sprintf('Person %d not found', $request->get('person', 0)),
                ],
            ];
            $code = Response::HTTP_NOT_ACCEPTABLE;
        } else {
            $repo = $manager->getRepository(PersonLike::class);
            /** @var PersonLike $like */
            $like = $repo->findOneBy(['person' => $person->getId(), 'player' => $player->getId()]);
            $value = (bool) $request->get('like', true);
            if (!$like) {
                $like = new PersonLike();
                $like->setPlayer($player);
                $like->setPerson($person);
            } else {
                // remove previous like/dislike from person consolidated values
                $person->removeLike($like->getType());
            }
            // add update like/dislike value
            $like->setType($value);
            // add like/dislike to person consolidated
            $person->addLike($value);

            $manager->persist($like);
            $manager->persist($person);
            $manager->flush();

            if ($like->getId()) {
                $code = Response::HTTP_OK;
                $data = [
                    'items' => 1,
                    'data' => [
                        'likes' => $person->getLikes(),
                        'dislikes' => $person->getDislikes(),
                    ],
                ];
            } else {
                $data = [
                    'error' => [
                        'code' => 1100,
                        'message' => sprintf('Like cannot be added'),
                    ],
                ];
                $code = Response::HTTP_NOT_ACCEPTABLE;
            }
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * Push comment for a person.
     *
     * @Rest\Post("/comment")
     */
    public function pushComment(Request $request, EntityManagerInterface $manager, Email $email): Response
    {
        $repo = $manager->getRepository(PersonLocale::class);
        /** @var PersonLocale $person */
        $person = $repo->find($request->get('person', 0));
        $repo = $manager->getRepository(Player::class);
        /** @var Player $player */
        $player = $repo->find($request->get('player', 0));
        if (!$person) {
            $data = [
                'error' => [
                    'code' => 1100,
                    'message' => sprintf('Person %d not found', $request->get('person', 0)),
                ],
            ];
            $code = Response::HTTP_NOT_ACCEPTABLE;
        } else {
            $personComment = new PersonComment();
            $personComment->setPlayer($player);
            $personComment->setPersonLocale($person);
            $personComment->setComment($request->get('comment'));
            $manager->persist($personComment);
            $manager->flush();

            if ($personComment->getId()) {
                $repo = $manager->getRepository(PersonComment::class);
                $comments = $repo->getCommentsCount($personComment->getPersonLocale());

                $code = Response::HTTP_OK;
                $data = [
                    'items' => 1,
                    'data' => [
                        'id' => $personComment->getId(),
                        'comments' => $comments,
                    ],
                ];

                $response = $email->setTemplate('comment')
                    ->setSubject($person->getName())
                    ->setBody([
                        'person' => $person,
                        'comment' => $personComment->getComment(),
                    ])
                    ->setUser($player)
                    ->send();
                $data = array_merge($data, $response);
            } else {
                $data = [
                    'error' => [
                        'code' => 1100,
                        'message' => 'Comment cannot be added',
                    ],
                ];
                $code = Response::HTTP_NOT_ACCEPTABLE;
            }
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * Push like/dislike for a comment in person.
     *
     * @Rest\Post("/comment/like")
     */
    public function pushCommentLike(Request $request, EntityManagerInterface $manager): Response
    {
        $repo = $manager->getRepository(PersonComment::class);
        $comment = $repo->find($request->get('comment', 0));
        $repo = $manager->getRepository(Player::class);
        $player = $repo->find($request->get('player', 0));
        if (!$comment || !$player) {
            $data = [
                'error' => [
                    'code' => 1100,
                    'message' => sprintf('Request error'),
                ],
            ];
            $code = Response::HTTP_NOT_ACCEPTABLE;
        } else {
            $repo = $manager->getRepository(PersonCommentLike::class);
            $like = $repo->findOneBy(['player' => $player->getId(), 'comment' => $comment->getId()]);
            if (!$like) {
                $like = new PersonCommentLike();
                $like->setPlayer($player);
                $like->setComment($comment);
            } else {
                $comment->removeLike($like->getType());
            }
            $type = (bool) $request->get('like', 1);
            $like->setType($type);
            $comment->addLike($type);
            $manager->persist($like);
            $manager->persist($comment);
            $manager->flush();

            if ($like->getId()) {
                $code = Response::HTTP_OK;
                $data = [
                    'items' => 1,
                    'data' => [
                        'likes' => $comment->getLikes(),
                        'dislikes' => $comment->getDislikes(),
                    ],
                ];
            } else {
                $data = [
                    'error' => [
                        'code' => 1100,
                        'message' => sprintf('Comment cannot be added'),
                    ],
                ];
                $code = Response::HTTP_NOT_ACCEPTABLE;
            }
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/error")
     */
    public function pushError(Request $request, EntityManagerInterface $manager, Email $email): Response
    {
        $repo = $manager->getRepository(PersonLocale::class);
        $locale = $repo->find($request->get('person', 0));
        $repo = $manager->getRepository(Player::class);
        /** @var Player $player */
        $player = $repo->find($request->get('user', 0));

        if ($player && $player->isTester()) {
            $data = [
                'items' => 0,
                'data' => [],
            ];
            $code = Response::HTTP_OK;
        } elseif (!$locale) {
            $data = [
                'error' => [
                    'code' => 1100,
                    'message' => sprintf('PersonLocale %d not found', $request->get('person', 0)),
                ],
            ];
            $code = Response::HTTP_NOT_ACCEPTABLE;
        } else {
            /** @var PersonLocale $locale */
            $repo = $manager->getRepository(PersonError::class);
            $error = new PersonError();
            $error->setUser($player);
            $error->setPerson($locale);
            $error->setError($request->get('error'));
            $manager->persist($error);
            $manager->flush();

            if ($error->getId()) {
                $code = Response::HTTP_OK;
                $data = [
                    'items' => 1,
                    'data' => [],
                ];
                $response = $email->setTemplate('error')
                    ->setSubject($locale->getName())
                    ->setBody([
                        'personLocale' => $locale,
                        'error' => $error->getError(),
                    ])
                    ->setUser($player)
                    ->send();
                $data = array_merge($data, $response);
            } else {
                $data = [
                    'error' => [
                        'code' => 1100,
                        'message' => 'Error cannot be handled',
                    ],
                ];
                $code = Response::HTTP_NOT_ACCEPTABLE;
            }
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }
}
