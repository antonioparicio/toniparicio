<?php

namespace App\Controller\API\v1;

use App\Entity\WhoIAm\Game;
use App\Entity\WhoIAm\GameRound;
use App\Entity\WhoIAm\GameRoundClue;
use App\Entity\WhoIAm\PersonComment;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\Player;
use App\Repository\WhoIAm\GameRepository;
use App\Repository\WhoIAm\PersonRepository;
use App\Repository\WhoIAm\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{version}/game")
 */
class GameController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("")
     */
    public function addGame(Request $request, EntityManagerInterface $manager): Response
    {
        /** @var PersonRepository $repo */
        $repo = $manager->getRepository(Player::class);
        $userId = $request->get('user');
        /** @var Player $player */
        $player = $userId ? $repo->find($userId) : null;
        if (!$player) {
            throw new \RuntimeException(sprintf('Player %d not found', $userId));
        }
        $game = new Game();
        $game->setPlayer($player);
        $game->setLocale($player->getLocale());
        $game->setIsSuccess(false);
        $manager->persist($game);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $game,
        ];

        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['api']);

        return $this->handleView($view);
    }

    /**
     * @Rest\Put("/round")
     */
    public function addRound(Request $request, EntityManagerInterface $manager): Response
    {
        $userId = $request->get('user', null);
        $personId = $request->get('person', 0);
        $gameId = $request->get('game', 0);

        $repo = $manager->getRepository(Player::class);
        /** @var Player $player */
        $player = $repo->find($userId);
        if (!$player) {
            throw new \RuntimeException('Player not found');
        }

        $repo = $manager->getRepository(PersonLocale::class);
        /** @var PersonLocale $person */
        $person = $personId ? $repo->find($personId) : null;
        if (!$person) {
            throw new \RuntimeException(sprintf('PersonLocale %d not found', $personId));
        }

        $repo = $manager->getRepository(Game::class);
        /** @var Game $game */
        $game = $gameId ? $repo->find($gameId) : null;
        if (!$game) {
            throw new \RuntimeException(sprintf('Game %d not found', $gameId));
        }

        $round = new GameRound();
        $round->setPlayer($player);
        $round->setPersonLocale($person);
        $round->setGame($game);
        $round->setIsSuccess(false);
        $manager->persist($round);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => [
                'id' => $round->getId(),
            ],
        ];

        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['api']);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/round")
     */
    public function updateRound(Request $request, EntityManagerInterface $manager): Response
    {
        $repo = $manager->getRepository(GameRound::class);

        $roundId = $request->get('round');
        $success = (bool) $request->get('success', false);
        $score = $request->get('score');
        $trophies = (int) $request->get('trophies');

        $round = $repo->find($roundId);
        if (!$round) {
            throw new \RuntimeException(sprintf('Round %d not found', $roundId));
        }
        $round->setScore($score);
        $round->setIsSuccess($success);
        $round->setTrophies($trophies);
        // check if this is the final round and game is a success
        if (10 === count($round->getGame()->getRounds())) {
            $successes = array_filter($round->getGame()->getRounds(), static function (GameRound $round) {
                return $round->isSuccess();
            });
            if (count($successes) >= 8) {
                $round->getGame()->setIsSuccess(true);
                $manager->persist($round->getGame());
            }
        }
        $manager->persist($round);
        $manager->flush();

        $avg = $repo->getPersonAvg($round->getPerson());
        $repo = $manager->getRepository(PersonComment::class);
        $count = $repo->getCommentsCount($round->getPersonLocale());
        $comments = $count ? $repo->getComments($round->getPersonLocale()) : [];

        $code = 200;
        $data = [
            'items' => 1,
            'data' => [
                'avg' => $avg,
                'likes' => $round->getPerson()->getLikes(),
                'dislikes' => $round->getPerson()->getDislikes(),
                'count' => $count,
                'comments' => $comments,
            ],
        ];

        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['api', 'comments', 'player']);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/round/clue")
     */
    public function addRoundClue(Request $request, EntityManagerInterface $manager): Response
    {
        /** @var GameRound $round */
        $roundId = (int) $request->get('round', 0);
        $repo = $manager->getRepository(GameRound::class);
        $round = $repo->find($roundId);
        if (!$round) {
            throw new \RuntimeException(sprintf('Round %d not found', $roundId));
        }

        $clue = new GameRoundClue();
        $clue->setPlayer($round->getPlayer());
        $clue->setPerson($round->getPerson());
        $clue->setGame($round->getGame());
        $clue->setRound($round);
        $clue->setType($request->get('type'));
        $clue->setIdentifier($request->get('clue'));
        $clue->setFree($request->get('free'));
        $manager->persist($clue);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $clue->getId(),
        ];

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/resume")
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getGameResume(Request $request, EntityManagerInterface $manager): Response
    {
        /** @var GameRepository $gameRepo */
        $gameRepo = $manager->getRepository(Game::class);
        /** @var PlayerRepository $playerRepo */
        $playerRepo = $manager->getRepository(Player::class);
        $gameId = $request->get('game');

        /** @var Game $game */
        $game = $gameRepo->find($gameId);
        if (!$game || !($player = $game->getPlayer())) {
            throw new \RuntimeException('Game not found');
        }

        $date = new \DateTime('now');
        $timeline = clone $date;
        $timeline->modify('first day of next month');
        $timeline->setTime(0, 0, 0);
        $minutes = round(($timeline->getTimestamp() - $date->getTimestamp()) / 60);

        $position = $playerRepo->getPlayerSeasonPosition($player);
        $before = $playerRepo->getScoreSeasonPosition($player->getSeason() - $game->getTrophies(), $player->getLocale());
        $positions = $before - $position;
        $code = 200;
        $data = [
            'items' => 1,
            'data' => [
                'today' => $gameRepo->getTodayPosition($game),
                'table' => $playerRepo->getPlayerSeasonTable($player, $position),
                'podium' => $playerRepo->getPlayerSeasonPodium($player->getLocale()),
                'position' => $position,
                'positions' => $positions,
                'season' => $player->getSeason(),
                'total' => $player->getTrophies(),
                'countdown' => $minutes,
            ],
        ];

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }
}
