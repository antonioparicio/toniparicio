<?php

namespace App\Controller\API\v1;

use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonClueShare;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\Player;
use App\Repository\WhoIAm\PersonLocaleRepository;
use App\Repository\WhoIAm\PersonRepository;
use App\Repository\WhoIAm\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{version}/locale/person")
 */
class PersonLocaleController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/{locale}")
     */
    public function all(EntityManagerInterface $manager, string $locale): Response
    {
        /** @var PersonRepository $repo */
        $repo = $manager->getRepository(PersonLocale::class);
        if ($data = $repo->findBy(['locale' => $locale])) {
            $code = 200;
            $data = [
                'items' => count($data),
                'data' => $data,
            ];
        } else {
            $data = [
                'error' => [
                    'code' => 1000,
                    'message' => 'There is no persons in database',
                ],
            ];
            $code = 404;
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/{locale}/{id}", requirements={"id"="\d+"})
     *
     * @param $id
     */
    public function single(EntityManagerInterface $manager, $id): Response
    {
        $repo = $manager->getRepository(PersonLocale::class);
        if ($data = $repo->find($id)) {
            $code = 200;
            $data = [
                'items' => 1,
                'data' => $data,
            ];
        } else {
            $data = [
                'error' => [
                    'code' => 1000,
                    'message' => sprintf('Person with %d not exists', $id),
                ],
            ];
            $code = 404;
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/updates")
     *
     * @throws \Exception
     */
    public function query(EntityManagerInterface $manager, Request $request): Response
    {
        /** @var PersonLocaleRepository $repo */
        $repo = $manager->getRepository(PersonLocale::class);
        $locale = $request->get('locale');
        $data = [];
        if ($date = $request->get('updated_at', 1_577_836_800)) {
            if ($date = (new \DateTime())->setTimestamp($date)) {
                // get only attributes with annotation group "api" :: see view->setGroups()
                // to avoid circular and extra data
                $persons = $repo->getUpdates($locale, $date);
                $data = [
                    'items' => count($persons),
                    'data' => $persons,
                ];
                $code = 200;
            } else {
                $data = [
                    'error' => [
                        'code' => 1000,
                        'message' => 'Date format is not valid',
                    ],
                ];
                $code = 400;
            }
        } else {
            $data = [
                'error' => [
                    'code' => 1001,
                    'message' => 'There is no persons in database',
                ],
            ];
            $code = 404;
        }
        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['api']);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/clue/share")
     */
    public function pushClueShare(Request $request, EntityManagerInterface $manager): Response
    {
        /** @var PlayerRepository $repo */
        $repo = $manager->getRepository(Player::class);
        $id = (int) $request->get('player');
        /** @var Player $player */
        $player = $id ? $repo->find($id) : null;

        if (!$player) {
            throw new \RuntimeException(sprintf('User %d not found', $id));
        }

        $repo = $manager->getRepository(PersonLocale::class);
        $id = (int) $request->get('person');
        /** @var PersonLocale $person */
        $person = $id ? $repo->find($id) : null;

        if (!$person && $id > 0) {
            throw new \RuntimeException(sprintf('PersonLocale %d not found', $id));
        }

        $clueId = (int) $request->get('clue', 0);
        $type = $request->get('type');

        $share = new PersonClueShare();
        $share
            ->setPerson($person)
            ->setPlayer($player)
            ->setClue($clueId)
            ->setType($type);

        $manager->persist($share);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => [
                'id' => $share->getId(),
            ],
        ];

        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['player']);

        return $this->handleView($view);
    }
}
