<?php

namespace App\Controller\API\v1;

use App\Entity\WhoIAm\PersonClueLike;
use App\Entity\WhoIAm\PersonFact;
use App\Entity\WhoIAm\PersonLocale;
use App\Entity\WhoIAm\Player;
use App\Entity\WhoIAm\WorkshopAttribute;
use App\Entity\WhoIAm\WorkshopDeath;
use App\Entity\WhoIAm\WorkshopDifficulty;
use App\Entity\WhoIAm\WorkshopFact;
use App\Repository\WhoIAm\PlayerRepository;
use App\Util\Email\Email;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{version}/workshop")
 */
class WorkshopController extends AbstractFOSRestController
{
    private const UNAUTHORIZED = 5000;
    private const ALREADY_EXISTS = 5001;

    /**
     * @var Player
     */
    private $player;

    /**
     * @var PersonLocale
     */
    private $person;

    /**
     * init request by get player and person.
     */
    public function init(Request $request, EntityManagerInterface $manager)
    {
        /** @var PlayerRepository $repo */
        $repo = $manager->getRepository(Player::class);
        $id = (int) $request->get('player');
        /** @var Player $player */
        $player = $id ? $repo->find($id) : null;

        if (!$player) {
            throw new \RuntimeException(sprintf('User %d not found', $id));
        }

        $repo = $manager->getRepository(PersonLocale::class);
        $id = (int) $request->get('person');
        /** @var PersonLocale $person */
        $person = $id ? $repo->find($id) : null;

        if (!$person && $id > 0) {
            throw new \RuntimeException(sprintf('PersonLocale %d not found', $id));
        }

        $this->player = $player;
        $this->person = $person;
    }

    /**
     * @Rest\Post("/attribute")
     */
    public function setAttribute(Request $request, EntityManagerInterface $manager, Email $email): Response
    {
        $this->init($request, $manager);
        $attribute = (string) $request->get('attribute');
        $value = (string) $request->get('value');

        if (!$this->player->isWorkshop()) {
            $code = 200;
            $data = [
                'error' => [
                    'code' => self::UNAUTHORIZED,
                    'message' => 'This person is not authorized to submit workshop',
                ],
            ];
        } elseif ($this->player->isPublisher()) {
            $method = 'set'.ucfirst($attribute);
            if (method_exists($this->person, $method)) {
                $this->person->$method($value);
                $manager->persist($this->person);
            } elseif (method_exists($this->person->getPerson(), $method)) {
                $this->person->getPerson()->$method($value);
                $manager->persist($this->person->getPerson());
            }
            $manager->flush();
            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'published' => 1,
                ],
            ];

            $response = $email->setTemplate('workshop_attribute')
                ->setSubject($this->person->getName())
                ->setBody([
                    'person' => $this->person,
                    'attribute' => $attribute,
                    'value' => $value,
                    'published' => 'true',
                ])
                ->setUser($this->player)
                ->send();

            $data = array_merge($data, $response);
        } else {
            // queue fact in workshop for approve
            $fact = new WorkshopAttribute();
            $fact->setAuthor($this->player);
            $fact->setPerson($this->person);
            $fact->setAttribute($attribute);
            $fact->setValue($value);
            $manager->persist($fact);
            $manager->flush();

            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'published' => 0,
                ],
            ];

            $response = $email->setTemplate('workshop_attribute')
                ->setSubject($this->person->getName())
                ->setBody([
                    'person' => $this->person,
                    'attribute' => $fact->getAttribute(),
                    'value' => $fact->getValue(),
                    'published' => 'false',
                ])
                ->setUser($this->player)
                ->send();

            $data = array_merge($data, $response);
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/fact")
     */
    public function addFact(Request $request, EntityManagerInterface $manager, Email $email): Response
    {
        $this->init($request, $manager);

        // check if this fact already exists first
        $criteria = ['person' => $this->person, 'fact' => (string) $request->get('fact')];
        $repo = $manager->getRepository(PersonFact::class);
        $item = $repo->findOneBy($criteria);
        if (!$item) {
            $repo = $manager->getRepository(WorkshopFact::class);
            $item = $repo->findOneBy($criteria);
        }
        if ($item) {
            $code = 200;
            $data = [
                'error' => [
                    'code' => self::ALREADY_EXISTS,
                    'message' => 'This fact already exists published or queued',
                ],
            ];
        } elseif (!$this->player->isWorkshop()) {
            $code = 200;
            $data = [
                'error' => [
                    'code' => self::UNAUTHORIZED,
                    'message' => 'This person is not authorized to submit workshop',
                ],
            ];
        } elseif ($this->player->isPublisher()) {
            // add fact to person facts
            $fact = new PersonFact();
            $fact->setAuthor($this->player);
            $fact->setPerson($this->person);
            $fact->setFact((string) $request->get('fact'));
            $manager->persist($fact);
            $manager->flush();

            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'id' => $fact->getId(),
                    'published' => true,
                ],
            ];
        } else {
            // queue fact in workshop for approve
            $fact = new WorkshopFact();
            $fact->setAuthor($this->player);
            $fact->setPerson($this->person);
            $fact->setFact((string) $request->get('fact'));
            $manager->persist($fact);
            $manager->flush();

            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'id' => $fact->getId(),
                    'published' => false,
                ],
            ];
        }

        // send a notification by email
        if (!isset($data['error'])) {
            $response = $email->setTemplate('workshop_fact')
                ->setSubject($this->person->getName())
                ->setBody([
                    'person' => $this->person,
                    'fact' => (string) $request->get('fact'),
                    'published' => (int) $data['data']['published'],
                ])
                ->setUser($this->player)
                ->send();
            $data = array_merge($data, $response);
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/facts/{person}")
     */
    public function getFactsQueue(Request $request, EntityManagerInterface $manager): Response
    {
        $repo = $manager->getRepository(WorkshopFact::class);
        $person = (int) $request->get('person');
        $facts = $repo->findBy(['person' => $person, 'deleted_at' => null]);

        $code = 200;
        $data = [
            'items' => count($facts),
            'data' => [
                'items' => array_map(static fn (WorkshopFact $item) => $item->toApi(), $facts),
            ],
        ];
        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['workshop', 'player']);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/death")
     */
    public function addDeath(Request $request, EntityManagerInterface $manager, Email $email): Response
    {
        $this->init($request, $manager);
        $candidate = (string) $request->get('death');
        if ($candidate === $this->person->getDeath()) {
            $code = 200;
            $data = [
                'error' => [
                    'code' => self::ALREADY_EXISTS,
                    'message' => 'This fact already exists published or queued',
                ],
            ];
        } elseif (!$this->player->isWorkshop()) {
            $code = 200;
            $data = [
                'error' => [
                    'code' => self::UNAUTHORIZED,
                    'message' => 'This person is not authorized to submit workshop',
                ],
            ];
        } elseif ($this->player->isPublisher()) {
            $this->person->setDeath($candidate);
            $manager->persist($this->person);
            $manager->flush();

            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'id' => 0,
                    'published' => true,
                ],
            ];
        } else {
            // queue fact in workshop for approve
            $death = new WorkshopDeath();
            $death->setAuthor($this->player);
            $death->setPerson($this->person);
            $death->setDeath($candidate);
            $manager->persist($death);
            $manager->flush();

            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'id' => $death->getId(),
                    'published' => false,
                ],
            ];
        }

        // send a notification by email
        if (!isset($data['error'])) {
            $response = $email->setTemplate('workshop_death')
                ->setSubject($this->person->getName())
                ->setBody([
                    'person' => $this->person,
                    'death' => (string) $request->get('death'),
                    'published' => $data['data']['published'] ?? false,
                ])
                ->setUser($this->player)
                ->send();
            $data = array_merge($data, $response);
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/death/{person}")
     */
    public function getDeathQueue(Request $request, EntityManagerInterface $manager): Response
    {
        $repo = $manager->getRepository(WorkshopDeath::class);
        $personId = (int) $request->get('person');
        $queue = $repo->findBy(['person' => $personId, 'deleted_at' => null]);

        $code = 200;
        $data = [
            'items' => count($queue),
            'data' => [
                'items' => array_map(static fn (WorkshopDeath $item) => $item->toApi(), $queue),
            ],
        ];
        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['workshop', 'player']);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/difficulty")
     */
    public function addDifficulty(Request $request, EntityManagerInterface $manager, Email $email): Response
    {
        $this->init($request, $manager);
        $candidate = (int) $request->get('difficulty');
        if ($candidate === $this->person->getDifficulty()) {
            $code = 200;
            $data = [
                'error' => [
                    'code' => self::ALREADY_EXISTS,
                    'message' => 'This difficulty is already defined',
                ],
            ];
        } elseif (!$this->player->isWorkshop()) {
            $code = 200;
            $data = [
                'error' => [
                    'code' => self::UNAUTHORIZED,
                    'message' => 'This person is not authorized to submit workshop',
                ],
            ];
        } elseif ($this->player->isPublisher()) {
            $this->person->setDifficulty($candidate);
            $manager->persist($this->person);
            $manager->flush();

            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'id' => 0,
                    'published' => true,
                ],
            ];
        } else {
            // queue fact in workshop for approve
            $death = new WorkshopDifficulty();
            $death->setAuthor($this->player);
            $death->setPerson($this->person);
            $death->setDifficulty($candidate);
            $manager->persist($death);
            $manager->flush();

            $code = 200;
            $data = [
                'items' => 1,
                'data' => [
                    'id' => $death->getId(),
                    'published' => false,
                ],
            ];
        }

        // send a notification by email
        if (!isset($data['error'])) {
            $response = $email->setTemplate('workshop_difficulty')
                ->setSubject($this->person->getName())
                ->setBody([
                    'person' => $this->person,
                    'difficulty' => (string) $request->get('difficulty'),
                    'published' => $data['data']['published'] ?? false,
                ])
                ->setUser($this->player)
                ->send();
            $data = array_merge($data, $response);
        }

        $view = $this->view($data, $code);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/like")
     */
    public function pushLike(Request $request, EntityManagerInterface $manager): Response
    {
        $this->init($request, $manager);

        $clueId = (int) $request->get('clue', 0);
        $workshop = (bool) $request->get('workshop');
        $type = $request->get('type');
        $repo = $manager->getRepository(PersonClueLike::class);
        $like = $repo->findOneBy([
            'player' => $this->player,
            'person' => $this->person,
            'type' => $type,
            'workshop' => $workshop,
            'clue' => $clueId,
        ]);
        if (!$like) {
            $like = new PersonClueLike();
            $like->setPerson($this->person)
                ->setPlayer($this->player)
                ->setClue($clueId)
                ->setWorkshop($workshop)
                ->setType($type);
        }
        $like->setLike((int) $request->get('like'));
        $manager->persist($like);
        $manager->flush();
        $resume = $repo->getClueResume($this->person, $type, $workshop, $clueId);
        $resume = $like->handlePublisher($resume);
        // publish / remove

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $resume,
        ];

        $view = $this->view($data, $code);
        $view->getContext()->setGroups(['workshop', 'player']);

        return $this->handleView($view);
    }
}
