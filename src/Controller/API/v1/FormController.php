<?php

namespace App\Controller\API\v1;

use App\Entity\WhoIAm\FormContact;
use App\Entity\WhoIAm\Game;
use App\Entity\WhoIAm\GameRound;
use App\Entity\WhoIAm\GameRoundClue;
use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonWorkshop;
use App\Entity\WhoIAm\Player;
use App\Repository\WhoIAm\PersonRepository;
use App\Repository\WhoIAm\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{version}/form")
 */
class FormController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/contact")
     * @return Response
     */
    public function contact(Request $request, EntityManagerInterface $manager, \Swift_Mailer $mailer): Response
    {
        /** @var PlayerRepository $repo */
        $repo = $manager->getRepository(Player::class);
        $userId = $request->get('user');
        $user = $userId ? $repo->find($userId) : null;
        /** @var Player $user */
        $form = new FormContact();
        $form->setUser($user);
        $form->setLocale($request->get('locale'));
        $form->setSubject($request->get('subject'));
        $form->setBody($request->get('body'));
        $manager->persist($form);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $form
        ];

        try {
            $env = new Dotenv();
            $env->load(__DIR__.'/../../../../.env');
            // send contact form also to mail
            $receiver = trim(getenv('MAILER_WHOIAM_RECEIVER'));
            $email = $user && $user->getEmail() ? $user->getEmail() : trim(getenv('MAILER_SENDER'));
            $name = $user && $user->getDisplay() ? $user->getDisplay() : null;
            $logger = new \Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
            $message = (new \Swift_Message($form->getSubject()))
                ->setFrom($email, $name)
                ->setReplyTo($email, $name)
                ->setTo($receiver)
                ->setBody($form->getBody())
            ;

            if (!$mailer->send($message, $failures)) {
                $dump = $logger->dump();
            }
        } catch (\Exception) {

        }

        $view = $this->view($data, $code);
        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/workshop")
     * @return Response
     */
    public function workshop(Request $request, EntityManagerInterface $manager, \Swift_Mailer $mailer): Response
    {
        /** @var PlayerRepository $repo */
        $repo = $manager->getRepository(Player::class);
        $userId = $request->get('user');
        $user = $userId ? $repo->find($userId) : null;
        /** @var Player $user */
        $workshop = new PersonWorkshop();
        $workshop->setUser($user);
        $workshop->setLocale($request->get('locale'));
        $workshop->setName($request->get('name'));
        $workshop->setReason($request->get('reason'));
        $manager->persist($workshop);
        $manager->flush();

        $code = 200;
        $data = [
            'items' => 1,
            'data' => $workshop
        ];

        try {
            $env = new Dotenv();
            $env->load(__DIR__.'/../../../../.env');
            // send contact form also to mail
            $receiver = trim(getenv('MAILER_WHOIAM_RECEIVER'));
            $subject = '[Workshop] ' . $workshop->getName();
            $email = $user && $user->getEmail() ? $user->getEmail() : trim(getenv('MAILER_SENDER'));
            $name = $user && $user->getDisplay() ? $user->getDisplay() : null;
            $logger = new \Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
            $message = (new \Swift_Message($subject))
                ->setFrom($email, $name)
                ->setReplyTo($email, $name)
                ->setTo($receiver)
                ->setBody($workshop->getReason())
            ;

            if (!$mailer->send($message, $failures)) {
                $dump = $logger->dump();
            }
        } catch (\Exception) {

        }

        $view = $this->view($data, $code);
        return $this->handleView($view);
    }
}