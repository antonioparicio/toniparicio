<?php


namespace App\Controller\API\v1;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/{version}")
 */
class IndexController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/")
     */
    public function index($apiVersion)
    {
        return [
            'service' => 'WhoIAm API',
            'version' => $apiVersion,
            'status' => 'ok'
            // database status
            // migration version
        ];
    }
}