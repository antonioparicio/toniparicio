<?php

namespace App\Form\Type;

use App\Entity\WhoIAm\PersonWork;
use App\Entity\WhoIAm\PersonWorkType as PersonWorkTypeModel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PersonFactType.
 */
class PersonWorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, static function (FormEvent $event) {
            if ($entity = $event->getData()) {
                $form = $event->getForm();
                if ($image = $entity->getImage()) {
                    $src = $image->getOriginal();
                    // add a 'help' option containing the preview's img tag
                    $fileFormOptions['help'] = '<img src="'.$src.'" class="admin-preview" height=128/>';
                    $fileFormOptions['help_html'] = true;
                    $form->add('preview', TextType::class, $fileFormOptions);
                }
            }
        });

        $builder
            ->add('type', EntityType::class, ['class' => PersonWorkTypeModel::class])
            ->add('title', TextType::class)
//            ->add('image', TextType::class, ['required' => false])
            ->add('preview', TextType::class, [
                'required' => false,
            ])
            ->add('url', TextType::class, ['required' => false])
            ->add('file', FileType::class, [
                'required' => false,
                'multiple' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PersonWork::class,
        ]);
    }
}
