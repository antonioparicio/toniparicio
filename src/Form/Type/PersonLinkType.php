<?php


namespace App\Form\Type;

use App\Entity\WhoIAm\PersonLink;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\WhoIAm\PersonLinkType as PersonLinkTypeModel;

/**
 * Class PersonFactType
 * @package App\Form\Type
 */
class PersonLinkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', EntityType::class, ['class' => PersonLinkTypeModel::class])
            ->add('url', TextType::class, ['required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PersonLink::class,
        ]);
    }
}