<?php

namespace App\Form\Type;

use App\Entity\WhoIAm\PersonImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PersonFactType.
 */
class PersonImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, static function (FormEvent $event) {
            if ($entity = $event->getData()) {
                $form = $event->getForm();
                /** @var PersonImage $entity */
                if ($image = $entity->getImage()) {
                    $src = $image->getOriginal();
                    // add a 'help' option containing the preview's img tag
                    $fileFormOptions['help'] = '<img src="'.$src.'" class="admin-preview" height=128/>';
                    $fileFormOptions['help_html'] = true;
                    $form->add('preview', TextType::class, $fileFormOptions);
                }
            }
        });

        $builder
            ->add('preview', TextType::class, [
                'required' => false,
            ])
            ->add('file', FileType::class, [
                'required' => false,
                'multiple' => false,
            ])
//            ->add('image', TextType::class)
//            ->add('image', EntityType::class, [
//                'class' => Image::class,
//                'multiple' => false,
//                'expanded' => true
//            ])
            ->add('portrait', CheckboxType::class, ['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PersonImage::class,
        ]);
    }
}
