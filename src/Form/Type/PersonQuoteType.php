<?php


namespace App\Form\Type;

use App\Entity\WhoIAm\PersonQuote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PersonFactType
 * @package App\Form\Type
 */
class PersonQuoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'quote',
                TextareaType::class,
                [
                    'attr' => [
                        'maxlength' => PersonQuote::QUOTE_LENGTH,
                        'rows' => 3
                    ]
                ]
            )
            ->add('footer', TextType::class, ['required' => false])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PersonQuote::class,
        ]);
    }
}