<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonCommand
 * @package App\Command
 */
class PersonCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:wikipedia';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Search for person in wikipedia')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addOption('locale', 'l', InputOption::VALUE_OPTIONAL, 'Language', 'es')
            ->setHelp('This command allows you to search in wikipedia a person for WhoIAm game');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // ... put here the code to run in your command

        $output->writeln('hello world');
        $name = $input->getArgument('name');
        $locale = $input->getOption('locale');
        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        $client = new \WikipediaClient();
//        $client->getPerson($name, $locale);


        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

//        return Command::SUCCESS;
    }
}
