<?php

namespace App\Command;

use App\Entity\WhoIAm\Player;
use App\Entity\WhoIAm\PlayerSeason;
use App\Entity\WhoIAm\Season;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonCommand
 * @package App\Command
 */
class ResetSeassonCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:season:reset';

    /**
     * ImageProcessCommand constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(private readonly EntityManagerInterface $manager)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Reset season')
            ->setHelp('Get all players with participation in actual season and save results and reset season to zero');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->manager->getRepository(Season::class);
        /** @var Season $season */
        $season = $repo->findOneBy([], ['created_at' => 'DESC']);
        $leagues = ['es'];

        $end = clone $season->getFinishedAt();
        $end->add(new \DateInterval('P1M'));

        $nextSeason = new Season();
        $nextSeason->setName($season->getName() + 1)
            ->setStartedAt($season->getFinishedAt())
            ->setFinishedAt($end);

        $this->manager->persist($nextSeason);
        
        foreach ($leagues as $league) {
            $repo = $this->manager->getRepository(Player::class);
            $players = $repo->getSeasonPlayers($league);

            $position = 0;
            foreach ($players as $player) {
                /** @var Player $player */
                $playerSeason = new PlayerSeason();
                $playerSeason->setPlayer($player);
                $playerSeason->setPosition(++$position);
                $this->manager->persist($playerSeason);
                
                $player->setSeason(0);
            }
        }
        $this->manager->flush();

        return true;
    }
}
