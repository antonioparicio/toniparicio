<?php

namespace App\Command;

use App\Entity\WhoIAm\ChatGptPrompt;
use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\ChatGptPromptRepository;
use App\Util\ChatGpt\ChatGptBiographyPrompt;
use App\Util\ChatGpt\ChatGptDeathPrompt;
use App\Util\ChatGpt\ChatGptFactPrompt;
use App\Util\ChatGpt\ChatGptQuotePrompt;
use App\Util\ChatGpt\ChatGptResponse;
use App\Util\ChatGpt\ChatGptService;
use App\Util\ChatGpt\ChatGptWorkPrompt;
use App\Util\Json\ExtractJsonFromString;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function _PHPStan_dfcaa3082\RingCentral\Psr7\str;

/**
 * Class PersonFactPromptCommand.
 */
class PersonChatGptRetryPromptCommand extends Command
{
    // the name of the command (the part after "bin/console")
    const SIMPLE_PROMPTS = [
        'death'
    ];
    protected static $defaultName = 'whoiam:person:chatgpt:retry_prompt';


    public function __construct(
        private readonly ChatGptPromptRepository $repository,
        private readonly EntityManagerInterface $manager,
        private readonly ChatGptFactPrompt $factPrompt,
        private readonly ChatGptQuotePrompt $quotePrompt,
        private readonly ChatGptWorkPrompt  $workPrompt,
        private readonly ChatGptDeathPrompt $deathPrompt,
        private readonly ChatGptBiographyPrompt $biographyPrompt,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Retry a unsuccessfull chatgpt_prompt with original response (without new request')
            ->addArgument('id', InputArgument::REQUIRED)
            ->setHelp('This command allows you to try process a chatgpt_prompt response again');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id');

        /** @var ChatGptPrompt $chatGptPrompt */
        $chatGptPrompt = $this->repository->find($id);

        if (!$chatGptPrompt) {
            $output->writeln(sprintf('ChatGptPrompt %d not found', $id));
            return 0;
        }
        if ($chatGptPrompt->isSuccess()) {
            $output->writeln(sprintf('ChatGptPrompt %d is already a successful prompt', $id));
            return 0;
        }

        return $this->process($chatGptPrompt, $output);
    }

    private function process(ChatGptPrompt $chatGptPrompt, OutputInterface $output): int
    {
        $response = $chatGptPrompt->getResponse();

        $output->writeln("\n### ORIGINAL RESPONSE ###\n");
        $output->writeln($response  . "\n\n");

        $json = ExtractJsonFromString::json($response);

        if (empty($json)) {
            $output->writeln('JSON response not found');
        }
        $output->writeln("\n### JSON RESPONSE ###\n");
        $output->writeln($json);

        try {
            if (in_array($chatGptPrompt->getType(), self::SIMPLE_PROMPTS) && !str_contains($response, '{')) {
                $chatGptResponse = ChatGptResponse::fromContent($response);
            } else {
                $arr = json_decode($json, true);
                $chatGptResponse = ChatGptResponse::json($json);
            }

            $output->writeln("\n### JSON DECODED ###\n");
            $output->writeln(json_encode($chatGptResponse->getOutput(), JSON_PRETTY_PRINT));

            $prompt = $this->getPrompt($chatGptPrompt, $chatGptPrompt->getPersonLocale());
            $prompt = $prompt->setResponse($chatGptResponse);
            $prompt->post();

            $chatGptPrompt->setIsSuccess(true);
            $chatGptPrompt->setInputTokens($arr['usage']['prompt_tokens'] ?? $chatGptPrompt->getInputTokens());
            $chatGptPrompt->setOutputTokens($arr['usage']['completion_tokens'] ?? $chatGptPrompt->getOutputTokens());
            $chatGptPrompt->setCost($chatGptPrompt->cost());
            $this->manager->persist($chatGptPrompt);
            $this->manager->flush();
        } catch (\Exception $exception) {
            $output->writeln('ERROR: ' . $exception);
            return 0;
        }

        return 0;
    }

    private function getPrompt(ChatGptPrompt $chatGptPrompt, PersonLocale $person): \App\Util\ChatGpt\ChatGptPrompt
    {
        return match ($chatGptPrompt->getType()) {
            ChatGptService::TYPE_FACTS => $this->factPrompt->setPerson($person),
            ChatGptService::TYPE_WORKS => $this->workPrompt->setPerson($person),
            ChatGptService::TYPE_QUOTES => $this->quotePrompt->setPerson($person),
            ChatGptService::TYPE_DEATH => $this->deathPrompt->setPerson($person),
            ChatGptService::TYPE_BIOGRAPHY => $this->biographyPrompt->setPerson($person),
        };
    }
}
