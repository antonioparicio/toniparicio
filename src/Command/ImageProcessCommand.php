<?php

namespace App\Command;

use App\Util\Image\ImageResize;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonCommand
 * @package App\Command
 */
class ImageProcessCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'image:process:queue';

    /**
     * ImageProcessCommand constructor.
     * @param ImageResize $service
     */
    public function __construct(private readonly ImageResize $service)
    {
        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Process images in queue')
            ->setHelp('Process images in queue');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->service->queue();

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        return 1;
    }
}
