<?php

namespace App\Command;

use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\PersonLocaleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonCommand
 * @package App\Command
 */
class PersonLocaleDifficultyCalculateCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:difficulty:calculate';

    /**
     * minimum games played to consider a change in difficulty
     */
    private const MINIMUM_GAMES = 5;

    /**
     * difficulty => % person_locale
     */
    private const RANGES = [
        1 => 2,
        2 => 5,
        3 => 10,
        4 => 18,
        5 => 30,
        6 => 45,
        7 => 60,
        8 => 70,
        9 => 80,
        10 => 100
    ];

    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly PersonLocaleRepository $personRepository
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Calculate difficulty using percentages')
            ->addArgument('locale', InputArgument::REQUIRED, 'Locales to use as source for get difficulty')
            ->addOption('update', 'u', InputOption::VALUE_OPTIONAL, "Update difficulties with new data", false)
            ->addOption('dry-run', null, InputOption::VALUE_OPTIONAL, "run without update DDBB", false)
            ->addOption(
                'minimum',
                'm',
                InputOption::VALUE_OPTIONAL,
                "minimum games to consider a change in difficulty",
                self::MINIMUM_GAMES
            )
            ->setHelp('This command allows you to recalculate difficulties');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $locale = $input->getArgument('locale');
        $dryRun = $input->getOption('dry-run') !== false;
        $update = $input->getOption('update') !== false;
        $minimum = (int)$input->getOption('minimum');


        $table = new Table($output);
        $table->setHeaders([
            'Status',
            'Person',
            'Difficulty',
            'Recommended',
            'Games',
            'Success',
            'AVG',
        ]);

        foreach ($this->getPersonLocales($output, $locale, $minimum) as $person) {
            $object = $person['person'] ?? null;
            $row = [
                $object?->getDifficulty() == $person['recommended'] ? '✔' : '✖',
                $object?->getName() ?? $person['person_locale_id'],
                $object?->getDifficulty() ?? 'N/A',
                $person['recommended'],
                $person['games'],
                $person['success'],
                round(100 * $person['avg']) . '%',
            ];
            $table->addRow($row);
            if ($update && $object) {
                /** @var PersonLocale $object */
                $object->setDifficulty($person['recommended']);
                $this->manager->persist($object);
            }
        }
        $table->render();

        if (!$dryRun && $update) {
            $this->manager->flush();
        }


        return 1;
    }

    private function getPersonLocales(OutputInterface $output, string $locale, int $minimum): array
    {
        $data = $this->personRepository->getPersonDifficulties($locale, $minimum);
        $persons = $this->personRepository->count(['locale' => $locale, 'isPlayable' => true, 'deleted_at' => null]);

        $count = 0;
        $total = count($data);

        $prev = 0;
        foreach (self::RANGES as $difficulty => $p) {
            $value = round($p * $persons / 100);
            $output->writeln(sprintf('Difficulty %d => %d%% (%d)', $difficulty, $p, $value - $prev));
            $prev = $value;
        }

        foreach ($data as $key => $item) {
            $data[$key]['person'] = $this->personRepository->find($item['person_locale_id']);
            $data[$key]['recommended'] = $this->getRecommendedDifficulty($item['avg'], ++$count, $total, $persons);
        }

        return $data;
    }

    private function getRecommendedDifficulty($avg, $count, $total, $persons): string
    {
        $p1 = 100 * $count/$total;
        $p2 = 100 * $count/$persons;

        return match (true) {
            $p1 < self::RANGES[1] || $p2 < self::RANGES[1] => 1,
            $p1 < self::RANGES[2] || $p2 < self::RANGES[2] => 2,
            $p1 < self::RANGES[3] || $p2 < self::RANGES[3] => 3,
            $p1 < self::RANGES[4] || $p2 < self::RANGES[4] => 4,
            $p1 < self::RANGES[5] || $p2 < self::RANGES[5] => 5,
            $p1 < self::RANGES[6] || $p2 < self::RANGES[6] => 6,
            $p1 < self::RANGES[7] || $p2 < self::RANGES[7] => 7,
            $p1 < self::RANGES[8] || $p2 < self::RANGES[8] => 8,
            $p1 < self::RANGES[9] || $p2 < self::RANGES[9] => 9,
            default => 10
        };
    }
}
