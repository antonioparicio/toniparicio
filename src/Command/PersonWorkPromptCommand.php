<?php

namespace App\Command;

use App\Repository\WhoIAm\PersonLocaleRepository;
use App\Util\ChatGpt\ChatGptService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonWorkPromptCommand.
 */
class PersonWorkPromptCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:chatgpt:works';


    public function __construct(
        private readonly ChatGptService         $chatGptService,
        private readonly PersonLocaleRepository $repository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Search works in chatgpt for a person')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('locale', 'l', InputOption::VALUE_OPTIONAL, 'Locale to use in batch mode', 'es')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force get facts even if already exists or processed')
            ->setHelp('This command allows you to search works in ChatGPT for a person locale');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $personId = $input->getArgument('id');
        $force = $input->getOption('force');

        if ($personId) {
            return $this->singlePersonMode((int) $personId, $output, $force);
        }

        return $this->batchMode($output, $input->getOption('locale'));
    }

    private function singlePersonMode(int $personId, OutputInterface $output, bool $force = false): int
    {
        $person = $this->repository->find($personId);

        if (null === $person) {
            $output->writeln(sprintf('PersonLocale %s not found', $personId));

            return 1;
        }

        $output->writeln(sprintf('Searching works for %s in %s', $person->getName(), $person->getLocale()));

        $result = $this->chatGptService->get([$person], ChatGptService::TYPE_WORKS, null, $force);

        $output->writeln(sprintf('Results %s', $result));

        return 0;
    }

    private function batchMode(OutputInterface $output, string $locale): int
    {
        $personsArr = $this->repository->getPersonsWithoutWorks($locale);

        if ([] === $personsArr) {
            $output->writeln('Not enough PersonLocales to execute in batch mode');

            return 1;
        }

        $persons = [];
        foreach ($personsArr as $personArr) {
            $output->writeln(sprintf('%s', $personArr['name']));
            $person = $this->repository->find($personArr['id']);
            if ($person !== null) {
                $persons[] = $person;
            }
        }
        $this->chatGptService->queue($persons, ChatGptService::TYPE_WORKS);

        $output->writeln(sprintf('%d PersonLocales added to ChatGPT queue for get works', count($persons)));


        return 0;
    }
}
