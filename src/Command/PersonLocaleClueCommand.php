<?php

namespace App\Command;

use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\PersonLocaleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonCommand
 * @package App\Command
 */
class PersonLocaleClueCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:clues';
    private const MINIMAL = 3;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * ImageProcessCommand constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Update PersonLocale clue values')
            ->addOption(
                'minimal',
                'm',
                InputOption::VALUE_OPTIONAL,
                'Minimal number of clues for recalculate PersonLocale clues',
                self::MINIMAL
            )
            ->addOption(
                'size',
                's',
                InputOption::VALUE_OPTIONAL,
                'Number of PersonLocale to handle in a batch',
                100
            )
            ->setHelp('This command update all PersonLocal clue values when no value is set');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // ... put here the code to run in your command
        $repo = $this->manager->getRepository(PersonLocale::class);
        /** @var PersonLocaleRepository $repo */
        $persons = $repo->getCluesEmpty($input->getOption('minimal'), $input->getOption('size'));
        $table = new Table($output);
        $table->setHeaders(['Person', 'Locale', 'Clues'])
        ;
        foreach ($persons as $person) {
            /** @var PersonLocale $person */
            $person->setClues();
            $this->manager->persist($person);

            $table->addRow([$person->getName(), $person->getLocale(), $person->getClues()]);
        }

        $output->writeln(sprintf('%d PersonLocale updated', count($persons)));
        $table->render();
        $this->manager->flush();
        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))


        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        return true;
    }
}
