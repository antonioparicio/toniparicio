<?php

namespace App\Command;

use App\Entity\WhoIAm\Person;
use App\Repository\WhoIAm\PersonRepository;
use App\Util\ChatGpt\ChatGptService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonFactPromptCommand.
 */
class PersonPromptCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:chatgpt';


    public function __construct(
        private readonly ChatGptService   $chatGptService,
        private readonly PersonRepository $repository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Search clues in ChatGPT for a person')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('items', 'i', InputArgument::OPTIONAL, 'Number of items to process in batch mode', 5)
            ->addOption('locales', 'l', InputArgument::OPTIONAL, 'Locale to use in batch mode: [es|en]', 'es')
            ->setHelp('This command allows you to search clues in ChatGPT for a person');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $personId = $input->getArgument('id');
        $locales = explode(',', $input->getOption('locales'));

        if ($personId) {
            return $this->singlePersonMode((int) $personId, $locales, $output);
        }

        return $this->batchMode($output, $locales, (int) $input->getOption('items'));
    }

    private function singlePersonMode(int $personId, array $locales, OutputInterface $output): int
    {
        /** @var Person $person */
        $person = $this->repository->find($personId);

        if (null === $person) {
            $output->writeln(sprintf('PersonLocale %s not found', $personId));

            return 1;
        }

        $output->writeln(sprintf('Searching clues for %s', $person->getRealName()));

        $queue = [];
        foreach ($person->getLocales() as $personLocale) {
            if (in_array($personLocale->getLocale(), $locales)) {
                $output->writeln(
                    sprintf('Searching clues for %s in %s', $personLocale->getName(), $personLocale->getLocale())
                );
                $queue[] = $personLocale;
            }
        }
        $this->chatGptService->queue($queue);

        $output->writeln('Done. Check chatgpt_prompts for more info.');

        return 0;
    }

    private function batchMode(OutputInterface $output, array $locales, int $items = 5): int
    {
        $persons = $this->repository->getRandomPersons($items);

        if ([] === $persons) {
            $output->writeln('Not enough PersonLocales to execute in batch mode');

            return 1;
        }

        $candidates = [];
        foreach ($persons as $person) {
            $output->writeln(sprintf('%s', $person->getRealName()));

            foreach ($person->getLocales() as $personLocale) {
                if (in_array($personLocale->getLocale(), $locales)) {
                    $output->writeln(
                        sprintf('Searching clues for %s in %s', $personLocale->getName(), $personLocale->getLocale())
                    );
                    $candidates[] = $personLocale;
                }
            }
        }
        $this->chatGptService->queue($candidates);

        $output->writeln(
            sprintf('%d PersonLocales queued will be processed by PersonPromptQueueCommand', count($persons))
        );

        return 0;
    }
}
