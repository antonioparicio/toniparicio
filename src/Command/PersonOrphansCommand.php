<?php

namespace App\Command;

use App\Entity\WhoIAm\Person;
use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\PersonLocaleRepository;
use App\Repository\WhoIAm\PersonRepository;
use App\Util\ChatGpt\ChatGptService;
use App\Util\Person\PersonTool;
use App\Util\Wikipedia\WikipediaClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class PersonCommand
 * @package App\Command
 */
class PersonOrphansCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:orphans';

    public function __construct(
        private readonly PersonRepository $personRepository,
        private readonly PersonLocaleRepository $personLocaleRepository,
        private readonly WikipediaClient $wikipediaClient,
        private readonly PersonTool $personTool,
        private readonly ChatGptService $chatGptService,
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Search for persons with none or less locales')
            ->addOption('fix', 'f', InputOption::VALUE_OPTIONAL, "try to fix orphans", false)
            ->setHelp('This command allows you to search in wikipedia a person for WhoIAm game');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $isFix = $input->getOption('fix') !== false;

        $helper = $this->getHelper('question');
        $table = new Table($output);
        $table->setHeaders(['ID', 'Person Name', 'Missed', 'Duplicated', 'Locales']);
        $i = 0;
        foreach ($this->personRepository->findAll() as $person) {
            $row = [];
            /** @var Person $person */
            if (count($person->getLocales()) === 0) {
                $row = [$person->getId(), $person->getRealName(), 'ES|EN'];
            }
            if (count($person->getLocales()) === 1) {
                /** @var PersonLocale $locale */
                $locale = $person->getLocales()->first();
                if ($locale->getLocale() === 'es') {
                    $row = [$person->getId(), $person->getRealName(), 'EN'];
                }
                if ($locale->getLocale() === 'en') {
                    $row = [$person->getId(), $person->getRealName(), 'ES'];
                }
            }
            if (count($person->getLocales()) < 2) {
                $row[3] = null;
                $duplicate = $this->personRepository->getDuplicate($person);
                if ($duplicate) {
                    // search a duplicate person
                    $row[3] = "[Person {$duplicate->getId()}] {$duplicate->getRealName()}";
                }
                if (!$duplicate) {
                    // search a duplicate locale
                    $duplicate = $this->personLocaleRepository->getDuplicate($person);
                    if ($duplicate) {
                        $row[3] = "[Person {$duplicate->getPerson()->getId()}][PersonLocale {$duplicate->getId()}|{$duplicate->getLocale()}] {$duplicate->getName()}";
                    }
                }

            }
            if ($row) {
                $i++;
                $question = new ConfirmationQuestion(
                    sprintf('Do you want search %s in wikipedia? (y/N): ', $person->getRealName()),
                    // choices can also be PHP objects that implement __toString() method
                    false
                );
                if ($isFix && $helper->ask($input, $output, $question)) {
                    $wikipedia = null;
                    try {
                        $wikipedia = $this->wikipediaClient->processUrl(rawurldecode($person->getRealName()));
                    } catch (\Exception $exception) {
                        $output->writeln($exception->getMessage());
                    }
                    if ($wikipedia) {
                        $output->writeln(sprintf('Wikipedia page: %s', $wikipedia->getTitle()));
                        $person = $this->personTool->updateFromWikipediaPerson($wikipedia);
                        if ($person instanceof Person && [] !== $person->getLocales()) {
                            $this->chatGptService->queue($person->getLocales()->toArray());
                            $output->writeln(sprintf('%s added to ChatGPT queue', $person->getRealName()));
                            $row[4] = implode(', ', array_map(static function (PersonLocale $locale) {
                                return $locale->getLocale() . ':' . $locale->getId();
                            }, $person->getLocales()->toArray()));
                        }
                    }
                }
                $row[4] = $row[4] ?? null;
                $table->addRow($row);
            }
        }
        $table->render();
        $output->writeln(sprintf('%d persons need attention', $i));

        return 1;
    }
}
