<?php

namespace App\Command;

use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\PersonLocaleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class PersonCommand
 * @package App\Command
 */
class PersonClueRealNameCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:clue:real_name';

    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly PersonLocaleRepository $personRepository
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Search person locales for real name clue candidate')
            ->addArgument('locale', InputArgument::OPTIONAL, 'Locale to check', null)
            ->addOption('latin-only', null, InputOption::VALUE_OPTIONAL, "Only use latin names", false)
            ->addOption('dry-run', null, InputOption::VALUE_OPTIONAL, "run without update DDBB", false)
            ->setHelp('This command search all person locales and compare name with real name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $locale = $input->getArgument('locale');
        $dryRun = $input->getOption('dry-run') !== false;
        $isLatinOnly = $input->getOption('latin-only') !== false;

        $helper = $this->getHelper('question');

        $repo = $this->personRepository;
        $query = ['is_clue_name' => false];
        if ($locale) {
            $query['locale'] = $locale;
        }
        $output->writeln("[Locale][Name Locale] [Real Name]");

        /** @var PersonLocale $person */
        foreach ($repo->findBy($query) as $person) {
            $nameArr = explode(' ', $person->getName());
            $realNameArr = explode(' ', $person->getPerson()->getRealName());
            $candidate = true;

            if ($isLatinOnly && !preg_match('/^[\p{L},\’\'-\.\s]+$/', utf8_decode($person->getPerson()->getRealName()))) {
                $output->writeln(
                    sprintf('%s discarted because no latin characteres', $person->getPerson()->getRealName())
                );
                continue;
            }
            foreach ($nameArr as $part) {
                if (in_array($part, $realNameArr)) {
                    $candidate = false;
                    break;
                }
            }
            if ($candidate) {
                $question = new ConfirmationQuestion(
                    sprintf(
                        '[%s][%s] [%s] Do you want activate real name clue? (y/N): ',
                        $person->getLocale(),
                        $person->getName(),
                        $person->getPerson()->getRealName()
                    ),
                    // choices can also be PHP objects that implement __toString() method
                    false
                );
                if ($helper->ask($input, $output, $question)) {
                    $person->setIsClueName(true);
                    $this->manager->persist($person);
                }
            }
        }

        if (!$dryRun) {
            $this->manager->flush();
        }

        return 1;
    }
}
