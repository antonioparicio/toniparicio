<?php

namespace App\Command;

use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\PersonLocaleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonCommand
 * @package App\Command
 */
class PersonLocaleDifficultyCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:difficulty:copy';

    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly PersonLocaleRepository $personRepository
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Search for persons with none or less locales')
            ->addArgument('from', InputArgument::OPTIONAL, 'Locales to use as source for get difficulty', 'es')
            ->addArgument('to', InputArgument::OPTIONAL, 'Locales to update with difficulty', 'en')
            ->addOption('dry-run', null, InputOption::VALUE_OPTIONAL, "run without update DDBB", false)
            ->setHelp('This command allows you to search in wikipedia a person for WhoIAm game');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $from = $input->getArgument('from');
        $to = $input->getArgument('to');
        $dryRun = $input->getOption('dry-run') !== false;

        $table = new Table($output);
        $table->setHeaders([
            sprintf('Origin [%s]', $from),
            sprintf('Update [%s]', $to),
            'Difficulty'
        ]);
        /** @var PersonLocale $person */
        foreach ($this->personRepository->findBy(['locale' => $to]) as $person) {
            $row = [
                "[{$person->getId()}] {$person->getName()}",
                null,
                $person->getDifficulty()
            ];
            $source = $person->getPerson()->getLocale($from);

            if ($source && $person->getDifficulty() !== $source->getDifficulty()) {
                $row[1] = "[{$source->getId()}] {$source->getName()}";
                $row[2] = "{$person->getDifficulty()} -> {$source->getDifficulty()}";
                $person->setDifficulty($source->getDifficulty());
                $table->addRow($row);

                if (!$dryRun) {
                    $this->manager->persist($person);
                }
            }
        }
        $table->render();

        if (!$dryRun) {
            $this->manager->flush();
        }

        return 1;
    }
}
