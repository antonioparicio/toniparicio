<?php

namespace App\Command;

use App\Entity\WhoIAm\ChatGptQueue;
use App\Entity\WhoIAm\Person;
use App\Repository\WhoIAm\PersonRepository;
use App\Util\ChatGpt\ChatGptService;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonFactPromptCommand.
 */
class PersonPromptQueueCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:chatgpt:queue';


    public function __construct(
        private readonly ChatGptService   $chatGptService,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Process ChatGPT queue')
            ->setHelp('This command process unhandled items in chatgpt_queue');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $repository = $this->entityManager->getRepository(ChatGptQueue::class);
        $queue = $repository->findOneBy(['isHandled' => false]);

        if (!$queue) {
            $output->writeln('No ChatGPT waiting in queue...');
            return 0;
        }

        $output->writeln(
            sprintf(
                'Processing item in queue: %s in locale %s for %s',
                $queue->getPersonLocale()->getName(),
                $queue->getPersonLocale()->getLocale(),
                $queue->getType()
            )
        );
        $this->entityManager->getConnection()->executeStatement('SET session wait_timeout=90');
        $this->chatGptService->get([$queue->getPersonLocale()], $queue->getType(), $queue);
        $queue->setIsHandled(true);
        $this->entityManager->persist($queue);
        $this->entityManager->flush();

        return 0;
    }
}
