<?php

namespace App\Command;

use App\Entity\WhoIAm\PersonLocale;
use App\Repository\WhoIAm\PersonLocaleRepository;
use App\Util\ChatGpt\ChatGptService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PersonFactPromptCommand.
 */
class PersonDeathPromptCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'whoiam:person:chatgpt:death';


    public function __construct(
        private readonly ChatGptService         $chatGptService,
        private readonly PersonLocaleRepository $repository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setName(self::$defaultName)
            ->setDescription('Search death info in chatgpt for a person')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('locale', 'l', InputOption::VALUE_OPTIONAL, 'Locale to use in batch mode', 'es')
            ->setHelp('This command allows you to search death info in ChatGPT for a person locale');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $personId = $input->getArgument('id');

        if ($personId) {
            return $this->singlePersonMode((int) $personId, $output);
        }

        return $this->batchMode($output, $input->getOption('locale'));
    }

    private function singlePersonMode(int $personId, OutputInterface $output): int
    {
        $person = $this->repository->find($personId);

        if (null === $person) {
            $output->writeln(sprintf('PersonLocale %s not found', $personId));

            return 1;
        }

        $output->writeln(sprintf('Searching death for %s in %s', $person->getName(), $person->getLocale()));

        $result = $this->chatGptService->get([$person], ChatGptService::TYPE_DEATH);

        $output->writeln(sprintf('Results %s', $result));

        return 0;
    }

    private function batchMode(OutputInterface $output, string $locale): int
    {
        $persons = $this->repository->findBy(['locale' => $locale]);

        if ([] === $persons) {
            $output->writeln('Not enough PersonLocales to execute in batch mode');

            return 1;
        }

        $this->chatGptService->queue($persons, ChatGptService::TYPE_DEATH);

        $output->writeln(sprintf('%d PersonLocales added to ChatGPT queue for get death', count($persons)));

        return 0;
    }
}
